"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
var pluralize = require('pluralize')

module.exports = {
  /**
   * Update a record.
   *
   * @return {Object}
   */

  async like(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.casino.findOne({ id });

    await strapi.services.casino.update(
      { id },
      {
        likes: entity.likes + 1,
      }
    );

    return sanitizeEntity(entity, { model: strapi.models.casino });
  },
  /**
   * Update a record.
   *
   * @return {Object}
   */

  async getByTag(ctx) {
    const { tag, tagValue } = ctx.params;
    if (!["restricted-countries", "payment-methods", "softwares", "licences", "languages", "categories", "currencies"].includes(tag)) {
      return;
    }
    let tagUnderscore = tag.replace("-", "_");
    let tagsingular = pluralize(tag, 1);

    const getID = await strapi.connections.default.raw(
      `SELECT id FROM ${tagUnderscore} WHERE slug = ?`,
      tagValue
    );
    const campaigns = await strapi.connections.default.raw(
      "SELECT DISTINCT casinos.id FROM casinos INNER JOIN casinos_components ON casinos.id = casinos_components.casino_id INNER JOIN components_misc_tags__" +
        tagUnderscore +
        "   ON casinos_components.component_id = components_misc_tags__" +
        tagUnderscore +
        ".components_misc_tag_id        WHERE casinos_components.component_type = 'components_misc_tags' AND  `" +
        tagsingular +
        "_id` = ?;",
      getID[0].id
    );
    var finalArray = campaigns.map(function (obj) {
      return obj.id;
    });
    const result = await strapi.query("casino").find({ id_in: finalArray });

    // const result = await strapi
    // .query('casino')
    // .model.query(qb => {
    //   qb.where('id','in', finalArray);
    // })
    // .fetch();

    // const fields = result.toJSON();
    var finalArray2 = result.map(function (obj) {
      delete obj.updated_by;
      delete obj.created_by;
      return obj;
    });

    return finalArray2;
  },
};
