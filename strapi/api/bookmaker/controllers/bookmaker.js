"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
var pluralize = require('pluralize')

module.exports = {
  /**
   * Update a record.
   *
   * @return {Object}
   */

  async like(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.bookmaker.findOne({ id });

    await strapi.services.bookmaker.update(
      { id },
      {
        likes: entity.likes + 1,
      }
    );

    return sanitizeEntity(entity, { model: strapi.models.bookmaker });
  },
  /**
   * Update a record.
   *
   * @return {Object}
   */

  async getByTag(ctx) {
    const { tag, tagValue } = ctx.params;
    if (!["restricted-countries", "payment-methods", "softwares", "licences", "languages", "categories", "currencies"].includes(tag)) {
      return;
    }
    let tagUnderscore = tag.replace("-", "_");
    let tagsingular = pluralize(tag, 1);

    const getID = await strapi.connections.default.raw(
      `SELECT id FROM ${tagUnderscore} WHERE slug = ?`,
      tagValue
    );
    const campaigns = await strapi.connections.default.raw(
      "SELECT DISTINCT bookmaker.id FROM bookmaker INNER JOIN bookmaker_components ON bookmaker.id = bookmaker_components.bookmaker_id INNER JOIN components_misc_tags__" +
        tagUnderscore +
        "   ON bookmaker_components.component_id = components_misc_tags__" +
        tagUnderscore +
        ".components_misc_tag_id        WHERE bookmaker_components.component_type = 'components_misc_tags' AND  `" +
        tagsingular +
        "_id` = ?;",
      getID[0].id
    );
    var finalArray = campaigns.map(function (obj) {
      return obj.id;
    });
    const result = await strapi.query("bookmaker").find({ id_in: finalArray });

    // const result = await strapi
    // .query('bookmaker')
    // .model.query(qb => {
    //   qb.where('id','in', finalArray);
    // })
    // .fetch();

    // const fields = result.toJSON();
    var finalArray2 = result.map(function (obj) {
      delete obj.updated_by;
      delete obj.created_by;
      return obj;
    });

    return finalArray2;
  },
};
