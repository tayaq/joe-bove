"use strict";
const { sanitizeEntity } = require("strapi-utils");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async getLatest(ctx) {
    const campaigns = await strapi.connections.default.raw(
      "SELECT relatedSlug, content, authorName FROM comments WHERE relatedSlug LIKE '%blog%' ORDER BY created_at LIMIT "+ ctx.params.limit ?? 5
    );

    var finalArray = await Promise.all(
      campaigns.map(async function (obj) {
        const result = await strapi
          .query("blog")
          .find({ id: obj.relatedSlug.replace("blog:", "") });
        obj.blog = sanitizeEntity(result, { model: strapi.models.blog });
        return obj;
      })
    );

    return finalArray;
  },
};
