const slugify = require('slugify');

const slugifyComponent = (data) => {
  if (data.title) {
    data.slug = slugify(data.title);
  } else if(data.name) {
    data.slug = slugify(data.name);
  }
}

module.exports = {
  beforeCreate: async (data) => {
    slugifyComponent(data)
  },
  beforeUpdate: async (params, data) => {
    slugifyComponent(data)
  },
};
