module.exports = ({ env }) => ({
  host: env('HOST', 'localhost'),
  port: env.int('PORT', 1337),
  url: env('URL'),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '2debb788d1ae18b2484dc85a15b5395a'),
    },
  },
});
