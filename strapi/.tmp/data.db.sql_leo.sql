BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "core_store" (
	"id"	integer NOT NULL,
	"key"	varchar(255),
	"value"	text,
	"type"	varchar(255),
	"environment"	varchar(255),
	"tag"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_links_normal_links" (
	"id"	integer NOT NULL,
	"href"	varchar(255),
	"name"	varchar(255),
	"target"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_links_social_links" (
	"id"	integer NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_links_social_links_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"components_links_social_link_id"	integer NOT NULL,
	CONSTRAINT "components_links_social_link_id_fk" FOREIGN KEY("components_links_social_link_id") REFERENCES "components_links_social_links"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_custom_enumerations" (
	"id"	integer NOT NULL,
	"text"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_details" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_details_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"components_misc_detail_id"	integer NOT NULL,
	CONSTRAINT "components_misc_detail_id_fk" FOREIGN KEY("components_misc_detail_id") REFERENCES "components_misc_details"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_icons" (
	"id"	integer NOT NULL,
	"type"	varchar(255),
	"name"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_pro_cons" (
	"id"	integer NOT NULL,
	"pros"	varchar(255),
	"cons"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "casinos_tags__tags_casinos" (
	"id"	integer NOT NULL,
	"casino_id"	integer,
	"tag_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "casinos_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"casino_id"	integer NOT NULL,
	CONSTRAINT "casino_id_fk" FOREIGN KEY("casino_id") REFERENCES "tmp_casinos"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "tag_types" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"slug"	varchar(255),
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "strapi_webhooks" (
	"id"	integer NOT NULL,
	"name"	varchar(255),
	"url"	text,
	"headers"	text,
	"events"	text,
	"enabled"	boolean,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "strapi_permission" (
	"id"	integer NOT NULL,
	"action"	varchar(255) NOT NULL,
	"subject"	varchar(255),
	"properties"	text,
	"conditions"	text,
	"role"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "strapi_role" (
	"id"	integer NOT NULL,
	"name"	varchar(255) NOT NULL,
	"code"	varchar(255) NOT NULL,
	"description"	varchar(255),
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "strapi_administrator" (
	"id"	integer NOT NULL,
	"firstname"	varchar(255),
	"lastname"	varchar(255),
	"username"	varchar(255),
	"email"	varchar(255) NOT NULL,
	"password"	varchar(255),
	"resetPasswordToken"	varchar(255),
	"registrationToken"	varchar(255),
	"isActive"	boolean,
	"blocked"	boolean,
	"preferedLanguage"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "strapi_users_roles" (
	"id"	integer NOT NULL,
	"user_id"	integer,
	"role_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "i18n_locales" (
	"id"	integer NOT NULL,
	"name"	varchar(255),
	"code"	varchar(255),
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "upload_file" (
	"id"	integer NOT NULL,
	"name"	varchar(255) NOT NULL,
	"alternativeText"	varchar(255),
	"caption"	varchar(255),
	"width"	integer,
	"height"	integer,
	"formats"	text,
	"hash"	varchar(255) NOT NULL,
	"ext"	varchar(255),
	"mime"	varchar(255) NOT NULL,
	"size"	float NOT NULL,
	"url"	varchar(255) NOT NULL,
	"previewUrl"	varchar(255),
	"provider"	varchar(255) NOT NULL,
	"provider_metadata"	text,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "upload_file_morph" (
	"id"	integer NOT NULL,
	"upload_file_id"	integer,
	"related_id"	integer,
	"related_type"	text,
	"field"	text,
	"order"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "users-permissions_permission" (
	"id"	integer NOT NULL,
	"type"	varchar(255) NOT NULL,
	"controller"	varchar(255) NOT NULL,
	"action"	varchar(255) NOT NULL,
	"enabled"	boolean NOT NULL,
	"policy"	varchar(255),
	"role"	integer,
	"created_by"	integer,
	"updated_by"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "users-permissions_role" (
	"id"	integer NOT NULL,
	"name"	varchar(255) NOT NULL,
	"description"	varchar(255),
	"type"	varchar(255),
	"created_by"	integer,
	"updated_by"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "users-permissions_user" (
	"id"	integer NOT NULL,
	"username"	varchar(255) NOT NULL,
	"email"	varchar(255) NOT NULL,
	"provider"	varchar(255),
	"password"	varchar(255),
	"resetPasswordToken"	varchar(255),
	"confirmationToken"	varchar(255),
	"confirmed"	boolean,
	"blocked"	boolean,
	"role"	integer,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_ratings" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"percentage"	float,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_links_mega_menu_links" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_links_mega_menu_links_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"components_links_mega_menu_link_id"	integer NOT NULL,
	CONSTRAINT "components_links_mega_menu_link_id_fk" FOREIGN KEY("components_links_mega_menu_link_id") REFERENCES "components_links_mega_menu_links"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "nav_links_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"nav_link_id"	integer NOT NULL,
	CONSTRAINT "nav_link_id_fk" FOREIGN KEY("nav_link_id") REFERENCES "tmp_nav_links"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_badges" (
	"id"	integer NOT NULL,
	"text"	varchar(255),
	"type"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "nav_links" (
	"id"	integer NOT NULL,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "slots__tags" (
	"id"	integer NOT NULL,
	"slot_id"	integer,
	"tag-type_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "slots_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"slot_id"	integer NOT NULL,
	CONSTRAINT "slot_id_fk" FOREIGN KEY("slot_id") REFERENCES "tmp_slots"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "comments" (
	"id"	integer NOT NULL,
	"content"	text,
	"name"	varchar(255),
	"email"	varchar(255),
	"website"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "bonuses" (
	"id"	integer NOT NULL,
	"bonus"	varchar(255),
	"title"	varchar(255),
	"bonusLink"	varchar(255),
	"slug"	varchar(255),
	"rewards"	text,
	"shortDesc"	varchar(255),
	"about"	text,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "bonuses_tags__tags_bonuses" (
	"id"	integer NOT NULL,
	"bonus_id"	integer,
	"tag_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "bonuses_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"bonus_id"	integer NOT NULL,
	CONSTRAINT "bonus_id_fk" FOREIGN KEY("bonus_id") REFERENCES "bonuses"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "bookmakers" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "bookmaker_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"bookmaker_id"	integer NOT NULL,
	CONSTRAINT "bookmaker_id_fk" FOREIGN KEY("bookmaker_id") REFERENCES "tmp_bookmaker"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "bookmakers_tags__tags_bookmakers" (
	"id"	integer NOT NULL,
	"bookmaker_id"	integer,
	"tag_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "tags" (
	"id"	integer NOT NULL,
	"title"	varchar(255) NOT NULL,
	"slug"	varchar(255),
	"tag_type"	integer,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "bookmaker" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"description"	varchar(255),
	"about"	text,
	"type"	varchar(255),
	"likes"	integer,
	"referralLink"	varchar(255),
	"active"	boolean,
	"slug"	varchar(255),
	"test"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "tests" (
	"id"	integer NOT NULL,
	"test"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "news" (
	"id"	integer NOT NULL,
	"test"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "blog_tags" (
	"id"	integer NOT NULL,
	"name"	varchar(255),
	"slug"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "blogs" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"content"	text,
	"author"	integer,
	"slug"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "blogs__tags" (
	"id"	integer NOT NULL,
	"blog_id"	integer,
	"blog-tag_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_headers_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"home_header_id"	integer NOT NULL,
	CONSTRAINT "home_header_id_fk" FOREIGN KEY("home_header_id") REFERENCES "tmp_home_headers"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_headers" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"description"	varchar(255),
	"email"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_characteristics" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"description"	text,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_characteristics_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"home_characteristic_id"	integer NOT NULL,
	CONSTRAINT "home_characteristic_id_fk" FOREIGN KEY("home_characteristic_id") REFERENCES "tmp_home_characteristics"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_section_titles" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"subtitle"	varchar(255),
	"description"	text,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_faqs" (
	"id"	integer NOT NULL,
	"question"	varchar(255),
	"answer"	text,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_faqs_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"home_faq_id"	integer NOT NULL,
	CONSTRAINT "home_faq_id_fk" FOREIGN KEY("home_faq_id") REFERENCES "tmp_home_faqs"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_testimonials" (
	"id"	integer NOT NULL,
	"author"	varchar(255),
	"position"	varchar(255),
	"message"	text,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_testimonials_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"home_testimonial_id"	integer NOT NULL,
	CONSTRAINT "home_testimonial_id_fk" FOREIGN KEY("home_testimonial_id") REFERENCES "tmp_home_testimonials"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_faqs" (
	"id"	integer NOT NULL,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_testimonials" (
	"id"	integer NOT NULL,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_characteristics" (
	"id"	integer NOT NULL,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_get_in_touches_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"components_misc_get_in_touch_id"	integer NOT NULL,
	CONSTRAINT "components_misc_get_in_touch_id_fk" FOREIGN KEY("components_misc_get_in_touch_id") REFERENCES "tmp_components_misc_get_in_touches"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_get_in_touches_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"home_get_in_touch_id"	integer NOT NULL,
	CONSTRAINT "home_get_in_touch_id_fk" FOREIGN KEY("home_get_in_touch_id") REFERENCES "tmp_home_get_in_touches"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_get_in_touches" (
	"id"	integer NOT NULL,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "home_partners" (
	"id"	integer NOT NULL,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "footers" (
	"id"	integer NOT NULL,
	"aboutUs"	text,
	"email"	varchar(255),
	"phoneNo"	varchar(255),
	"subscribeText"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "footers_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"footer_id"	integer NOT NULL,
	CONSTRAINT "footer_id_fk" FOREIGN KEY("footer_id") REFERENCES "footers"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "menus" (
	"id"	integer NOT NULL,
	"siteName"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "menus__nav_links" (
	"id"	integer NOT NULL,
	"menu_id"	integer,
	"nav-link_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "menus_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"menu_id"	integer NOT NULL,
	CONSTRAINT "menu_id_fk" FOREIGN KEY("menu_id") REFERENCES "menus"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "components_misc_get_in_touches" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"description"	varchar(255),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "casinos" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"description"	varchar(255),
	"about"	text,
	"type"	varchar(255),
	"likes"	integer,
	"referralLink"	varchar(255),
	"active"	boolean,
	"slug"	varchar(255),
	"review"	integer,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "reviews_components" (
	"id"	integer NOT NULL,
	"field"	varchar(255) NOT NULL,
	"order"	integer NOT NULL,
	"component_type"	varchar(255) NOT NULL,
	"component_id"	integer NOT NULL,
	"review_id"	integer NOT NULL,
	CONSTRAINT "review_id_fk" FOREIGN KEY("review_id") REFERENCES "tmp_reviews"("id") on delete CASCADE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "reviews" (
	"id"	integer NOT NULL,
	"content"	text,
	"title"	varchar(255),
	"slug"	varchar(255),
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "slots" (
	"id"	integer NOT NULL,
	"title"	varchar(255),
	"casino"	integer,
	"description"	text,
	"about"	text,
	"type"	varchar(255),
	"shortDescription"	varchar(255),
	"review"	integer,
	"published_at"	datetime,
	"created_by"	integer,
	"updated_by"	integer,
	"created_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	"updated_at"	datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "slots__comments" (
	"id"	integer NOT NULL,
	"slot_id"	integer,
	"comment_id"	integer,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (1,'model_def_strapi::core-store','{"uid":"strapi::core-store","collectionName":"core_store","info":{"name":"core_store","description":""},"options":{"timestamps":false},"pluginOptions":{"content-manager":{"visible":false},"content-type-builder":{"visible":false}},"attributes":{"key":{"type":"string"},"value":{"type":"text"},"type":{"type":"string"},"environment":{"type":"string"},"tag":{"type":"string"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (2,'model_def_links.normal-link','{"uid":"links.normal-link","collectionName":"components_links_normal_links","info":{"name":"NormalLink","icon":"external-link-alt"},"options":{"timestamps":false},"attributes":{"href":{"type":"string"},"name":{"type":"string"},"target":{"type":"string"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (3,'model_def_links.social-link','{"uid":"links.social-link","collectionName":"components_links_social_links","info":{"name":"SocialLink","icon":"link"},"options":{"timestamps":false},"attributes":{"icon":{"type":"component","repeatable":false,"component":"misc.icon"},"link":{"type":"component","repeatable":false,"component":"links.normal-link"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (4,'model_def_misc.custom-enumeration','{"uid":"misc.custom-enumeration","collectionName":"components_misc_custom_enumerations","info":{"name":"CustomEnumeration","icon":"angry"},"options":{"timestamps":false},"attributes":{"text":{"type":"string"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (5,'model_def_misc.details','{"uid":"misc.details","collectionName":"components_misc_details","info":{"name":"Details","icon":"align-left"},"options":{"timestamps":false},"attributes":{"title":{"type":"string"},"item":{"type":"component","repeatable":true,"component":"misc.custom-enumeration"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (6,'model_def_misc.icon','{"uid":"misc.icon","collectionName":"components_misc_icons","info":{"name":"Icon","icon":"external-link-square-alt"},"options":{"timestamps":false},"attributes":{"type":{"type":"enumeration","enum":["Solid","Regular","Bussiness"]},"name":{"type":"string"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (7,'model_def_misc.pro-con','{"uid":"misc.pro-con","collectionName":"components_misc_pro_cons","info":{"name":"Experiences","icon":"plus","description":""},"options":{"timestamps":false},"attributes":{"pros":{"type":"string"},"cons":{"type":"string"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (8,'model_def_misc.rating','{"uid":"misc.rating","collectionName":"components_misc_ratings","info":{"name":"Rating","icon":"star","description":""},"options":{"timestamps":false},"attributes":{"title":{"type":"string","required":true},"percentage":{"type":"float","default":0,"max":100}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (9,'model_def_application::casino.casino','{"uid":"application::casino.casino","collectionName":"casinos","kind":"collectionType","info":{"name":"Casino","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"title":{"type":"string","required":true},"thumbnail":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"description":{"type":"string"},"about":{"type":"richtext"},"type":{"type":"enumeration","enum":["affiliate","certified","download","live","mobile","online"],"required":true},"experiences":{"type":"component","repeatable":true,"component":"misc.pro-con"},"likes":{"type":"integer","default":0},"referralLink":{"type":"string","private":false},"socials":{"type":"component","repeatable":true,"component":"links.social-link"},"active":{"type":"boolean","default":true,"required":true},"acceptedCountries":{"type":"component","repeatable":true,"component":"misc.custom-enumeration"},"slug":{"type":"string"},"tags":{"collection":"tags","via":"casinos","dominant":true,"attribute":"tag","column":"id","isVirtual":true},"slots":{"via":"casino","collection":"slot","isVirtual":true},"cover":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"review":{"via":"casinos","model":"review"},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (10,'model_def_application::tag-types.tag-types','{"uid":"application::tag-types.tag-types","collectionName":"tag_types","kind":"collectionType","info":{"name":"TagTypes","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":false},"pluginOptions":{},"attributes":{"title":{"type":"string"},"slug":{"type":"string"},"tags":{"collection":"tags","via":"tag_type","isVirtual":true},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (11,'model_def_application::tags.tags','{"uid":"application::tags.tags","collectionName":"tags","kind":"collectionType","info":{"name":"Tags","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":false},"pluginOptions":{},"attributes":{"title":{"type":"string","required":true},"slug":{"type":"string"},"tag_type":{"via":"tags","model":"tag-types"},"casinos":{"via":"tags","collection":"casino","attribute":"casino","column":"id","isVirtual":true},"bookmakers":{"via":"tags","collection":"bookmaker","attribute":"bookmaker","column":"id","isVirtual":true},"bonuses":{"via":"tags","collection":"bonus","attribute":"bonus","column":"id","isVirtual":true},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (12,'model_def_strapi::webhooks','{"uid":"strapi::webhooks","collectionName":"strapi_webhooks","info":{"name":"Strapi webhooks","description":""},"options":{"timestamps":false},"pluginOptions":{"content-manager":{"visible":false},"content-type-builder":{"visible":false}},"attributes":{"name":{"type":"string"},"url":{"type":"text"},"headers":{"type":"json"},"events":{"type":"json"},"enabled":{"type":"boolean"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (13,'model_def_strapi::permission','{"uid":"strapi::permission","collectionName":"strapi_permission","kind":"collectionType","info":{"name":"Permission","description":""},"options":{"timestamps":["created_at","updated_at"]},"pluginOptions":{"content-manager":{"visible":false},"content-type-builder":{"visible":false}},"attributes":{"action":{"type":"string","minLength":1,"configurable":false,"required":true},"subject":{"type":"string","minLength":1,"configurable":false,"required":false},"properties":{"type":"json","configurable":false,"required":false,"default":{}},"conditions":{"type":"json","configurable":false,"required":false,"default":[]},"role":{"configurable":false,"model":"role","plugin":"admin"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (14,'model_def_strapi::role','{"uid":"strapi::role","collectionName":"strapi_role","kind":"collectionType","info":{"name":"Role","description":""},"options":{"timestamps":["created_at","updated_at"]},"pluginOptions":{"content-manager":{"visible":false},"content-type-builder":{"visible":false}},"attributes":{"name":{"type":"string","minLength":1,"unique":true,"configurable":false,"required":true},"code":{"type":"string","minLength":1,"unique":true,"configurable":false,"required":true},"description":{"type":"string","configurable":false},"users":{"configurable":false,"collection":"user","via":"roles","plugin":"admin","attribute":"user","column":"id","isVirtual":true},"permissions":{"configurable":false,"plugin":"admin","collection":"permission","via":"role","isVirtual":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (15,'model_def_strapi::user','{"uid":"strapi::user","collectionName":"strapi_administrator","kind":"collectionType","info":{"name":"User","description":""},"options":{"timestamps":false},"pluginOptions":{"content-manager":{"visible":false},"content-type-builder":{"visible":false}},"attributes":{"firstname":{"type":"string","unique":false,"minLength":1,"configurable":false,"required":false},"lastname":{"type":"string","unique":false,"minLength":1,"configurable":false,"required":false},"username":{"type":"string","unique":false,"configurable":false,"required":false},"email":{"type":"email","minLength":6,"configurable":false,"required":true,"unique":true,"private":true},"password":{"type":"password","minLength":6,"configurable":false,"required":false,"private":true},"resetPasswordToken":{"type":"string","configurable":false,"private":true},"registrationToken":{"type":"string","configurable":false,"private":true},"isActive":{"type":"boolean","default":false,"configurable":false,"private":true},"roles":{"collection":"role","collectionName":"strapi_users_roles","via":"users","dominant":true,"plugin":"admin","configurable":false,"private":true,"attribute":"role","column":"id","isVirtual":true},"blocked":{"type":"boolean","default":false,"configurable":false,"private":true},"preferedLanguage":{"type":"string","configurable":false,"required":false}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (16,'model_def_plugins::i18n.locale','{"uid":"plugins::i18n.locale","collectionName":"i18n_locales","kind":"collectionType","info":{"name":"locale","description":""},"options":{"timestamps":["created_at","updated_at"]},"pluginOptions":{"content-manager":{"visible":false},"content-type-builder":{"visible":false}},"attributes":{"name":{"type":"string","min":1,"max":50,"configurable":false},"code":{"type":"string","unique":true,"configurable":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (17,'model_def_plugins::upload.file','{"uid":"plugins::upload.file","collectionName":"upload_file","kind":"collectionType","info":{"name":"file","description":""},"options":{"timestamps":["created_at","updated_at"]},"pluginOptions":{"content-manager":{"visible":false},"content-type-builder":{"visible":false}},"attributes":{"name":{"type":"string","configurable":false,"required":true},"alternativeText":{"type":"string","configurable":false},"caption":{"type":"string","configurable":false},"width":{"type":"integer","configurable":false},"height":{"type":"integer","configurable":false},"formats":{"type":"json","configurable":false},"hash":{"type":"string","configurable":false,"required":true},"ext":{"type":"string","configurable":false},"mime":{"type":"string","configurable":false,"required":true},"size":{"type":"decimal","configurable":false,"required":true},"url":{"type":"string","configurable":false,"required":true},"previewUrl":{"type":"string","configurable":false},"provider":{"type":"string","configurable":false,"required":true},"provider_metadata":{"type":"json","configurable":false},"related":{"collection":"*","filter":"field","configurable":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (18,'model_def_plugins::users-permissions.permission','{"uid":"plugins::users-permissions.permission","collectionName":"users-permissions_permission","kind":"collectionType","info":{"name":"permission","description":""},"options":{"timestamps":false},"pluginOptions":{"content-manager":{"visible":false}},"attributes":{"type":{"type":"string","required":true,"configurable":false},"controller":{"type":"string","required":true,"configurable":false},"action":{"type":"string","required":true,"configurable":false},"enabled":{"type":"boolean","required":true,"configurable":false},"policy":{"type":"string","configurable":false},"role":{"model":"role","via":"permissions","plugin":"users-permissions","configurable":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (19,'model_def_plugins::users-permissions.role','{"uid":"plugins::users-permissions.role","collectionName":"users-permissions_role","kind":"collectionType","info":{"name":"role","description":""},"options":{"timestamps":false},"pluginOptions":{"content-manager":{"visible":false}},"attributes":{"name":{"type":"string","minLength":3,"required":true,"configurable":false},"description":{"type":"string","configurable":false},"type":{"type":"string","unique":true,"configurable":false},"permissions":{"collection":"permission","via":"role","plugin":"users-permissions","configurable":false,"isVirtual":true},"users":{"collection":"user","via":"role","configurable":false,"plugin":"users-permissions","isVirtual":true},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (20,'model_def_plugins::users-permissions.user','{"uid":"plugins::users-permissions.user","collectionName":"users-permissions_user","kind":"collectionType","info":{"name":"user","description":""},"options":{"draftAndPublish":false,"timestamps":["created_at","updated_at"]},"attributes":{"username":{"type":"string","minLength":3,"unique":true,"configurable":false,"required":true},"email":{"type":"email","minLength":6,"configurable":false,"required":true},"provider":{"type":"string","configurable":false},"password":{"type":"password","minLength":6,"configurable":false,"private":true},"resetPasswordToken":{"type":"string","configurable":false,"private":true},"confirmationToken":{"type":"string","configurable":false,"private":true},"confirmed":{"type":"boolean","default":false,"configurable":false},"blocked":{"type":"boolean","default":false,"configurable":false},"role":{"model":"role","via":"users","plugin":"users-permissions","configurable":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (21,'plugin_upload_settings','{"sizeOptimization":true,"responsiveDimensions":true,"autoOrientation":true}','object','development','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (22,'plugin_users-permissions_grant','{"email":{"enabled":true,"icon":"envelope"},"discord":{"enabled":false,"icon":"discord","key":"","secret":"","callback":"/auth/discord/callback","scope":["identify","email"]},"facebook":{"enabled":false,"icon":"facebook-square","key":"","secret":"","callback":"/auth/facebook/callback","scope":["email"]},"google":{"enabled":false,"icon":"google","key":"","secret":"","callback":"/auth/google/callback","scope":["email"]},"github":{"enabled":false,"icon":"github","key":"","secret":"","callback":"/auth/github/callback","scope":["user","user:email"]},"microsoft":{"enabled":false,"icon":"windows","key":"","secret":"","callback":"/auth/microsoft/callback","scope":["user.read"]},"twitter":{"enabled":false,"icon":"twitter","key":"","secret":"","callback":"/auth/twitter/callback"},"instagram":{"enabled":false,"icon":"instagram","key":"","secret":"","callback":"/auth/instagram/callback","scope":["user_profile"]},"vk":{"enabled":false,"icon":"vk","key":"","secret":"","callback":"/auth/vk/callback","scope":["email"]},"twitch":{"enabled":false,"icon":"twitch","key":"","secret":"","callback":"/auth/twitch/callback","scope":["user:read:email"]},"linkedin":{"enabled":false,"icon":"linkedin","key":"","secret":"","callback":"/auth/linkedin/callback","scope":["r_liteprofile","r_emailaddress"]},"cognito":{"enabled":false,"icon":"aws","key":"","secret":"","subdomain":"my.subdomain.com","callback":"/auth/cognito/callback","scope":["email","openid","profile"]},"reddit":{"enabled":false,"icon":"reddit","key":"","secret":"","state":true,"callback":"/auth/reddit/callback","scope":["identity"]},"auth0":{"enabled":false,"icon":"","key":"","secret":"","subdomain":"my-tenant.eu","callback":"/auth/auth0/callback","scope":["openid","email","profile"]},"cas":{"enabled":false,"icon":"book","key":"","secret":"","callback":"/auth/cas/callback","scope":["openid email"],"subdomain":"my.subdomain.com/cas"}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (23,'plugin_i18n_default_locale','"en"','string','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (24,'plugin_content_manager_configuration_components::links.normal-link','{"uid":"links.normal-link","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"href","defaultSortBy":"href","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"href":{"edit":{"label":"Href","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Href","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"target":{"edit":{"label":"Target","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Target","searchable":true,"sortable":true}}},"layouts":{"list":["id","href","name","target"],"edit":[[{"name":"href","size":6},{"name":"name","size":6}],[{"name":"target","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (25,'plugin_content_manager_configuration_components::links.social-link','{"uid":"links.social-link","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"icon":{"edit":{"label":"Icon","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Icon","searchable":false,"sortable":false}},"link":{"edit":{"label":"Link","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Link","searchable":false,"sortable":false}}},"layouts":{"list":["id"],"edit":[[{"name":"icon","size":12}],[{"name":"link","size":12}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (26,'plugin_content_manager_configuration_components::misc.custom-enumeration','{"uid":"misc.custom-enumeration","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"text","defaultSortBy":"text","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"text":{"edit":{"label":"Text","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Text","searchable":true,"sortable":true}}},"layouts":{"list":["id","text"],"edit":[[{"name":"text","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (27,'plugin_content_manager_configuration_components::misc.details','{"uid":"misc.details","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"item":{"edit":{"label":"Item","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Item","searchable":false,"sortable":false}}},"layouts":{"list":["id","title"],"edit":[[{"name":"title","size":6}],[{"name":"item","size":12}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (28,'plugin_content_manager_configuration_components::misc.icon','{"uid":"misc.icon","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}}},"layouts":{"list":["id","type","name"],"edit":[[{"name":"type","size":6},{"name":"name","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (29,'plugin_content_manager_configuration_components::misc.pro-con','{"uid":"misc.pro-con","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"pros","defaultSortBy":"pros","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"pros":{"edit":{"label":"Pros","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Pros","searchable":true,"sortable":true}},"cons":{"edit":{"label":"Cons","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Cons","searchable":true,"sortable":true}}},"layouts":{"list":["id","pros","cons"],"edit":[[{"name":"pros","size":6},{"name":"cons","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (30,'plugin_content_manager_configuration_components::misc.rating','{"uid":"misc.rating","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"percentage":{"edit":{"label":"Percentage","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Percentage","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","percentage"],"edit":[[{"name":"title","size":6},{"name":"percentage","size":4}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (31,'plugin_users-permissions_email','{"reset_password":{"display":"Email.template.reset_password","icon":"sync","options":{"from":{"name":"Administration Panel","email":"no-reply@strapi.io"},"response_email":"","object":"Reset password","message":"<p>We heard that you lost your password. Sorry about that!</p>\n\n<p>But don’t worry! You can use the following link to reset your password:</p>\n<p><%= URL %>?code=<%= TOKEN %></p>\n\n<p>Thanks.</p>"}},"email_confirmation":{"display":"Email.template.email_confirmation","icon":"check-square","options":{"from":{"name":"Administration Panel","email":"no-reply@strapi.io"},"response_email":"","object":"Account confirmation","message":"<p>Thank you for registering!</p>\n\n<p>You have to confirm your email address. Please click on the link below.</p>\n\n<p><%= URL %>?confirmation=<%= CODE %></p>\n\n<p>Thanks.</p>"}}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (32,'plugin_content_manager_configuration_content_types::application::casino.casino','{"uid":"application::casino.casino","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"thumbnail":{"edit":{"label":"Thumbnail","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Thumbnail","searchable":false,"sortable":false}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"about":{"edit":{"label":"About","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"About","searchable":false,"sortable":false}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"experiences":{"edit":{"label":"Experiences","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Experiences","searchable":false,"sortable":false}},"likes":{"edit":{"label":"Likes","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Likes","searchable":true,"sortable":true}},"referralLink":{"edit":{"label":"ReferralLink","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"ReferralLink","searchable":true,"sortable":true}},"socials":{"edit":{"label":"Socials","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Socials","searchable":false,"sortable":false}},"active":{"edit":{"label":"Active","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Active","searchable":true,"sortable":true}},"acceptedCountries":{"edit":{"label":"AcceptedCountries","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AcceptedCountries","searchable":false,"sortable":false}},"slug":{"edit":{"label":"Slug","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slug","searchable":true,"sortable":true}},"tags":{"edit":{"label":"Tags","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Tags","searchable":false,"sortable":false}},"slots":{"edit":{"label":"Slots","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Slots","searchable":false,"sortable":false}},"cover":{"edit":{"label":"Cover","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Cover","searchable":false,"sortable":false}},"review":{"edit":{"label":"Review","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"Review","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","thumbnail","description"],"edit":[[{"name":"title","size":6},{"name":"likes","size":4}],[{"name":"description","size":6},{"name":"type","size":6}],[{"name":"about","size":12}],[{"name":"thumbnail","size":6}],[{"name":"experiences","size":12}],[{"name":"socials","size":12}],[{"name":"active","size":4}],[{"name":"acceptedCountries","size":12}],[{"name":"slug","size":6},{"name":"referralLink","size":6}],[{"name":"cover","size":6}]],"editRelations":["tags","slots","review"]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (33,'plugin_content_manager_configuration_content_types::application::tag-types.tag-types','{"uid":"application::tag-types.tag-types","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"slug":{"edit":{"label":"Slug","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slug","searchable":true,"sortable":true}},"tags":{"edit":{"label":"Tags","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Tags","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","slug","tags"],"editRelations":["tags"],"edit":[[{"name":"title","size":6},{"name":"slug","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (34,'plugin_content_manager_configuration_content_types::application::tags.tags','{"uid":"application::tags.tags","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"slug":{"edit":{"label":"Slug","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slug","searchable":true,"sortable":true}},"tag_type":{"edit":{"label":"Tag_type","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Tag_type","searchable":true,"sortable":true}},"casinos":{"edit":{"label":"Casinos","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Casinos","searchable":false,"sortable":false}},"bookmakers":{"edit":{"label":"Bookmakers","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Bookmakers","searchable":false,"sortable":false}},"bonuses":{"edit":{"label":"Bonuses","description":"","placeholder":"","visible":true,"editable":true,"mainField":"bonus"},"list":{"label":"Bonuses","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","slug","tag_type"],"edit":[[{"name":"title","size":6},{"name":"slug","size":6}]],"editRelations":["tag_type","casinos","bonuses","bookmakers"]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (35,'plugin_content_manager_configuration_content_types::strapi::permission','{"uid":"strapi::permission","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"action","defaultSortBy":"action","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"action":{"edit":{"label":"Action","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Action","searchable":true,"sortable":true}},"subject":{"edit":{"label":"Subject","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Subject","searchable":true,"sortable":true}},"properties":{"edit":{"label":"Properties","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Properties","searchable":false,"sortable":false}},"conditions":{"edit":{"label":"Conditions","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Conditions","searchable":false,"sortable":false}},"role":{"edit":{"label":"Role","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Role","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","action","subject","role"],"editRelations":["role"],"edit":[[{"name":"action","size":6},{"name":"subject","size":6}],[{"name":"properties","size":12}],[{"name":"conditions","size":12}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (36,'plugin_content_manager_configuration_content_types::strapi::role','{"uid":"strapi::role","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"code":{"edit":{"label":"Code","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Code","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"users":{"edit":{"label":"Users","description":"","placeholder":"","visible":true,"editable":true,"mainField":"firstname"},"list":{"label":"Users","searchable":false,"sortable":false}},"permissions":{"edit":{"label":"Permissions","description":"","placeholder":"","visible":true,"editable":true,"mainField":"action"},"list":{"label":"Permissions","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","code","description"],"editRelations":["users","permissions"],"edit":[[{"name":"name","size":6},{"name":"code","size":6}],[{"name":"description","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (37,'plugin_content_manager_configuration_content_types::strapi::user','{"uid":"strapi::user","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"firstname","defaultSortBy":"firstname","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"firstname":{"edit":{"label":"Firstname","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Firstname","searchable":true,"sortable":true}},"lastname":{"edit":{"label":"Lastname","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Lastname","searchable":true,"sortable":true}},"username":{"edit":{"label":"Username","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Username","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"password":{"edit":{"label":"Password","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Password","searchable":true,"sortable":true}},"resetPasswordToken":{"edit":{"label":"ResetPasswordToken","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"ResetPasswordToken","searchable":true,"sortable":true}},"registrationToken":{"edit":{"label":"RegistrationToken","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"RegistrationToken","searchable":true,"sortable":true}},"isActive":{"edit":{"label":"IsActive","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"IsActive","searchable":true,"sortable":true}},"roles":{"edit":{"label":"Roles","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Roles","searchable":false,"sortable":false}},"blocked":{"edit":{"label":"Blocked","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Blocked","searchable":true,"sortable":true}},"preferedLanguage":{"edit":{"label":"PreferedLanguage","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"PreferedLanguage","searchable":true,"sortable":true}}},"layouts":{"list":["id","firstname","lastname","username"],"editRelations":["roles"],"edit":[[{"name":"firstname","size":6},{"name":"lastname","size":6}],[{"name":"username","size":6},{"name":"email","size":6}],[{"name":"password","size":6},{"name":"resetPasswordToken","size":6}],[{"name":"registrationToken","size":6},{"name":"isActive","size":4}],[{"name":"blocked","size":4},{"name":"preferedLanguage","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (38,'plugin_content_manager_configuration_content_types::plugins::i18n.locale','{"uid":"plugins::i18n.locale","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"code":{"edit":{"label":"Code","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Code","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","code","created_at"],"editRelations":[],"edit":[[{"name":"name","size":6},{"name":"code","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (39,'plugin_content_manager_configuration_content_types::plugins::upload.file','{"uid":"plugins::upload.file","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"alternativeText":{"edit":{"label":"AlternativeText","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AlternativeText","searchable":true,"sortable":true}},"caption":{"edit":{"label":"Caption","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Caption","searchable":true,"sortable":true}},"width":{"edit":{"label":"Width","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Width","searchable":true,"sortable":true}},"height":{"edit":{"label":"Height","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Height","searchable":true,"sortable":true}},"formats":{"edit":{"label":"Formats","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Formats","searchable":false,"sortable":false}},"hash":{"edit":{"label":"Hash","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Hash","searchable":true,"sortable":true}},"ext":{"edit":{"label":"Ext","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Ext","searchable":true,"sortable":true}},"mime":{"edit":{"label":"Mime","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Mime","searchable":true,"sortable":true}},"size":{"edit":{"label":"Size","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Size","searchable":true,"sortable":true}},"url":{"edit":{"label":"Url","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Url","searchable":true,"sortable":true}},"previewUrl":{"edit":{"label":"PreviewUrl","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"PreviewUrl","searchable":true,"sortable":true}},"provider":{"edit":{"label":"Provider","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Provider","searchable":true,"sortable":true}},"provider_metadata":{"edit":{"label":"Provider_metadata","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Provider_metadata","searchable":false,"sortable":false}},"related":{"edit":{"label":"Related","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Related","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","alternativeText","caption"],"editRelations":["related"],"edit":[[{"name":"name","size":6},{"name":"alternativeText","size":6}],[{"name":"caption","size":6},{"name":"width","size":4}],[{"name":"height","size":4}],[{"name":"formats","size":12}],[{"name":"hash","size":6},{"name":"ext","size":6}],[{"name":"mime","size":6},{"name":"size","size":4}],[{"name":"url","size":6},{"name":"previewUrl","size":6}],[{"name":"provider","size":6}],[{"name":"provider_metadata","size":12}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (40,'plugin_content_manager_configuration_content_types::plugins::users-permissions.permission','{"uid":"plugins::users-permissions.permission","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"type","defaultSortBy":"type","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"controller":{"edit":{"label":"Controller","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Controller","searchable":true,"sortable":true}},"action":{"edit":{"label":"Action","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Action","searchable":true,"sortable":true}},"enabled":{"edit":{"label":"Enabled","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Enabled","searchable":true,"sortable":true}},"policy":{"edit":{"label":"Policy","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Policy","searchable":true,"sortable":true}},"role":{"edit":{"label":"Role","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Role","searchable":true,"sortable":true}}},"layouts":{"list":["id","type","controller","action"],"editRelations":["role"],"edit":[[{"name":"type","size":6},{"name":"controller","size":6}],[{"name":"action","size":6},{"name":"enabled","size":4}],[{"name":"policy","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (41,'plugin_content_manager_configuration_content_types::plugins::users-permissions.role','{"uid":"plugins::users-permissions.role","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"permissions":{"edit":{"label":"Permissions","description":"","placeholder":"","visible":true,"editable":true,"mainField":"type"},"list":{"label":"Permissions","searchable":false,"sortable":false}},"users":{"edit":{"label":"Users","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"Users","searchable":false,"sortable":false}}},"layouts":{"list":["id","name","description","type"],"editRelations":["permissions","users"],"edit":[[{"name":"name","size":6},{"name":"description","size":6}],[{"name":"type","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (42,'plugin_content_manager_configuration_content_types::plugins::users-permissions.user','{"uid":"plugins::users-permissions.user","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"username","defaultSortBy":"username","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"username":{"edit":{"label":"Username","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Username","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"provider":{"edit":{"label":"Provider","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Provider","searchable":true,"sortable":true}},"password":{"edit":{"label":"Password","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Password","searchable":true,"sortable":true}},"resetPasswordToken":{"edit":{"label":"ResetPasswordToken","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"ResetPasswordToken","searchable":true,"sortable":true}},"confirmationToken":{"edit":{"label":"ConfirmationToken","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"ConfirmationToken","searchable":true,"sortable":true}},"confirmed":{"edit":{"label":"Confirmed","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Confirmed","searchable":true,"sortable":true}},"blocked":{"edit":{"label":"Blocked","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Blocked","searchable":true,"sortable":true}},"role":{"edit":{"label":"Role","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Role","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","username","email","confirmed"],"editRelations":["role"],"edit":[[{"name":"username","size":6},{"name":"email","size":6}],[{"name":"password","size":6},{"name":"confirmed","size":4}],[{"name":"blocked","size":4}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (43,'plugin_users-permissions_advanced','{"unique_email":true,"allow_register":true,"email_confirmation":false,"email_reset_password":null,"email_confirmation_redirection":null,"default_role":"authenticated"}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (44,'core_admin_auth','{"providers":{"autoRegister":false,"defaultRole":null}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (45,'model_def_links.mega-menu-link','{"uid":"links.mega-menu-link","collectionName":"components_links_mega_menu_links","info":{"name":"MegaMenuLink","icon":"unlink"},"options":{"timestamps":false},"attributes":{"title":{"type":"string"},"links":{"type":"component","repeatable":true,"component":"links.normal-link"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (46,'plugin_content_manager_configuration_components::links.mega-menu-link','{"uid":"links.mega-menu-link","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"links":{"edit":{"label":"Links","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Links","searchable":false,"sortable":false}}},"layouts":{"list":["id","title"],"edit":[[{"name":"title","size":6}],[{"name":"links","size":12}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (47,'model_def_application::nav-links.nav-links','{"uid":"application::nav-links.nav-links","collectionName":"nav_links","kind":"collectionType","info":{"name":"NavLinks","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"submenu":{"type":"dynamiczone","components":["links.mega-menu-link","links.normal-link"]},"badge":{"type":"component","repeatable":false,"component":"misc.badge"},"link":{"type":"component","repeatable":false,"component":"links.normal-link"},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (48,'plugin_content_manager_configuration_content_types::application::nav-links.nav-links','{"uid":"application::nav-links.nav-links","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"submenu":{"edit":{"label":"Submenu","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Submenu","searchable":false,"sortable":false}},"badge":{"edit":{"label":"Badge","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Badge","searchable":false,"sortable":false}},"link":{"edit":{"label":"Link","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Link","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at"],"edit":[[{"name":"submenu","size":12}],[{"name":"badge","size":12}],[{"name":"link","size":12}]],"editRelations":[]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (49,'model_def_misc.badge','{"uid":"misc.badge","collectionName":"components_misc_badges","info":{"name":"Badge","icon":"adjust"},"options":{"timestamps":false},"attributes":{"text":{"type":"string"},"type":{"type":"enumeration","enum":["hot","new","newCount"]}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (50,'plugin_content_manager_configuration_components::misc.badge','{"uid":"misc.badge","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"text","defaultSortBy":"text","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"text":{"edit":{"label":"Text","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Text","searchable":true,"sortable":true}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}}},"layouts":{"list":["id","text","type"],"edit":[[{"name":"text","size":6},{"name":"type","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (51,'model_def_application::test.test','{"uid":"application::test.test","collectionName":"blogs","kind":"collectionType","info":{"name":"Blog","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"title":{"type":"string"},"content":{"type":"richtext"},"thumbnail":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"author":{"plugin":"admin","model":"user"},"slug":{"type":"string"},"tags":{"collection":"blog-tags","attribute":"blog-tag","column":"id","isVirtual":true},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (53,'model_def_application::slot.slot','{"uid":"application::slot.slot","collectionName":"slots","kind":"collectionType","info":{"name":"Slot","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"title":{"type":"string"},"casino":{"via":"slots","model":"casino"},"description":{"type":"richtext"},"about":{"type":"richtext"},"experiences":{"type":"component","repeatable":true,"component":"misc.pro-con"},"features":{"type":"component","repeatable":true,"component":"misc.custom-enumeration"},"tags":{"collection":"tag-types","attribute":"tag-type","column":"id","isVirtual":true},"type":{"type":"enumeration","enum":["classic","esport","fruit_games","games","progressive","video"]},"thumbnail":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"shortDescription":{"type":"string","default":"Jackpot Night is an Online Casino."},"review":{"model":"review"},"comments":{"collection":"comment","attribute":"comment","column":"id","isVirtual":true},"link":{"type":"component","repeatable":false,"component":"links.normal-link"},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (54,'plugin_content_manager_configuration_content_types::application::slot.slot','{"uid":"application::slot.slot","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"casino":{"edit":{"label":"Casino","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Casino","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":false,"sortable":false}},"about":{"edit":{"label":"About","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"About","searchable":false,"sortable":false}},"experiences":{"edit":{"label":"Experiences","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Experiences","searchable":false,"sortable":false}},"features":{"edit":{"label":"Features","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Features","searchable":false,"sortable":false}},"tags":{"edit":{"label":"Tags","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Tags","searchable":false,"sortable":false}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"thumbnail":{"edit":{"label":"Thumbnail","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Thumbnail","searchable":false,"sortable":false}},"shortDescription":{"edit":{"label":"ShortDescription","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"ShortDescription","searchable":true,"sortable":true}},"review":{"edit":{"label":"Review","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Review","searchable":true,"sortable":true}},"comments":{"edit":{"label":"Comments","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Comments","searchable":false,"sortable":false}},"link":{"edit":{"label":"Link","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Link","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","casino","tags"],"edit":[[{"name":"title","size":6},{"name":"type","size":6}],[{"name":"thumbnail","size":6}],[{"name":"description","size":12}],[{"name":"about","size":12}],[{"name":"experiences","size":12}],[{"name":"features","size":12}],[{"name":"shortDescription","size":6}],[{"name":"link","size":12}]],"editRelations":["casino","tags","review","comments"]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (55,'model_def_application::comment.comment','{"uid":"application::comment.comment","collectionName":"comments","kind":"collectionType","info":{"name":"comment"},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"content":{"type":"text"},"name":{"type":"string"},"email":{"type":"email"},"website":{"type":"string"},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (56,'plugin_content_manager_configuration_content_types::application::comment.comment','{"uid":"application::comment.comment","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"content":{"edit":{"label":"Content","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Content","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"website":{"edit":{"label":"Website","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Website","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","content","name","email"],"editRelations":[],"edit":[[{"name":"content","size":6},{"name":"name","size":6}],[{"name":"email","size":6},{"name":"website","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (57,'model_def_application::bonus.bonus','{"uid":"application::bonus.bonus","collectionName":"bonuses","kind":"collectionType","info":{"name":"Bonus"},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"bonus":{"type":"string"},"title":{"type":"string"},"bonusLink":{"type":"string"},"slug":{"type":"string"},"background":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"rewards":{"type":"richtext"},"shortDesc":{"type":"string"},"about":{"type":"richtext"},"experiences":{"type":"component","repeatable":true,"component":"misc.pro-con"},"features":{"type":"component","repeatable":true,"component":"misc.custom-enumeration"},"tags":{"collection":"tags","via":"bonuses","dominant":true,"attribute":"tag","column":"id","isVirtual":true},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (58,'plugin_content_manager_configuration_content_types::application::bonus.bonus','{"uid":"application::bonus.bonus","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"bonus","defaultSortBy":"bonus","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"bonus":{"edit":{"label":"Bonus","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Bonus","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"bonusLink":{"edit":{"label":"BonusLink","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"BonusLink","searchable":true,"sortable":true}},"slug":{"edit":{"label":"Slug","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slug","searchable":true,"sortable":true}},"background":{"edit":{"label":"Background","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Background","searchable":false,"sortable":false}},"rewards":{"edit":{"label":"Rewards","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Rewards","searchable":false,"sortable":false}},"shortDesc":{"edit":{"label":"ShortDesc","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"ShortDesc","searchable":true,"sortable":true}},"about":{"edit":{"label":"About","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"About","searchable":false,"sortable":false}},"experiences":{"edit":{"label":"Experiences","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Experiences","searchable":false,"sortable":false}},"features":{"edit":{"label":"Features","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Features","searchable":false,"sortable":false}},"tags":{"edit":{"label":"Tags","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Tags","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","bonus","title","bonusLink"],"editRelations":["tags"],"edit":[[{"name":"bonus","size":6},{"name":"title","size":6}],[{"name":"bonusLink","size":6},{"name":"slug","size":6}],[{"name":"background","size":6}],[{"name":"rewards","size":12}],[{"name":"shortDesc","size":6}],[{"name":"about","size":12}],[{"name":"experiences","size":12}],[{"name":"features","size":12}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (59,'model_def_application::bookmaker.bookmaker','{"uid":"application::bookmaker.bookmaker","collectionName":"bookmaker","kind":"collectionType","info":{"name":"Bookmaker","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"title":{"type":"string","required":true},"thumbnail":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"description":{"type":"string"},"about":{"type":"richtext"},"type":{"type":"enumeration","enum":["affiliate","certified","download","live","mobile","online"],"required":true},"experiences":{"type":"component","repeatable":true,"component":"misc.pro-con"},"rating":{"type":"component","repeatable":true,"component":"misc.rating"},"likes":{"type":"integer","default":0},"referralLink":{"type":"string","private":false},"socials":{"type":"component","repeatable":true,"component":"links.social-link"},"active":{"type":"boolean","default":true,"required":true},"acceptedCountries":{"type":"component","repeatable":true,"component":"misc.custom-enumeration"},"slug":{"type":"string"},"tags":{"collection":"tags","via":"bookmakers","dominant":true,"attribute":"tag","column":"id","isVirtual":true},"cover":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (60,'plugin_content_manager_configuration_content_types::application::bookmaker.bookmaker','{"uid":"application::bookmaker.bookmaker","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"thumbnail":{"edit":{"label":"Thumbnail","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Thumbnail","searchable":false,"sortable":false}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"about":{"edit":{"label":"About","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"About","searchable":false,"sortable":false}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"experiences":{"edit":{"label":"Experiences","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Experiences","searchable":false,"sortable":false}},"rating":{"edit":{"label":"Rating","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Rating","searchable":false,"sortable":false}},"likes":{"edit":{"label":"Likes","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Likes","searchable":true,"sortable":true}},"referralLink":{"edit":{"label":"ReferralLink","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"ReferralLink","searchable":true,"sortable":true}},"socials":{"edit":{"label":"Socials","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Socials","searchable":false,"sortable":false}},"active":{"edit":{"label":"Active","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Active","searchable":true,"sortable":true}},"acceptedCountries":{"edit":{"label":"AcceptedCountries","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AcceptedCountries","searchable":false,"sortable":false}},"slug":{"edit":{"label":"Slug","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slug","searchable":true,"sortable":true}},"tags":{"edit":{"label":"Tags","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Tags","searchable":false,"sortable":false}},"cover":{"edit":{"label":"Cover","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Cover","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","created_at","updated_at"],"edit":[[{"name":"title","size":6},{"name":"thumbnail","size":6}],[{"name":"description","size":6}],[{"name":"about","size":12}],[{"name":"type","size":6}],[{"name":"experiences","size":12}],[{"name":"rating","size":12}],[{"name":"likes","size":4},{"name":"referralLink","size":6}],[{"name":"socials","size":12}],[{"name":"active","size":4}],[{"name":"acceptedCountries","size":12}],[{"name":"slug","size":6},{"name":"cover","size":6}]],"editRelations":["tags"]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (62,'model_def_application::blog-tags.blog-tags','{"uid":"application::blog-tags.blog-tags","collectionName":"blog_tags","kind":"collectionType","info":{"name":"BlogTags"},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"name":{"type":"string"},"slug":{"type":"string"},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (63,'plugin_content_manager_configuration_content_types::application::blog-tags.blog-tags','{"uid":"application::blog-tags.blog-tags","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"slug":{"edit":{"label":"Slug","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slug","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","slug","created_at"],"editRelations":[],"edit":[[{"name":"name","size":6},{"name":"slug","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (64,'model_def_application::home-header.home-header','{"uid":"application::home-header.home-header","collectionName":"home_headers","kind":"singleType","info":{"name":"HOME_HEADER","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"title":{"type":"string"},"description":{"type":"string"},"email":{"type":"string"},"button":{"type":"component","repeatable":false,"component":"links.normal-link"},"image":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (65,'plugin_content_manager_configuration_content_types::application::home-header.home-header','{"uid":"application::home-header.home-header","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"button":{"edit":{"label":"Button","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Button","searchable":false,"sortable":false}},"image":{"edit":{"label":"Image","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Image","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","description","email"],"edit":[[{"name":"title","size":6},{"name":"description","size":6}],[{"name":"email","size":6}],[{"name":"button","size":12}],[{"name":"image","size":6}]],"editRelations":[]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (66,'model_def_misc.characteristics','{"uid":"misc.characteristics","collectionName":"components_misc_characteristics","info":{"name":"characteristic","icon":"align-center","description":""},"options":{"timestamps":false},"attributes":{"title":{"type":"string"},"description":{"type":"text"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (67,'plugin_content_manager_configuration_components::misc.characteristics','{"uid":"misc.characteristics","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","description"],"edit":[[{"name":"title","size":6},{"name":"description","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (68,'model_def_application::home-characteristics.home-characteristics','{"uid":"application::home-characteristics.home-characteristics","collectionName":"home_characteristics","kind":"singleType","info":{"name":"HOME_CHARACTERISTICS","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"characteristics":{"type":"component","repeatable":true,"component":"misc.characteristics","max":3,"required":true},"sectionTitle":{"type":"component","repeatable":false,"component":"misc.section-title"},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (69,'plugin_content_manager_configuration_content_types::application::home-characteristics.home-characteristics','{"uid":"application::home-characteristics.home-characteristics","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"characteristics":{"edit":{"label":"Characteristics","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Characteristics","searchable":false,"sortable":false}},"sectionTitle":{"edit":{"label":"SectionTitle","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"SectionTitle","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at"],"edit":[[{"name":"characteristics","size":12}],[{"name":"sectionTitle","size":12}]],"editRelations":[]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (70,'model_def_misc.section-title','{"uid":"misc.section-title","collectionName":"components_misc_section_titles","info":{"name":"SectionTitle","icon":"allergies"},"options":{"timestamps":false},"attributes":{"title":{"type":"string"},"subtitle":{"type":"string"},"description":{"type":"text"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (71,'plugin_content_manager_configuration_components::misc.section-title','{"uid":"misc.section-title","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"subtitle":{"edit":{"label":"Subtitle","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Subtitle","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","subtitle","description"],"edit":[[{"name":"title","size":6},{"name":"subtitle","size":6}],[{"name":"description","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (72,'model_def_misc.faq','{"uid":"misc.faq","collectionName":"components_misc_faqs","info":{"name":"FAQ","icon":"question"},"options":{"timestamps":false},"attributes":{"question":{"type":"string"},"answer":{"type":"richtext"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (73,'plugin_content_manager_configuration_components::misc.faq','{"uid":"misc.faq","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"question","defaultSortBy":"question","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"question":{"edit":{"label":"Question","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Question","searchable":true,"sortable":true}},"answer":{"edit":{"label":"Answer","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Answer","searchable":false,"sortable":false}}},"layouts":{"list":["id","question"],"edit":[[{"name":"question","size":6}],[{"name":"answer","size":12}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (74,'model_def_application::home-faq.home-faq','{"uid":"application::home-faq.home-faq","collectionName":"home_faqs","kind":"singleType","info":{"name":"HOME_FAQ","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"faqList":{"type":"component","repeatable":true,"component":"misc.faq","max":3},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (75,'plugin_content_manager_configuration_content_types::application::home-faq.home-faq','{"uid":"application::home-faq.home-faq","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"faqList":{"edit":{"label":"FaqList","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"FaqList","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at"],"editRelations":[],"edit":[[{"name":"faqList","size":12}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (76,'model_def_misc.testimonial','{"uid":"misc.testimonial","collectionName":"components_misc_testimonials","info":{"name":"Testimonial","icon":"assistive-listening-systems"},"options":{"timestamps":false},"attributes":{"author":{"type":"string"},"position":{"type":"string"},"message":{"type":"text"},"avatar":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (77,'plugin_content_manager_configuration_components::misc.testimonial','{"uid":"misc.testimonial","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"author","defaultSortBy":"author","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"author":{"edit":{"label":"Author","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Author","searchable":true,"sortable":true}},"position":{"edit":{"label":"Position","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Position","searchable":true,"sortable":true}},"message":{"edit":{"label":"Message","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Message","searchable":true,"sortable":true}},"avatar":{"edit":{"label":"Avatar","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Avatar","searchable":false,"sortable":false}}},"layouts":{"list":["id","author","position","message"],"edit":[[{"name":"author","size":6},{"name":"position","size":6}],[{"name":"message","size":6},{"name":"avatar","size":6}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (78,'model_def_application::home-testimonials.home-testimonials','{"uid":"application::home-testimonials.home-testimonials","collectionName":"home_testimonials","kind":"singleType","info":{"name":"HOME_TESTIMONIALS","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"testimonials":{"type":"component","repeatable":true,"component":"misc.testimonial","max":5},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (79,'plugin_content_manager_configuration_content_types::application::home-testimonials.home-testimonials','{"uid":"application::home-testimonials.home-testimonials","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"testimonials":{"edit":{"label":"Testimonials","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Testimonials","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at"],"editRelations":[],"edit":[[{"name":"testimonials","size":12}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (80,'model_def_misc.get-in-touch','{"uid":"misc.get-in-touch","collectionName":"components_misc_get_in_touches","info":{"name":"GetInTouch","icon":"anchor","description":""},"options":{"timestamps":false},"attributes":{"title":{"type":"string"},"description":{"type":"string"},"link":{"type":"component","repeatable":false,"component":"links.normal-link"},"icon":{"type":"component","repeatable":true,"component":"misc.icon"}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (81,'plugin_content_manager_configuration_components::misc.get-in-touch','{"uid":"misc.get-in-touch","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":false,"sortable":false}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"link":{"edit":{"label":"Link","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Link","searchable":false,"sortable":false}},"icon":{"edit":{"label":"Icon","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Icon","searchable":false,"sortable":false}}},"layouts":{"list":["id","title","description"],"edit":[[{"name":"title","size":6},{"name":"description","size":6}],[{"name":"link","size":12}],[{"name":"icon","size":12}]],"editRelations":[]},"isComponent":true}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (82,'model_def_application::home-get-in-touch.home-get-in-touch','{"uid":"application::home-get-in-touch.home-get-in-touch","collectionName":"home_get_in_touches","kind":"singleType","info":{"name":"HOME_GET_IN_TOUCH","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"content":{"type":"component","repeatable":true,"component":"misc.get-in-touch","max":3},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (83,'plugin_content_manager_configuration_content_types::application::home-get-in-touch.home-get-in-touch','{"uid":"application::home-get-in-touch.home-get-in-touch","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"content":{"edit":{"label":"Content","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Content","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at"],"edit":[[{"name":"content","size":12}]],"editRelations":[]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (84,'model_def_application::home-partners.home-partners','{"uid":"application::home-partners.home-partners","collectionName":"home_partners","kind":"singleType","info":{"name":"HOME_PARTNERS"},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"slider":{"collection":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (85,'plugin_content_manager_configuration_content_types::application::home-partners.home-partners','{"uid":"application::home-partners.home-partners","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"slider":{"edit":{"label":"Slider","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slider","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","slider","created_at","updated_at"],"editRelations":[],"edit":[[{"name":"slider","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (86,'model_def_application::footer.footer','{"uid":"application::footer.footer","collectionName":"footers","kind":"singleType","info":{"name":"FOOTER","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"aboutUs":{"type":"text"},"email":{"type":"email"},"phoneNo":{"type":"string"},"socials":{"type":"component","repeatable":true,"component":"links.social-link"},"subscribeText":{"type":"string"},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (87,'plugin_content_manager_configuration_content_types::application::footer.footer','{"uid":"application::footer.footer","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"aboutUs":{"edit":{"label":"AboutUs","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AboutUs","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"phoneNo":{"edit":{"label":"PhoneNo","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"PhoneNo","searchable":true,"sortable":true}},"socials":{"edit":{"label":"Socials","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Socials","searchable":false,"sortable":false}},"subscribeText":{"edit":{"label":"SubscribeText","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"SubscribeText","searchable":true,"sortable":true}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at","aboutUs"],"edit":[[{"name":"aboutUs","size":6},{"name":"email","size":6}],[{"name":"phoneNo","size":6}],[{"name":"socials","size":12}],[{"name":"subscribeText","size":6}]],"editRelations":[]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (88,'model_def_application::menu.menu','{"uid":"application::menu.menu","collectionName":"menus","kind":"singleType","info":{"name":"MENU"},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"socials":{"type":"component","repeatable":true,"component":"links.social-link"},"logo":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"siteName":{"type":"string"},"navLinks":{"collection":"nav-links","attribute":"nav-link","column":"id","isVirtual":true},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (89,'plugin_content_manager_configuration_content_types::application::menu.menu','{"uid":"application::menu.menu","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"siteName","defaultSortBy":"siteName","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"socials":{"edit":{"label":"Socials","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Socials","searchable":false,"sortable":false}},"logo":{"edit":{"label":"Logo","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Logo","searchable":false,"sortable":false}},"siteName":{"edit":{"label":"SiteName","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"SiteName","searchable":true,"sortable":true}},"navLinks":{"edit":{"label":"NavLinks","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"NavLinks","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","logo","siteName","navLinks"],"editRelations":["navLinks"],"edit":[[{"name":"socials","size":12}],[{"name":"logo","size":6},{"name":"siteName","size":6}]]}}','object','','');
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (90,'model_def_application::review.review','{"uid":"application::review.review","collectionName":"reviews","kind":"collectionType","info":{"name":"Review","description":""},"options":{"increments":true,"timestamps":["created_at","updated_at"],"draftAndPublish":true},"pluginOptions":{},"attributes":{"rating":{"type":"component","repeatable":true,"component":"misc.rating"},"casinos":{"via":"review","collection":"casino","isVirtual":true},"content":{"type":"richtext"},"title":{"type":"string"},"slug":{"type":"string"},"thumbnail":{"model":"file","via":"related","allowedTypes":["images","files","videos"],"plugin":"upload","required":false,"pluginOptions":{}},"published_at":{"type":"datetime","configurable":false,"writable":true,"visible":false},"created_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true},"updated_by":{"model":"user","plugin":"admin","configurable":false,"writable":false,"visible":false,"private":true}}}','object',NULL,NULL);
INSERT INTO "core_store" ("id","key","value","type","environment","tag") VALUES (91,'plugin_content_manager_configuration_content_types::application::review.review','{"uid":"application::review.review","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"rating":{"edit":{"label":"Rating","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Rating","searchable":false,"sortable":false}},"casinos":{"edit":{"label":"Casinos","description":"","placeholder":"","visible":true,"editable":true,"mainField":"title"},"list":{"label":"Casinos","searchable":false,"sortable":false}},"content":{"edit":{"label":"Content","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Content","searchable":false,"sortable":false}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"slug":{"edit":{"label":"Slug","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Slug","searchable":true,"sortable":true}},"thumbnail":{"edit":{"label":"Thumbnail","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Thumbnail","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","casinos","created_at","updated_at"],"edit":[[{"name":"rating","size":12}],[{"name":"content","size":12}],[{"name":"title","size":6},{"name":"slug","size":6}],[{"name":"thumbnail","size":6}]],"editRelations":["casinos"]}}','object','','');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (1,'/','Link #1',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (2,'/','Link #2',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (3,'/','Link #3',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (4,'/','Link #4',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (5,'/','Home',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (6,'facebook',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (7,'instagram.com',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (8,'linkedin.com',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (9,'twitter.com',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (10,'/contact','Directions',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (11,'/contact','Send a message','');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (12,'/contact','Contact Us',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (13,'/contact','CONTACT US',NULL);
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (14,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (15,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (16,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (17,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (18,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (19,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (20,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (21,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (22,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (23,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (24,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (25,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (26,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (27,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (28,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (29,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (30,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (31,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (32,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (33,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (34,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (35,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (36,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (37,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (38,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (39,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (40,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (41,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (42,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (43,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (44,'/',NULL,'_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (45,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (46,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (47,'/','facebook','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (48,'/','reddit','_blank');
INSERT INTO "components_links_normal_links" ("id","href","name","target") VALUES (49,'/',NULL,'_blank');
INSERT INTO "components_links_social_links" ("id") VALUES (1);
INSERT INTO "components_links_social_links" ("id") VALUES (2);
INSERT INTO "components_links_social_links" ("id") VALUES (3);
INSERT INTO "components_links_social_links" ("id") VALUES (4);
INSERT INTO "components_links_social_links" ("id") VALUES (5);
INSERT INTO "components_links_social_links" ("id") VALUES (6);
INSERT INTO "components_links_social_links" ("id") VALUES (7);
INSERT INTO "components_links_social_links" ("id") VALUES (8);
INSERT INTO "components_links_social_links" ("id") VALUES (9);
INSERT INTO "components_links_social_links" ("id") VALUES (10);
INSERT INTO "components_links_social_links" ("id") VALUES (11);
INSERT INTO "components_links_social_links" ("id") VALUES (12);
INSERT INTO "components_links_social_links" ("id") VALUES (13);
INSERT INTO "components_links_social_links" ("id") VALUES (14);
INSERT INTO "components_links_social_links" ("id") VALUES (15);
INSERT INTO "components_links_social_links" ("id") VALUES (16);
INSERT INTO "components_links_social_links" ("id") VALUES (17);
INSERT INTO "components_links_social_links" ("id") VALUES (18);
INSERT INTO "components_links_social_links" ("id") VALUES (19);
INSERT INTO "components_links_social_links" ("id") VALUES (20);
INSERT INTO "components_links_social_links" ("id") VALUES (21);
INSERT INTO "components_links_social_links" ("id") VALUES (22);
INSERT INTO "components_links_social_links" ("id") VALUES (23);
INSERT INTO "components_links_social_links" ("id") VALUES (24);
INSERT INTO "components_links_social_links" ("id") VALUES (25);
INSERT INTO "components_links_social_links" ("id") VALUES (26);
INSERT INTO "components_links_social_links" ("id") VALUES (27);
INSERT INTO "components_links_social_links" ("id") VALUES (28);
INSERT INTO "components_links_social_links" ("id") VALUES (29);
INSERT INTO "components_links_social_links" ("id") VALUES (30);
INSERT INTO "components_links_social_links" ("id") VALUES (31);
INSERT INTO "components_links_social_links" ("id") VALUES (32);
INSERT INTO "components_links_social_links" ("id") VALUES (33);
INSERT INTO "components_links_social_links" ("id") VALUES (34);
INSERT INTO "components_links_social_links" ("id") VALUES (35);
INSERT INTO "components_links_social_links" ("id") VALUES (36);
INSERT INTO "components_links_social_links" ("id") VALUES (37);
INSERT INTO "components_links_social_links" ("id") VALUES (38);
INSERT INTO "components_links_social_links" ("id") VALUES (39);
INSERT INTO "components_links_social_links" ("id") VALUES (40);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (1,'icon',1,'components_misc_icons',1,1);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (2,'link',1,'components_links_normal_links',6,1);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (3,'icon',1,'components_misc_icons',2,4);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (4,'icon',1,'components_misc_icons',4,3);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (5,'icon',1,'components_misc_icons',3,2);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (6,'link',1,'components_links_normal_links',7,4);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (7,'link',1,'components_links_normal_links',8,3);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (8,'link',1,'components_links_normal_links',9,2);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (9,'icon',1,'components_misc_icons',10,7);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (10,'icon',1,'components_misc_icons',8,5);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (11,'icon',1,'components_misc_icons',9,6);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (12,'link',1,'components_links_normal_links',14,7);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (13,'link',1,'components_links_normal_links',16,6);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (14,'link',1,'components_links_normal_links',15,5);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (15,'icon',1,'components_misc_icons',11,8);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (16,'icon',1,'components_misc_icons',12,9);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (17,'icon',1,'components_misc_icons',13,10);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (18,'link',1,'components_links_normal_links',17,8);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (19,'link',1,'components_links_normal_links',18,9);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (20,'link',1,'components_links_normal_links',19,10);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (21,'icon',1,'components_misc_icons',14,11);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (22,'icon',1,'components_misc_icons',15,12);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (23,'icon',1,'components_misc_icons',16,13);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (24,'link',1,'components_links_normal_links',21,12);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (25,'link',1,'components_links_normal_links',22,13);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (26,'link',1,'components_links_normal_links',20,11);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (27,'icon',1,'components_misc_icons',17,14);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (28,'icon',1,'components_misc_icons',18,16);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (29,'icon',1,'components_misc_icons',19,15);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (30,'link',1,'components_links_normal_links',23,14);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (31,'link',1,'components_links_normal_links',24,16);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (32,'link',1,'components_links_normal_links',25,15);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (33,'icon',1,'components_misc_icons',20,17);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (34,'icon',1,'components_misc_icons',21,18);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (35,'icon',1,'components_misc_icons',22,19);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (36,'link',1,'components_links_normal_links',26,17);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (37,'link',1,'components_links_normal_links',27,18);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (38,'link',1,'components_links_normal_links',28,19);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (39,'icon',1,'components_misc_icons',23,20);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (40,'icon',1,'components_misc_icons',24,21);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (41,'icon',1,'components_misc_icons',25,22);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (42,'link',1,'components_links_normal_links',29,20);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (43,'link',1,'components_links_normal_links',31,22);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (44,'link',1,'components_links_normal_links',30,21);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (45,'icon',1,'components_misc_icons',26,23);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (46,'icon',1,'components_misc_icons',27,24);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (47,'icon',1,'components_misc_icons',28,25);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (48,'link',1,'components_links_normal_links',32,23);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (49,'link',1,'components_links_normal_links',33,24);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (50,'link',1,'components_links_normal_links',34,25);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (51,'icon',1,'components_misc_icons',29,27);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (52,'icon',1,'components_misc_icons',30,28);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (53,'icon',1,'components_misc_icons',31,26);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (54,'link',1,'components_links_normal_links',35,27);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (55,'link',1,'components_links_normal_links',36,28);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (56,'link',1,'components_links_normal_links',37,26);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (57,'icon',1,'components_misc_icons',32,29);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (58,'icon',1,'components_misc_icons',33,30);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (59,'icon',1,'components_misc_icons',34,31);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (60,'link',1,'components_links_normal_links',38,29);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (61,'link',1,'components_links_normal_links',39,30);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (62,'link',1,'components_links_normal_links',40,31);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (63,'icon',1,'components_misc_icons',37,34);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (64,'icon',1,'components_misc_icons',36,33);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (65,'icon',1,'components_misc_icons',35,32);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (66,'link',1,'components_links_normal_links',41,34);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (67,'link',1,'components_links_normal_links',42,33);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (68,'link',1,'components_links_normal_links',43,32);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (69,'icon',1,'components_misc_icons',38,36);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (70,'icon',1,'components_misc_icons',40,37);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (71,'icon',1,'components_misc_icons',39,35);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (72,'link',1,'components_links_normal_links',44,36);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (73,'link',1,'components_links_normal_links',45,37);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (74,'link',1,'components_links_normal_links',46,35);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (75,'icon',1,'components_misc_icons',41,38);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (76,'icon',1,'components_misc_icons',42,39);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (77,'icon',1,'components_misc_icons',43,40);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (78,'link',1,'components_links_normal_links',47,38);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (79,'link',1,'components_links_normal_links',49,39);
INSERT INTO "components_links_social_links_components" ("id","field","order","component_type","component_id","components_links_social_link_id") VALUES (80,'link',1,'components_links_normal_links',48,40);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (2,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (3,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (4,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (5,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (6,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (7,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (8,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (9,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (10,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (11,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (12,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (13,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (14,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (15,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (16,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (17,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (18,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (19,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (20,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (21,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (22,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (23,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (24,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (25,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (26,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (27,'RO');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (28,'DE');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (29,NULL);
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (30,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (31,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (32,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (33,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (34,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (35,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (36,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (37,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (38,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (39,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (40,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (41,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (42,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (43,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (44,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (45,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (46,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (47,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (48,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (49,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (50,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (51,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (52,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (53,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (54,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (55,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (56,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (57,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (58,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (59,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (60,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (61,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (62,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (63,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (64,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (65,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (66,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (67,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (68,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (69,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (70,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (71,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (72,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (73,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (74,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (75,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (76,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (77,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (78,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (79,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (80,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (81,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (82,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (83,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (84,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (85,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (86,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (87,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (88,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (89,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (90,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (91,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (92,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (93,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (94,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (95,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (96,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (97,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (98,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (99,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (100,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (101,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (102,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (103,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (104,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (105,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (106,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (107,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (108,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (109,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (110,'Excepteur sint occaecat cupidatat');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (111,'Dignissim cras tincidunt lobortis');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (112,'Duis aute irure dolor');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (113,'Lorem ipsum dolor sit amet, consectetur');
INSERT INTO "components_misc_custom_enumerations" ("id","text") VALUES (114,'Fames ac turpis egestas maecenas');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (1,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (2,'Bussiness','instagram');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (3,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (4,'Bussiness','linkedin');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (5,'Solid','globe');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (6,'Regular','inbox');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (7,'Solid','info');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (8,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (9,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (10,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (11,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (12,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (13,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (14,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (15,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (16,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (17,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (18,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (19,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (20,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (21,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (22,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (23,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (24,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (25,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (26,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (27,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (28,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (29,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (30,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (31,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (32,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (33,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (34,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (35,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (36,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (37,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (38,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (39,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (40,'Bussiness','reddit');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (41,'Bussiness','facebook');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (42,'Bussiness','twitter');
INSERT INTO "components_misc_icons" ("id","type","name") VALUES (43,'Bussiness','reddit');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (1,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (2,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (3,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (4,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (5,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (6,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (7,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (8,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (9,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (10,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (11,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (12,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (13,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (14,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (15,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (16,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (17,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (18,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (19,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (20,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (21,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (22,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (23,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (24,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (25,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (26,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (27,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (28,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (29,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (30,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (31,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (32,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (33,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (34,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (35,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (36,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (37,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (38,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (39,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (40,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (41,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (42,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (43,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (44,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (45,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (46,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (47,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (48,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (49,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (50,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (51,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (52,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (53,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (54,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (55,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (56,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (57,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (58,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (59,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (60,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (61,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (62,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (63,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (64,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (65,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (66,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (67,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (68,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (69,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (70,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (71,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (72,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (73,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (74,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (75,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (76,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (77,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (78,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (79,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (80,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (81,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (82,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (83,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (84,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (85,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (86,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (87,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (88,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (89,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (90,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (91,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (92,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (93,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (94,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (95,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (96,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (97,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (98,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (99,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (100,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (101,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (102,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (103,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (104,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (105,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (106,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (107,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (108,'Mobile-friendly design','Some bugs');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (109,'Unlimited withdraw limits','A number of restricted countries');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (110,'Multiple withdraw methods','Slow connection sometimes');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (111,'Live chat open 24/7','Long withdraw time');
INSERT INTO "components_misc_pro_cons" ("id","pros","cons") VALUES (112,'Mobile-friendly design','Some bugs');
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (3,1,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (4,1,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (5,1,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (6,1,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (7,1,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (8,1,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (9,1,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (10,1,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (11,1,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (12,1,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (13,1,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (14,1,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (15,1,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (16,1,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (17,1,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (18,1,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (19,1,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (20,1,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (21,1,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (22,1,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (23,1,18);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (24,1,25);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (25,1,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (26,1,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (27,1,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (28,1,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (29,1,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (30,1,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (31,1,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (32,1,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (33,1,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (34,3,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (35,3,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (36,3,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (37,3,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (38,3,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (39,3,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (40,3,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (41,3,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (42,3,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (43,3,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (44,3,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (45,3,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (46,3,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (47,3,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (48,3,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (49,3,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (50,3,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (51,3,18);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (52,3,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (53,3,25);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (54,3,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (55,3,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (56,3,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (57,3,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (58,3,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (59,3,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (60,3,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (61,3,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (62,3,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (63,3,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (64,3,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (66,4,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (67,4,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (68,4,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (69,4,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (70,4,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (71,4,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (72,4,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (73,4,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (74,4,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (75,4,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (76,4,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (77,4,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (78,4,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (80,4,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (82,4,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (83,4,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (84,4,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (85,4,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (86,4,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (87,4,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (88,4,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (89,4,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (91,4,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (93,4,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (95,4,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (96,5,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (97,5,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (98,5,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (99,5,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (100,5,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (101,5,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (102,5,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (103,5,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (104,5,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (105,5,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (106,5,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (107,5,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (108,5,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (109,5,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (110,5,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (111,5,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (112,5,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (113,5,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (114,5,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (115,5,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (116,5,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (117,5,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (118,5,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (119,5,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (120,5,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (121,5,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (122,5,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (123,5,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (124,5,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (125,6,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (126,6,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (127,6,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (128,6,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (129,6,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (130,6,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (131,6,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (132,6,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (133,6,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (134,6,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (135,6,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (136,6,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (137,6,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (138,6,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (139,6,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (140,6,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (141,6,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (142,6,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (143,6,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (144,6,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (145,6,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (146,6,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (147,6,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (148,7,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (149,7,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (150,7,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (151,7,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (152,7,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (153,7,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (154,7,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (155,7,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (156,7,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (157,7,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (158,7,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (159,7,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (160,7,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (161,7,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (162,7,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (163,7,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (164,7,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (165,7,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (166,7,18);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (167,7,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (168,7,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (169,7,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (170,7,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (171,7,25);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (172,7,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (173,7,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (174,7,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (175,7,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (176,7,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (177,7,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (178,7,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (179,8,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (180,8,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (181,8,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (182,8,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (183,8,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (184,8,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (185,8,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (186,8,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (187,8,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (188,8,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (189,8,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (190,8,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (191,8,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (192,8,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (193,8,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (194,8,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (195,8,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (196,8,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (197,8,18);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (198,8,25);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (199,8,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (200,8,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (201,8,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (202,8,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (203,8,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (204,8,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (205,8,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (206,8,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (207,8,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (208,8,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (209,8,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (210,9,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (211,9,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (212,9,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (213,9,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (214,9,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (215,9,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (216,9,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (217,9,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (218,9,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (219,9,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (220,9,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (221,9,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (222,9,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (223,9,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (224,9,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (225,9,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (226,9,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (227,9,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (228,9,18);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (229,9,25);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (230,9,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (231,9,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (232,9,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (233,9,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (234,9,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (235,9,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (236,9,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (237,9,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (238,9,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (239,9,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (240,9,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (241,10,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (242,10,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (243,10,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (244,10,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (245,10,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (246,10,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (247,10,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (248,10,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (249,10,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (250,10,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (251,10,18);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (252,10,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (253,10,25);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (254,10,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (255,10,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (256,10,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (257,10,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (258,10,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (259,10,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (260,10,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (261,10,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (262,10,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (263,10,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (264,10,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (265,10,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (266,11,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (267,11,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (268,11,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (269,11,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (270,11,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (271,11,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (272,11,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (273,11,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (274,11,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (275,11,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (276,11,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (277,11,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (278,11,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (279,11,18);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (280,11,25);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (281,11,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (282,11,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (283,11,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (284,11,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (285,11,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (286,11,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (287,11,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (288,11,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (289,11,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (290,11,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (291,11,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (292,11,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (293,11,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (294,12,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (295,12,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (296,12,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (297,12,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (298,12,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (299,12,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (300,12,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (301,12,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (302,12,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (303,12,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (304,12,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (305,12,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (306,12,26);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (307,12,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (308,12,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (309,12,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (310,12,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (311,12,30);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (312,12,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (313,12,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (314,12,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (315,12,29);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (316,12,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (317,12,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (318,12,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (319,12,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (320,12,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (321,13,9);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (322,13,11);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (323,13,7);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (324,13,13);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (325,13,4);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (326,13,8);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (327,13,5);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (328,13,3);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (329,13,15);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (330,13,14);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (331,13,12);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (332,13,10);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (333,13,22);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (334,13,17);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (335,13,19);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (336,13,2);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (337,13,21);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (338,13,16);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (339,13,20);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (340,13,6);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (341,13,31);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (342,13,28);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (343,13,24);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (344,13,32);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (345,13,23);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (346,13,27);
INSERT INTO "casinos_tags__tags_casinos" ("id","casino_id","tag_id") VALUES (347,13,30);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (3,'rating',2,'components_misc_ratings',4,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (4,'rating',1,'components_misc_ratings',3,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (6,'experiences',2,'components_misc_pro_cons',2,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (7,'experiences',1,'components_misc_pro_cons',1,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (8,'experiences',3,'components_misc_pro_cons',3,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (9,'experiences',4,'components_misc_pro_cons',4,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (10,'rating',3,'components_misc_ratings',5,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (11,'rating',4,'components_misc_ratings',6,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (12,'rating',5,'components_misc_ratings',7,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (13,'socials',3,'components_links_social_links',7,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (14,'socials',2,'components_links_social_links',6,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (15,'socials',1,'components_links_social_links',5,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (16,'acceptedCountries',1,'components_misc_custom_enumerations',2,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (17,'acceptedCountries',3,'components_misc_custom_enumerations',4,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (18,'acceptedCountries',2,'components_misc_custom_enumerations',3,1);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (19,'experiences',1,'components_misc_pro_cons',5,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (20,'experiences',2,'components_misc_pro_cons',6,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (21,'experiences',3,'components_misc_pro_cons',7,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (22,'rating',1,'components_misc_ratings',8,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (23,'rating',5,'components_misc_ratings',9,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (24,'rating',2,'components_misc_ratings',10,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (25,'rating',4,'components_misc_ratings',12,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (26,'rating',3,'components_misc_ratings',11,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (27,'socials',1,'components_links_social_links',8,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (28,'socials',2,'components_links_social_links',9,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (29,'socials',3,'components_links_social_links',10,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (30,'acceptedCountries',1,'components_misc_custom_enumerations',5,3);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (31,'experiences',1,'components_misc_pro_cons',8,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (32,'experiences',3,'components_misc_pro_cons',10,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (33,'experiences',2,'components_misc_pro_cons',9,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (34,'rating',1,'components_misc_ratings',13,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (35,'rating',2,'components_misc_ratings',15,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (36,'rating',4,'components_misc_ratings',17,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (37,'rating',3,'components_misc_ratings',16,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (38,'rating',5,'components_misc_ratings',14,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (39,'socials',3,'components_links_social_links',12,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (40,'socials',2,'components_links_social_links',13,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (41,'socials',1,'components_links_social_links',11,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (42,'acceptedCountries',1,'components_misc_custom_enumerations',6,4);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (43,'experiences',1,'components_misc_pro_cons',11,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (44,'experiences',3,'components_misc_pro_cons',13,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (45,'experiences',4,'components_misc_pro_cons',14,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (46,'experiences',2,'components_misc_pro_cons',12,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (47,'rating',1,'components_misc_ratings',18,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (48,'rating',5,'components_misc_ratings',19,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (49,'rating',2,'components_misc_ratings',20,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (50,'rating',3,'components_misc_ratings',21,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (51,'rating',4,'components_misc_ratings',22,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (52,'socials',1,'components_links_social_links',14,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (53,'socials',3,'components_links_social_links',16,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (54,'socials',2,'components_links_social_links',15,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (55,'acceptedCountries',1,'components_misc_custom_enumerations',7,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (56,'acceptedCountries',2,'components_misc_custom_enumerations',9,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (57,'acceptedCountries',3,'components_misc_custom_enumerations',8,5);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (58,'experiences',1,'components_misc_pro_cons',15,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (59,'experiences',2,'components_misc_pro_cons',16,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (60,'experiences',3,'components_misc_pro_cons',17,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (61,'rating',1,'components_misc_ratings',23,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (62,'rating',2,'components_misc_ratings',24,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (63,'rating',4,'components_misc_ratings',26,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (64,'rating',3,'components_misc_ratings',25,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (65,'rating',5,'components_misc_ratings',27,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (66,'socials',1,'components_links_social_links',17,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (67,'socials',2,'components_links_social_links',18,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (68,'socials',3,'components_links_social_links',19,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (69,'acceptedCountries',1,'components_misc_custom_enumerations',10,6);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (70,'experiences',1,'components_misc_pro_cons',18,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (71,'experiences',2,'components_misc_pro_cons',19,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (72,'experiences',4,'components_misc_pro_cons',21,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (73,'experiences',3,'components_misc_pro_cons',20,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (74,'socials',1,'components_links_social_links',20,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (75,'socials',3,'components_links_social_links',22,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (76,'socials',2,'components_links_social_links',21,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (77,'acceptedCountries',2,'components_misc_custom_enumerations',12,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (78,'acceptedCountries',1,'components_misc_custom_enumerations',11,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (79,'acceptedCountries',3,'components_misc_custom_enumerations',13,7);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (80,'experiences',4,'components_misc_pro_cons',25,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (81,'experiences',1,'components_misc_pro_cons',22,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (82,'experiences',2,'components_misc_pro_cons',23,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (83,'experiences',3,'components_misc_pro_cons',24,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (84,'socials',1,'components_links_social_links',23,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (85,'socials',3,'components_links_social_links',24,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (86,'socials',2,'components_links_social_links',25,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (87,'acceptedCountries',1,'components_misc_custom_enumerations',14,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (88,'acceptedCountries',3,'components_misc_custom_enumerations',16,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (89,'acceptedCountries',2,'components_misc_custom_enumerations',15,8);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (90,'experiences',1,'components_misc_pro_cons',26,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (91,'experiences',2,'components_misc_pro_cons',27,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (92,'experiences',3,'components_misc_pro_cons',28,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (93,'experiences',4,'components_misc_pro_cons',29,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (94,'socials',2,'components_links_social_links',27,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (95,'socials',3,'components_links_social_links',28,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (96,'socials',1,'components_links_social_links',26,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (97,'acceptedCountries',1,'components_misc_custom_enumerations',17,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (98,'acceptedCountries',2,'components_misc_custom_enumerations',18,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (99,'acceptedCountries',3,'components_misc_custom_enumerations',19,9);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (100,'experiences',1,'components_misc_pro_cons',30,10);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (101,'experiences',2,'components_misc_pro_cons',31,10);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (102,'experiences',3,'components_misc_pro_cons',32,10);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (103,'socials',1,'components_links_social_links',29,10);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (104,'socials',2,'components_links_social_links',30,10);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (105,'socials',3,'components_links_social_links',31,10);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (106,'acceptedCountries',1,'components_misc_custom_enumerations',20,10);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (107,'experiences',1,'components_misc_pro_cons',33,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (108,'experiences',2,'components_misc_pro_cons',35,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (109,'experiences',4,'components_misc_pro_cons',36,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (110,'experiences',3,'components_misc_pro_cons',34,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (111,'socials',3,'components_links_social_links',34,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (112,'socials',2,'components_links_social_links',33,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (113,'socials',1,'components_links_social_links',32,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (114,'acceptedCountries',1,'components_misc_custom_enumerations',21,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (115,'acceptedCountries',2,'components_misc_custom_enumerations',22,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (116,'acceptedCountries',3,'components_misc_custom_enumerations',23,11);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (117,'experiences',2,'components_misc_pro_cons',38,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (118,'experiences',1,'components_misc_pro_cons',37,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (119,'experiences',3,'components_misc_pro_cons',39,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (120,'experiences',4,'components_misc_pro_cons',40,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (121,'socials',2,'components_links_social_links',36,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (122,'socials',3,'components_links_social_links',37,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (123,'socials',1,'components_links_social_links',35,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (124,'acceptedCountries',2,'components_misc_custom_enumerations',25,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (125,'acceptedCountries',3,'components_misc_custom_enumerations',26,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (126,'acceptedCountries',1,'components_misc_custom_enumerations',24,12);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (127,'experiences',4,'components_misc_pro_cons',44,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (128,'experiences',2,'components_misc_pro_cons',42,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (129,'experiences',3,'components_misc_pro_cons',43,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (130,'experiences',1,'components_misc_pro_cons',41,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (131,'socials',1,'components_links_social_links',38,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (132,'socials',3,'components_links_social_links',40,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (133,'socials',2,'components_links_social_links',39,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (134,'acceptedCountries',1,'components_misc_custom_enumerations',27,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (135,'acceptedCountries',2,'components_misc_custom_enumerations',28,13);
INSERT INTO "casinos_components" ("id","field","order","component_type","component_id","casino_id") VALUES (136,'acceptedCountries',3,'components_misc_custom_enumerations',29,13);
INSERT INTO "tag_types" ("id","title","slug","created_by","updated_by","created_at","updated_at") VALUES (2,'Software','Software',1,1,1633536346431,1633536346436);
INSERT INTO "tag_types" ("id","title","slug","created_by","updated_by","created_at","updated_at") VALUES (3,'Licences','Licences',1,1,1633536361436,1633536361443);
INSERT INTO "tag_types" ("id","title","slug","created_by","updated_by","created_at","updated_at") VALUES (4,'Languages','Languages',1,1,1633536372790,1633536372797);
INSERT INTO "tag_types" ("id","title","slug","created_by","updated_by","created_at","updated_at") VALUES (5,'Currencies','Currencies',1,1,1633536381242,1633536381250);
INSERT INTO "tag_types" ("id","title","slug","created_by","updated_by","created_at","updated_at") VALUES (6,'Payment Methods','Payment-Methods',1,1,1633536391147,1633536391152);
INSERT INTO "tag_types" ("id","title","slug","created_by","updated_by","created_at","updated_at") VALUES (7,'Restricted Countries','Restricted-Countries',1,1,1633536408370,1633536408377);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1,'plugins::content-manager.explorer.create','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags"]}','[]',2,1633093857997,1633537543301);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (2,'plugins::content-manager.explorer.create','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','[]',2,1633093858011,1633093858016);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (3,'plugins::content-manager.explorer.create','application::tags.tags','{"fields":["title","slug","tag_type","casinos"]}','[]',2,1633093858026,1633093858031);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (4,'plugins::content-manager.explorer.read','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags"]}','[]',2,1633093858041,1633537543317);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (5,'plugins::content-manager.explorer.read','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','[]',2,1633093858056,1633093858060);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (6,'plugins::content-manager.explorer.read','application::tags.tags','{"fields":["title","slug","tag_type","casinos"]}','[]',2,1633093858070,1633093858074);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (7,'plugins::content-manager.explorer.update','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags"]}','[]',2,1633093858085,1633537543334);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (8,'plugins::content-manager.explorer.update','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','[]',2,1633093858102,1633093858109);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (9,'plugins::content-manager.explorer.update','application::tags.tags','{"fields":["title","slug","tag_type","casinos"]}','[]',2,1633093858119,1633093858124);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (10,'plugins::content-manager.explorer.delete','application::casino.casino','{}','[]',2,1633093858133,1633093858138);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (11,'plugins::content-manager.explorer.delete','application::tag-types.tag-types','{}','[]',2,1633093858147,1633093858152);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (12,'plugins::content-manager.explorer.delete','application::tags.tags','{}','[]',2,1633093858162,1633093858167);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (13,'plugins::content-manager.explorer.publish','application::casino.casino','{}','[]',2,1633093858176,1633093858181);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (14,'plugins::upload.read',NULL,'{}','[]',2,1633093858190,1633093858194);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (15,'plugins::upload.assets.create',NULL,'{}','[]',2,1633093858203,1633093858207);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (16,'plugins::upload.assets.update',NULL,'{}','[]',2,1633093858217,1633093858222);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (17,'plugins::upload.assets.download',NULL,'{}','[]',2,1633093858231,1633093858236);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (18,'plugins::upload.assets.copy-link',NULL,'{}','[]',2,1633093858245,1633093858249);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (19,'plugins::content-manager.explorer.create','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags"]}','["admin::is-creator"]',3,1633093858260,1633537543349);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (20,'plugins::content-manager.explorer.create','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','["admin::is-creator"]',3,1633093858274,1633093858278);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (21,'plugins::content-manager.explorer.create','application::tags.tags','{"fields":["title","slug","tag_type","casinos"]}','["admin::is-creator"]',3,1633093858288,1633093858292);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (22,'plugins::content-manager.explorer.read','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags"]}','["admin::is-creator"]',3,1633093858302,1633537543364);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (23,'plugins::content-manager.explorer.read','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','["admin::is-creator"]',3,1633093858316,1633093858321);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (24,'plugins::content-manager.explorer.read','application::tags.tags','{"fields":["title","slug","tag_type","casinos"]}','["admin::is-creator"]',3,1633093858330,1633093858334);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (25,'plugins::content-manager.explorer.update','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags"]}','["admin::is-creator"]',3,1633093858343,1633537543380);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (26,'plugins::content-manager.explorer.update','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','["admin::is-creator"]',3,1633093858357,1633093858362);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (27,'plugins::content-manager.explorer.update','application::tags.tags','{"fields":["title","slug","tag_type","casinos"]}','["admin::is-creator"]',3,1633093858371,1633093858375);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (28,'plugins::content-manager.explorer.delete','application::casino.casino','{}','["admin::is-creator"]',3,1633093858384,1633093858389);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (29,'plugins::content-manager.explorer.delete','application::tag-types.tag-types','{}','["admin::is-creator"]',3,1633093858398,1633093858402);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (30,'plugins::content-manager.explorer.delete','application::tags.tags','{}','["admin::is-creator"]',3,1633093858411,1633093858416);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (31,'plugins::upload.read',NULL,'{}','["admin::is-creator"]',3,1633093858425,1633093858430);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (32,'plugins::upload.assets.create',NULL,'{}','[]',3,1633093858438,1633093858443);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (33,'plugins::upload.assets.update',NULL,'{}','["admin::is-creator"]',3,1633093858451,1633093858456);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (34,'plugins::upload.assets.download',NULL,'{}','[]',3,1633093858465,1633093858469);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (35,'plugins::upload.assets.copy-link',NULL,'{}','[]',3,1633093858478,1633093858483);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (37,'plugins::content-manager.explorer.create','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','[]',1,1633093858530,1633093858534);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (39,'plugins::content-manager.explorer.create','plugins::users-permissions.user','{"fields":["username","email","provider","password","resetPasswordToken","confirmationToken","confirmed","blocked","role"]}','[]',1,1633093858557,1633093858561);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (41,'plugins::content-manager.explorer.read','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','[]',1,1633093858583,1633093858588);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (43,'plugins::content-manager.explorer.read','plugins::users-permissions.user','{"fields":["username","email","provider","password","resetPasswordToken","confirmationToken","confirmed","blocked","role"]}','[]',1,1633093858611,1633093858616);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (45,'plugins::content-manager.explorer.update','application::tag-types.tag-types','{"fields":["title","slug","tags"]}','[]',1,1633093858639,1633093858643);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (47,'plugins::content-manager.explorer.update','plugins::users-permissions.user','{"fields":["username","email","provider","password","resetPasswordToken","confirmationToken","confirmed","blocked","role"]}','[]',1,1633093858665,1633093858670);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (53,'plugins::content-type-builder.read',NULL,'{}','[]',1,1633093858748,1633093858753);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (54,'plugins::email.settings.read',NULL,'{}','[]',1,1633093858761,1633093858766);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (55,'plugins::upload.read',NULL,'{}','[]',1,1633093858775,1633093858780);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (56,'plugins::upload.assets.create',NULL,'{}','[]',1,1633093858789,1633093858793);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (57,'plugins::upload.assets.update',NULL,'{}','[]',1,1633093858802,1633093858807);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (58,'plugins::upload.assets.download',NULL,'{}','[]',1,1633093858817,1633093858823);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (59,'plugins::upload.assets.copy-link',NULL,'{}','[]',1,1633093858833,1633093858838);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (60,'plugins::upload.settings.read',NULL,'{}','[]',1,1633093858847,1633093858851);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (61,'plugins::i18n.locale.create',NULL,'{}','[]',1,1633093858860,1633093858865);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (62,'plugins::i18n.locale.read',NULL,'{}','[]',1,1633093858874,1633093858879);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (63,'plugins::i18n.locale.update',NULL,'{}','[]',1,1633093858887,1633093858892);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (64,'plugins::i18n.locale.delete',NULL,'{}','[]',1,1633093858901,1633093858905);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (65,'plugins::content-manager.single-types.configure-view',NULL,'{}','[]',1,1633093858916,1633093858920);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (66,'plugins::content-manager.collection-types.configure-view',NULL,'{}','[]',1,1633093858929,1633093858933);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (67,'plugins::content-manager.components.configure-layout',NULL,'{}','[]',1,1633093858942,1633093858947);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (68,'plugins::users-permissions.roles.create',NULL,'{}','[]',1,1633093858956,1633093858960);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (69,'plugins::users-permissions.roles.read',NULL,'{}','[]',1,1633093858970,1633093858974);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (70,'plugins::users-permissions.roles.update',NULL,'{}','[]',1,1633093858983,1633093858988);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (71,'plugins::users-permissions.roles.delete',NULL,'{}','[]',1,1633093858997,1633093859003);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (72,'plugins::users-permissions.providers.read',NULL,'{}','[]',1,1633093859013,1633093859018);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (73,'plugins::users-permissions.providers.update',NULL,'{}','[]',1,1633093859027,1633093859031);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (74,'plugins::users-permissions.email-templates.read',NULL,'{}','[]',1,1633093859040,1633093859045);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (75,'plugins::users-permissions.email-templates.update',NULL,'{}','[]',1,1633093859054,1633093859058);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (76,'plugins::users-permissions.advanced-settings.read',NULL,'{}','[]',1,1633093859068,1633093859072);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (77,'plugins::users-permissions.advanced-settings.update',NULL,'{}','[]',1,1633093859081,1633093859086);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (78,'admin::marketplace.read',NULL,'{}','[]',1,1633093859095,1633093859099);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (79,'admin::marketplace.plugins.install',NULL,'{}','[]',1,1633093859108,1633093859113);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (80,'admin::marketplace.plugins.uninstall',NULL,'{}','[]',1,1633093859122,1633093859126);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (81,'admin::webhooks.create',NULL,'{}','[]',1,1633093859136,1633093859140);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (82,'admin::webhooks.read',NULL,'{}','[]',1,1633093859149,1633093859153);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (83,'admin::webhooks.update',NULL,'{}','[]',1,1633093859163,1633093859167);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (84,'admin::webhooks.delete',NULL,'{}','[]',1,1633093859176,1633093859181);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (85,'admin::users.create',NULL,'{}','[]',1,1633093859190,1633093859194);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (86,'admin::users.read',NULL,'{}','[]',1,1633093859203,1633093859208);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (87,'admin::users.update',NULL,'{}','[]',1,1633093859217,1633093859221);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (88,'admin::users.delete',NULL,'{}','[]',1,1633093859230,1633093859235);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (89,'admin::roles.create',NULL,'{}','[]',1,1633093859244,1633093859248);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (90,'admin::roles.read',NULL,'{}','[]',1,1633093859257,1633093859262);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (91,'admin::roles.update',NULL,'{}','[]',1,1633093859271,1633093859275);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (92,'admin::roles.delete',NULL,'{}','[]',1,1633093859285,1633093859291);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (158,'plugins::content-manager.explorer.create','application::nav-links.nav-links','{"fields":["submenu","badge.text","badge.type","link.href","link.name","link.target"]}','[]',1,1633423466525,1633423466530);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (159,'plugins::content-manager.explorer.read','application::nav-links.nav-links','{"fields":["submenu","badge.text","badge.type","link.href","link.name","link.target"]}','[]',1,1633423466539,1633423466544);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (160,'plugins::content-manager.explorer.update','application::nav-links.nav-links','{"fields":["submenu","badge.text","badge.type","link.href","link.name","link.target"]}','[]',1,1633423466554,1633423466559);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (202,'plugins::content-manager.explorer.create','application::comment.comment','{"fields":["content","name","email","website"]}','[]',1,1633444863473,1633444863477);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (203,'plugins::content-manager.explorer.read','application::comment.comment','{"fields":["content","name","email","website"]}','[]',1,1633444863486,1633444863491);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (204,'plugins::content-manager.explorer.update','application::comment.comment','{"fields":["content","name","email","website"]}','[]',1,1633444863502,1633444863506);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (227,'plugins::content-manager.explorer.create','application::bonus.bonus','{"fields":["bonus","title","bonusLink","slug","background","rewards","shortDesc","about","experiences.pros","experiences.cons","features.text","tags"]}','[]',1,1633518049311,1633518049316);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (229,'plugins::content-manager.explorer.read','application::bonus.bonus','{"fields":["bonus","title","bonusLink","slug","background","rewards","shortDesc","about","experiences.pros","experiences.cons","features.text","tags"]}','[]',1,1633518049339,1633518049344);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (231,'plugins::content-manager.explorer.update','application::bonus.bonus','{"fields":["bonus","title","bonusLink","slug","background","rewards","shortDesc","about","experiences.pros","experiences.cons","features.text","tags"]}','[]',1,1633518049367,1633518049371);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (301,'plugins::content-manager.explorer.create','application::tags.tags','{"fields":["title","slug","tag_type","casinos","bookmakers","bonuses"]}','[]',1,1633519011856,1633519011861);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (303,'plugins::content-manager.explorer.read','application::tags.tags','{"fields":["title","slug","tag_type","casinos","bookmakers","bonuses"]}','[]',1,1633519011885,1633519011889);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (305,'plugins::content-manager.explorer.update','application::tags.tags','{"fields":["title","slug","tag_type","casinos","bookmakers","bonuses"]}','[]',1,1633519011912,1633519011916);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (354,'plugins::content-manager.explorer.create','application::bookmaker.bookmaker','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","rating.title","rating.percentage","likes","referralLink","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags","cover"]}','[]',1,1633519331734,1633519331739);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (355,'plugins::content-manager.explorer.read','application::bookmaker.bookmaker','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","rating.title","rating.percentage","likes","referralLink","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags","cover"]}','[]',1,1633519331749,1633519331753);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (356,'plugins::content-manager.explorer.update','application::bookmaker.bookmaker','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","rating.title","rating.percentage","likes","referralLink","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags","cover"]}','[]',1,1633519331762,1633519331768);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (463,'plugins::content-manager.explorer.create','application::blog-tags.blog-tags','{"fields":["name","slug"]}','[]',1,1633519597147,1633519597152);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (464,'plugins::content-manager.explorer.read','application::blog-tags.blog-tags','{"fields":["name","slug"]}','[]',1,1633519597160,1633519597165);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (465,'plugins::content-manager.explorer.update','application::blog-tags.blog-tags','{"fields":["name","slug"]}','[]',1,1633519597173,1633519597179);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (531,'plugins::content-manager.explorer.create','application::home-header.home-header','{"fields":["title","description","email","button.href","button.name","button.target","image"]}','[]',1,1633519769588,1633519769593);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (532,'plugins::content-manager.explorer.read','application::home-header.home-header','{"fields":["title","description","email","button.href","button.name","button.target","image"]}','[]',1,1633519769602,1633519769607);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (533,'plugins::content-manager.explorer.update','application::home-header.home-header','{"fields":["title","description","email","button.href","button.name","button.target","image"]}','[]',1,1633519769616,1633519769621);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (646,'plugins::content-manager.explorer.create','application::home-characteristics.home-characteristics','{"fields":["characteristics.title","characteristics.description","sectionTitle.title","sectionTitle.subtitle","sectionTitle.description"]}','[]',1,1633520063942,1633520063947);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (647,'plugins::content-manager.explorer.read','application::home-characteristics.home-characteristics','{"fields":["characteristics.title","characteristics.description","sectionTitle.title","sectionTitle.subtitle","sectionTitle.description"]}','[]',1,1633520063955,1633520063960);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (648,'plugins::content-manager.explorer.update','application::home-characteristics.home-characteristics','{"fields":["characteristics.title","characteristics.description","sectionTitle.title","sectionTitle.subtitle","sectionTitle.description"]}','[]',1,1633520063969,1633520063973);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (695,'plugins::content-manager.explorer.create','application::home-faq.home-faq','{"fields":["faqList.question","faqList.answer"]}','[]',1,1633520353469,1633520353474);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (696,'plugins::content-manager.explorer.read','application::home-faq.home-faq','{"fields":["faqList.question","faqList.answer"]}','[]',1,1633520353484,1633520353488);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (697,'plugins::content-manager.explorer.update','application::home-faq.home-faq','{"fields":["faqList.question","faqList.answer"]}','[]',1,1633520353497,1633520353503);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (748,'plugins::content-manager.explorer.create','application::home-testimonials.home-testimonials','{"fields":["testimonials.author","testimonials.position","testimonials.message","testimonials.avatar"]}','[]',1,1633520489765,1633520489770);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (749,'plugins::content-manager.explorer.read','application::home-testimonials.home-testimonials','{"fields":["testimonials.author","testimonials.position","testimonials.message","testimonials.avatar"]}','[]',1,1633520489779,1633520489783);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (750,'plugins::content-manager.explorer.update','application::home-testimonials.home-testimonials','{"fields":["testimonials.author","testimonials.position","testimonials.message","testimonials.avatar"]}','[]',1,1633520489791,1633520489795);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (979,'plugins::content-manager.explorer.create','application::home-partners.home-partners','{"fields":["slider"]}','[]',1,1633521024424,1633521024428);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (980,'plugins::content-manager.explorer.read','application::home-partners.home-partners','{"fields":["slider"]}','[]',1,1633521024437,1633521024441);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (981,'plugins::content-manager.explorer.update','application::home-partners.home-partners','{"fields":["slider"]}','[]',1,1633521024450,1633521024455);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1049,'plugins::content-manager.explorer.create','application::footer.footer','{"fields":["aboutUs","email","phoneNo","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","subscribeText"]}','[]',1,1633521387870,1633521387875);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1050,'plugins::content-manager.explorer.read','application::footer.footer','{"fields":["aboutUs","email","phoneNo","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","subscribeText"]}','[]',1,1633521387885,1633521387890);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1051,'plugins::content-manager.explorer.update','application::footer.footer','{"fields":["aboutUs","email","phoneNo","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","subscribeText"]}','[]',1,1633521387899,1633521387904);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1085,'plugins::content-manager.explorer.create','application::menu.menu','{"fields":["socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","logo","siteName","navLinks"]}','[]',1,1633521487845,1633521487849);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1086,'plugins::content-manager.explorer.read','application::menu.menu','{"fields":["socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","logo","siteName","navLinks"]}','[]',1,1633521487858,1633521487863);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1087,'plugins::content-manager.explorer.update','application::menu.menu','{"fields":["socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","logo","siteName","navLinks"]}','[]',1,1633521487871,1633521487876);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1123,'plugins::content-manager.explorer.create','application::home-get-in-touch.home-get-in-touch','{"fields":["content.title","content.description","content.link.href","content.link.name","content.link.target","content.icon.type","content.icon.name"]}','[]',1,1633530357789,1633530357794);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1124,'plugins::content-manager.explorer.read','application::home-get-in-touch.home-get-in-touch','{"fields":["content.title","content.description","content.link.href","content.link.name","content.link.target","content.icon.type","content.icon.name"]}','[]',1,1633530357803,1633530357808);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1125,'plugins::content-manager.explorer.update','application::home-get-in-touch.home-get-in-touch','{"fields":["content.title","content.description","content.link.href","content.link.name","content.link.target","content.icon.type","content.icon.name"]}','[]',1,1633530357816,1633530357821);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1276,'plugins::content-manager.explorer.create','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","referralLink","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags","slots","cover","review"]}','[]',1,1633537542657,1633537542663);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1277,'plugins::content-manager.explorer.read','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","referralLink","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags","slots","cover","review"]}','[]',1,1633537542673,1633537542678);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1278,'plugins::content-manager.explorer.update','application::casino.casino','{"fields":["title","thumbnail","description","about","type","experiences.pros","experiences.cons","likes","referralLink","socials.icon.type","socials.icon.name","socials.link.href","socials.link.name","socials.link.target","active","acceptedCountries.text","slug","tags","slots","cover","review"]}','[]',1,1633537542687,1633537542692);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1433,'plugins::content-manager.explorer.create','application::review.review','{"fields":["rating.title","rating.percentage","casinos","content","title","slug","thumbnail"]}','[]',1,1633542935455,1633542935460);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1434,'plugins::content-manager.explorer.read','application::review.review','{"fields":["rating.title","rating.percentage","casinos","content","title","slug","thumbnail"]}','[]',1,1633542935469,1633542935474);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1435,'plugins::content-manager.explorer.update','application::review.review','{"fields":["rating.title","rating.percentage","casinos","content","title","slug","thumbnail"]}','[]',1,1633542935482,1633542935486);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1869,'plugins::content-manager.explorer.create','application::slot.slot','{"fields":["title","casino","description","about","experiences.pros","experiences.cons","features.text","tags","type","thumbnail","shortDescription","review","comments","link.href","link.name","link.target"]}','[]',1,1633618565128,1633618565133);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1870,'plugins::content-manager.explorer.read','application::slot.slot','{"fields":["title","casino","description","about","experiences.pros","experiences.cons","features.text","tags","type","thumbnail","shortDescription","review","comments","link.href","link.name","link.target"]}','[]',1,1633618565142,1633618565146);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1871,'plugins::content-manager.explorer.update','application::slot.slot','{"fields":["title","casino","description","about","experiences.pros","experiences.cons","features.text","tags","type","thumbnail","shortDescription","review","comments","link.href","link.name","link.target"]}','[]',1,1633618565156,1633618565161);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1872,'plugins::content-manager.explorer.delete','application::blog-tags.blog-tags','{}','[]',1,1633618565169,1633618565173);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1873,'plugins::content-manager.explorer.delete','application::bonus.bonus','{}','[]',1,1633618565182,1633618565187);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1874,'plugins::content-manager.explorer.delete','application::bookmaker.bookmaker','{}','[]',1,1633618565195,1633618565200);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1875,'plugins::content-manager.explorer.delete','application::casino.casino','{}','[]',1,1633618565209,1633618565213);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1876,'plugins::content-manager.explorer.delete','application::comment.comment','{}','[]',1,1633618565222,1633618565227);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1877,'plugins::content-manager.explorer.delete','application::footer.footer','{}','[]',1,1633618565236,1633618565241);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1878,'plugins::content-manager.explorer.delete','application::home-characteristics.home-characteristics','{}','[]',1,1633618565250,1633618565255);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1879,'plugins::content-manager.explorer.delete','application::home-faq.home-faq','{}','[]',1,1633618565265,1633618565270);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1880,'plugins::content-manager.explorer.delete','application::home-get-in-touch.home-get-in-touch','{}','[]',1,1633618565278,1633618565283);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1881,'plugins::content-manager.explorer.delete','application::home-header.home-header','{}','[]',1,1633618565292,1633618565296);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1882,'plugins::content-manager.explorer.delete','application::home-partners.home-partners','{}','[]',1,1633618565306,1633618565311);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1883,'plugins::content-manager.explorer.delete','application::home-testimonials.home-testimonials','{}','[]',1,1633618565319,1633618565324);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1884,'plugins::content-manager.explorer.delete','application::menu.menu','{}','[]',1,1633618565332,1633618565337);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1885,'plugins::content-manager.explorer.delete','application::nav-links.nav-links','{}','[]',1,1633618565346,1633618565350);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1886,'plugins::content-manager.explorer.delete','application::review.review','{}','[]',1,1633618565359,1633618565363);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1887,'plugins::content-manager.explorer.delete','application::slot.slot','{}','[]',1,1633618565372,1633618565376);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1888,'plugins::content-manager.explorer.delete','application::tag-types.tag-types','{}','[]',1,1633618565385,1633618565390);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1889,'plugins::content-manager.explorer.delete','application::tags.tags','{}','[]',1,1633618565398,1633618565402);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1890,'plugins::content-manager.explorer.delete','plugins::users-permissions.user','{}','[]',1,1633618565411,1633618565415);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1891,'plugins::content-manager.explorer.publish','application::blog-tags.blog-tags','{}','[]',1,1633618565423,1633618565428);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1892,'plugins::content-manager.explorer.publish','application::bonus.bonus','{}','[]',1,1633618565437,1633618565441);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1893,'plugins::content-manager.explorer.publish','application::bookmaker.bookmaker','{}','[]',1,1633618565450,1633618565454);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1894,'plugins::content-manager.explorer.publish','application::casino.casino','{}','[]',1,1633618565462,1633618565467);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1895,'plugins::content-manager.explorer.publish','application::comment.comment','{}','[]',1,1633618565477,1633618565482);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1896,'plugins::content-manager.explorer.publish','application::footer.footer','{}','[]',1,1633618565491,1633618565495);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1897,'plugins::content-manager.explorer.publish','application::home-characteristics.home-characteristics','{}','[]',1,1633618565505,1633618565509);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1898,'plugins::content-manager.explorer.publish','application::home-faq.home-faq','{}','[]',1,1633618565518,1633618565523);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1899,'plugins::content-manager.explorer.publish','application::home-get-in-touch.home-get-in-touch','{}','[]',1,1633618565532,1633618565536);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1900,'plugins::content-manager.explorer.publish','application::home-header.home-header','{}','[]',1,1633618565545,1633618565550);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1901,'plugins::content-manager.explorer.publish','application::home-partners.home-partners','{}','[]',1,1633618565559,1633618565563);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1902,'plugins::content-manager.explorer.publish','application::home-testimonials.home-testimonials','{}','[]',1,1633618565572,1633618565577);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1903,'plugins::content-manager.explorer.publish','application::menu.menu','{}','[]',1,1633618565585,1633618565589);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1904,'plugins::content-manager.explorer.publish','application::nav-links.nav-links','{}','[]',1,1633618565599,1633618565603);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1905,'plugins::content-manager.explorer.publish','application::review.review','{}','[]',1,1633618565611,1633618565616);
INSERT INTO "strapi_permission" ("id","action","subject","properties","conditions","role","created_at","updated_at") VALUES (1906,'plugins::content-manager.explorer.publish','application::slot.slot','{}','[]',1,1633618565625,1633618565629);
INSERT INTO "strapi_role" ("id","name","code","description","created_at","updated_at") VALUES (1,'Super Admin','strapi-super-admin','Super Admins can access and manage all features and settings.',1633093857947,1633093857947);
INSERT INTO "strapi_role" ("id","name","code","description","created_at","updated_at") VALUES (2,'Editor','strapi-editor','Editors can manage and publish contents including those of other users.',1633093857964,1633093857964);
INSERT INTO "strapi_role" ("id","name","code","description","created_at","updated_at") VALUES (3,'Author','strapi-author','Authors can manage the content they have created.',1633093857978,1633093857978);
INSERT INTO "strapi_administrator" ("id","firstname","lastname","username","email","password","resetPasswordToken","registrationToken","isActive","blocked","preferedLanguage") VALUES (1,'admin','admin',NULL,'admin@admin.com','$2a$10$MBBmZQbOFFgHyE9MtS43FOxsDz1ChSdBUQC2gDMZJA.sww2yto60S',NULL,NULL,1,NULL,NULL);
INSERT INTO "strapi_users_roles" ("id","user_id","role_id") VALUES (1,1,1);
INSERT INTO "i18n_locales" ("id","name","code","created_by","updated_by","created_at","updated_at") VALUES (1,'English (en)','en',NULL,NULL,1633093854733,1633093854733);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (1,'cat_casino02.jpg','','',500,500,'{"thumbnail":{"name":"thumbnail_cat_casino02.jpg","hash":"thumbnail_cat_casino02_cd6a192aab","ext":".jpg","mime":"image/jpeg","width":156,"height":156,"size":4.2,"path":null,"url":"/uploads/thumbnail_cat_casino02_cd6a192aab.jpg"}}','cat_casino02_cd6a192aab','.jpg','image/jpeg',25.55,'/uploads/cat_casino02_cd6a192aab.jpg',NULL,'local',NULL,1,1,1633096654390,1633096654397);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (2,'client_5.png','','',160,100,NULL,'client_5_af7000e960','.png','image/png',11.17,'/uploads/client_5_af7000e960.png',NULL,'local',NULL,1,1,1633530888503,1633530888511);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (3,'client_3.png','','',160,100,NULL,'client_3_d6e3ec94d6','.png','image/png',6.46,'/uploads/client_3_d6e3ec94d6.png',NULL,'local',NULL,1,1,1633530888526,1633530888535);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (4,'client_02.png','','',160,100,NULL,'client_02_56330312d3','.png','image/png',15.38,'/uploads/client_02_56330312d3.png',NULL,'local',NULL,1,1,1633530888551,1633530888560);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (5,'client_4.png','','',160,100,NULL,'client_4_421f8dbe79','.png','image/png',18.96,'/uploads/client_4_421f8dbe79.png',NULL,'local',NULL,1,1,1633530888574,1633530888582);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (6,'client_1.png','','',160,100,NULL,'client_1_ffa3c209c6','.png','image/png',8.8,'/uploads/client_1_ffa3c209c6.png',NULL,'local',NULL,1,1,1633530888596,1633530888604);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (7,'testimonial1-v1-90x90.jpg','','',90,90,NULL,'testimonial1_v1_90x90_95059a1f1d','.jpg','image/jpeg',2.23,'/uploads/testimonial1_v1_90x90_95059a1f1d.jpg',NULL,'local',NULL,1,1,1633531035645,1633531035654);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (8,'testimonial-member2-90x90.jpg','','',90,90,NULL,'testimonial_member2_90x90_c96b6f5c1a','.jpg','image/jpeg',2.11,'/uploads/testimonial_member2_90x90_c96b6f5c1a.jpg',NULL,'local',NULL,1,1,1633531061297,1633531061305);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (9,'testimonial-member1-90x90.jpg','','',90,90,NULL,'testimonial_member1_90x90_7573ad67c1','.jpg','image/jpeg',2.13,'/uploads/testimonial_member1_90x90_7573ad67c1.jpg',NULL,'local',NULL,1,1,1633531141645,1633531141651);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (10,'slider_plans.png','','',780,1100,'{"thumbnail":{"name":"thumbnail_slider_plans.png","hash":"thumbnail_slider_plans_0e4418c0dc","ext":".png","mime":"image/png","width":111,"height":156,"size":31.16,"path":null,"url":"/uploads/thumbnail_slider_plans_0e4418c0dc.png"},"large":{"name":"large_slider_plans.png","hash":"large_slider_plans_0e4418c0dc","ext":".png","mime":"image/png","width":709,"height":1000,"size":843.66,"path":null,"url":"/uploads/large_slider_plans_0e4418c0dc.png"},"medium":{"name":"medium_slider_plans.png","hash":"medium_slider_plans_0e4418c0dc","ext":".png","mime":"image/png","width":532,"height":750,"size":506.71,"path":null,"url":"/uploads/medium_slider_plans_0e4418c0dc.png"},"small":{"name":"small_slider_plans.png","hash":"small_slider_plans_0e4418c0dc","ext":".png","mime":"image/png","width":355,"height":500,"size":240.53,"path":null,"url":"/uploads/small_slider_plans_0e4418c0dc.png"}}','slider_plans_0e4418c0dc','.png','image/png',229.97,'/uploads/slider_plans_0e4418c0dc.png',NULL,'local',NULL,1,1,1633532933240,1633532933253);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (11,'featured_casino2-1900x500.jpg','','',1900,500,'{"thumbnail":{"name":"thumbnail_featured_casino2-1900x500.jpg","hash":"thumbnail_featured_casino2_1900x500_ddd415e53c","ext":".jpg","mime":"image/jpeg","width":245,"height":64,"size":4.34,"path":null,"url":"/uploads/thumbnail_featured_casino2_1900x500_ddd415e53c.jpg"},"large":{"name":"large_featured_casino2-1900x500.jpg","hash":"large_featured_casino2_1900x500_ddd415e53c","ext":".jpg","mime":"image/jpeg","width":1000,"height":263,"size":31.14,"path":null,"url":"/uploads/large_featured_casino2_1900x500_ddd415e53c.jpg"},"medium":{"name":"medium_featured_casino2-1900x500.jpg","hash":"medium_featured_casino2_1900x500_ddd415e53c","ext":".jpg","mime":"image/jpeg","width":750,"height":197,"size":20.5,"path":null,"url":"/uploads/medium_featured_casino2_1900x500_ddd415e53c.jpg"},"small":{"name":"small_featured_casino2-1900x500.jpg","hash":"small_featured_casino2_1900x500_ddd415e53c","ext":".jpg","mime":"image/jpeg","width":500,"height":132,"size":11.91,"path":null,"url":"/uploads/small_featured_casino2_1900x500_ddd415e53c.jpg"}}','featured_casino2_1900x500_ddd415e53c','.jpg','image/jpeg',75.92,'/uploads/featured_casino2_1900x500_ddd415e53c.jpg',NULL,'local',NULL,1,1,1633535090013,1633535090020);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (13,'slots-img7-1.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img7-1.jpg","hash":"thumbnail_slots_img7_1_ccba9c97f4","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":8.26,"path":null,"url":"/uploads/thumbnail_slots_img7_1_ccba9c97f4.jpg"},"large":{"name":"large_slots-img7-1.jpg","hash":"large_slots_img7_1_ccba9c97f4","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":75.35,"path":null,"url":"/uploads/large_slots_img7_1_ccba9c97f4.jpg"},"medium":{"name":"medium_slots-img7-1.jpg","hash":"medium_slots_img7_1_ccba9c97f4","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":48.23,"path":null,"url":"/uploads/medium_slots_img7_1_ccba9c97f4.jpg"},"small":{"name":"small_slots-img7-1.jpg","hash":"small_slots_img7_1_ccba9c97f4","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":25.31,"path":null,"url":"/uploads/small_slots_img7_1_ccba9c97f4.jpg"}}','slots_img7_1_ccba9c97f4','.jpg','image/jpeg',144.07,'/uploads/slots_img7_1_ccba9c97f4.jpg',NULL,'local',NULL,1,1,1633615562414,1633615562421);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (14,'slots-img6.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img6.jpg","hash":"thumbnail_slots_img6_d2469bddf4","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":7.6,"path":null,"url":"/uploads/thumbnail_slots_img6_d2469bddf4.jpg"},"large":{"name":"large_slots-img6.jpg","hash":"large_slots_img6_d2469bddf4","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":65.7,"path":null,"url":"/uploads/large_slots_img6_d2469bddf4.jpg"},"medium":{"name":"medium_slots-img6.jpg","hash":"medium_slots_img6_d2469bddf4","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":42.75,"path":null,"url":"/uploads/medium_slots_img6_d2469bddf4.jpg"},"small":{"name":"small_slots-img6.jpg","hash":"small_slots_img6_d2469bddf4","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":22.54,"path":null,"url":"/uploads/small_slots_img6_d2469bddf4.jpg"}}','slots_img6_d2469bddf4','.jpg','image/jpeg',116.65,'/uploads/slots_img6_d2469bddf4.jpg',NULL,'local',NULL,1,1,1633615639949,1633615639958);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (15,'slots-img5.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img5.jpg","hash":"thumbnail_slots_img5_654b9d90d5","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":8.82,"path":null,"url":"/uploads/thumbnail_slots_img5_654b9d90d5.jpg"},"large":{"name":"large_slots-img5.jpg","hash":"large_slots_img5_654b9d90d5","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":65.27,"path":null,"url":"/uploads/large_slots_img5_654b9d90d5.jpg"},"medium":{"name":"medium_slots-img5.jpg","hash":"medium_slots_img5_654b9d90d5","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":43.95,"path":null,"url":"/uploads/medium_slots_img5_654b9d90d5.jpg"},"small":{"name":"small_slots-img5.jpg","hash":"small_slots_img5_654b9d90d5","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":24.75,"path":null,"url":"/uploads/small_slots_img5_654b9d90d5.jpg"}}','slots_img5_654b9d90d5','.jpg','image/jpeg',112.24,'/uploads/slots_img5_654b9d90d5.jpg',NULL,'local',NULL,1,1,1633615732933,1633615732943);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (16,'slots-img4.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img4.jpg","hash":"thumbnail_slots_img4_d726517107","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":5.95,"path":null,"url":"/uploads/thumbnail_slots_img4_d726517107.jpg"},"large":{"name":"large_slots-img4.jpg","hash":"large_slots_img4_d726517107","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":45.55,"path":null,"url":"/uploads/large_slots_img4_d726517107.jpg"},"medium":{"name":"medium_slots-img4.jpg","hash":"medium_slots_img4_d726517107","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":29.47,"path":null,"url":"/uploads/medium_slots_img4_d726517107.jpg"},"small":{"name":"small_slots-img4.jpg","hash":"small_slots_img4_d726517107","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":16.16,"path":null,"url":"/uploads/small_slots_img4_d726517107.jpg"}}','slots_img4_d726517107','.jpg','image/jpeg',83.18,'/uploads/slots_img4_d726517107.jpg',NULL,'local',NULL,1,1,1633615782645,1633615782653);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (17,'slots-img1.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img1.jpg","hash":"thumbnail_slots_img1_4e704c0824","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":7.78,"path":null,"url":"/uploads/thumbnail_slots_img1_4e704c0824.jpg"},"large":{"name":"large_slots-img1.jpg","hash":"large_slots_img1_4e704c0824","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":53.9,"path":null,"url":"/uploads/large_slots_img1_4e704c0824.jpg"},"medium":{"name":"medium_slots-img1.jpg","hash":"medium_slots_img1_4e704c0824","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":36.83,"path":null,"url":"/uploads/medium_slots_img1_4e704c0824.jpg"},"small":{"name":"small_slots-img1.jpg","hash":"small_slots_img1_4e704c0824","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":21.11,"path":null,"url":"/uploads/small_slots_img1_4e704c0824.jpg"}}','slots_img1_4e704c0824','.jpg','image/jpeg',88.01,'/uploads/slots_img1_4e704c0824.jpg',NULL,'local',NULL,1,1,1633615831156,1633615831166);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (18,'slots-img2.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img2.jpg","hash":"thumbnail_slots_img2_81e7c21a56","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":6.37,"path":null,"url":"/uploads/thumbnail_slots_img2_81e7c21a56.jpg"},"large":{"name":"large_slots-img2.jpg","hash":"large_slots_img2_81e7c21a56","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":48.76,"path":null,"url":"/uploads/large_slots_img2_81e7c21a56.jpg"},"medium":{"name":"medium_slots-img2.jpg","hash":"medium_slots_img2_81e7c21a56","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":31.92,"path":null,"url":"/uploads/medium_slots_img2_81e7c21a56.jpg"},"small":{"name":"small_slots-img2.jpg","hash":"small_slots_img2_81e7c21a56","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":17.45,"path":null,"url":"/uploads/small_slots_img2_81e7c21a56.jpg"}}','slots_img2_81e7c21a56','.jpg','image/jpeg',87.81,'/uploads/slots_img2_81e7c21a56.jpg',NULL,'local',NULL,1,1,1633615879134,1633615879143);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (19,'slots-img3-505x305.jpg','','',505,305,'{"thumbnail":{"name":"thumbnail_slots-img3-505x305.jpg","hash":"thumbnail_slots_img3_505x305_8f41420a5c","ext":".jpg","mime":"image/jpeg","width":245,"height":148,"size":12.84,"path":null,"url":"/uploads/thumbnail_slots_img3_505x305_8f41420a5c.jpg"},"small":{"name":"small_slots-img3-505x305.jpg","hash":"small_slots_img3_505x305_8f41420a5c","ext":".jpg","mime":"image/jpeg","width":500,"height":302,"size":36.97,"path":null,"url":"/uploads/small_slots_img3_505x305_8f41420a5c.jpg"}}','slots_img3_505x305_8f41420a5c','.jpg','image/jpeg',39.18,'/uploads/slots_img3_505x305_8f41420a5c.jpg',NULL,'local',NULL,1,1,1633615919548,1633615919555);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (20,'1-1.jpg','','',360,360,'{"thumbnail":{"name":"thumbnail_1-1.jpg","hash":"thumbnail_1_1_acceb5bd67","ext":".jpg","mime":"image/jpeg","width":156,"height":156,"size":5.22,"path":null,"url":"/uploads/thumbnail_1_1_acceb5bd67.jpg"}}','1_1_acceb5bd67','.jpg','image/jpeg',17.8,'/uploads/1_1_acceb5bd67.jpg',NULL,'local',NULL,1,1,1633618154492,1633618154501);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (21,'esport-betting-slot2.jpg','','',360,360,'{"thumbnail":{"name":"thumbnail_esport-betting-slot2.jpg","hash":"thumbnail_esport_betting_slot2_1bcd78bf3f","ext":".jpg","mime":"image/jpeg","width":156,"height":156,"size":6.97,"path":null,"url":"/uploads/thumbnail_esport_betting_slot2_1bcd78bf3f.jpg"}}','esport_betting_slot2_1bcd78bf3f','.jpg','image/jpeg',27.66,'/uploads/esport_betting_slot2_1bcd78bf3f.jpg',NULL,'local',NULL,1,1,1633618220192,1633618220199);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (22,'esport-betting-slot3.jpg','','',360,360,'{"thumbnail":{"name":"thumbnail_esport-betting-slot3.jpg","hash":"thumbnail_esport_betting_slot3_6d3dd052e2","ext":".jpg","mime":"image/jpeg","width":156,"height":156,"size":7.7,"path":null,"url":"/uploads/thumbnail_esport_betting_slot3_6d3dd052e2.jpg"}}','esport_betting_slot3_6d3dd052e2','.jpg','image/jpeg',30.77,'/uploads/esport_betting_slot3_6d3dd052e2.jpg',NULL,'local',NULL,1,1,1633618349574,1633618349582);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (23,'esport-betting-slot4.jpg','','',360,360,'{"thumbnail":{"name":"thumbnail_esport-betting-slot4.jpg","hash":"thumbnail_esport_betting_slot4_f865dbf43f","ext":".jpg","mime":"image/jpeg","width":156,"height":156,"size":7.34,"path":null,"url":"/uploads/thumbnail_esport_betting_slot4_f865dbf43f.jpg"}}','esport_betting_slot4_f865dbf43f','.jpg','image/jpeg',33.8,'/uploads/esport_betting_slot4_f865dbf43f.jpg',NULL,'local',NULL,1,1,1633618428352,1633618428360);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (24,'esport-betting-slot5.jpg','','',360,360,'{"thumbnail":{"name":"thumbnail_esport-betting-slot5.jpg","hash":"thumbnail_esport_betting_slot5_6a6390ce14","ext":".jpg","mime":"image/jpeg","width":156,"height":156,"size":7.74,"path":null,"url":"/uploads/thumbnail_esport_betting_slot5_6a6390ce14.jpg"}}','esport_betting_slot5_6a6390ce14','.jpg','image/jpeg',31.1,'/uploads/esport_betting_slot5_6a6390ce14.jpg',NULL,'local',NULL,1,1,1633618458998,1633618459006);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (25,'slots-img10.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img10.jpg","hash":"thumbnail_slots_img10_a53cab6ec4","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":7.02,"path":null,"url":"/uploads/thumbnail_slots_img10_a53cab6ec4.jpg"},"large":{"name":"large_slots-img10.jpg","hash":"large_slots_img10_a53cab6ec4","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":50.29,"path":null,"url":"/uploads/large_slots_img10_a53cab6ec4.jpg"},"medium":{"name":"medium_slots-img10.jpg","hash":"medium_slots_img10_a53cab6ec4","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":34.1,"path":null,"url":"/uploads/medium_slots_img10_a53cab6ec4.jpg"},"small":{"name":"small_slots-img10.jpg","hash":"small_slots_img10_a53cab6ec4","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":18.9,"path":null,"url":"/uploads/small_slots_img10_a53cab6ec4.jpg"}}','slots_img10_a53cab6ec4','.jpg','image/jpeg',88.49,'/uploads/slots_img10_a53cab6ec4.jpg',NULL,'local',NULL,1,1,1633618805434,1633618805445);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (26,'slots-img8 (1).jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img8 (1).jpg","hash":"thumbnail_slots_img8_1_aa97bd63c7","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":8.2,"path":null,"url":"/uploads/thumbnail_slots_img8_1_aa97bd63c7.jpg"},"large":{"name":"large_slots-img8 (1).jpg","hash":"large_slots_img8_1_aa97bd63c7","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":60.75,"path":null,"url":"/uploads/large_slots_img8_1_aa97bd63c7.jpg"},"medium":{"name":"medium_slots-img8 (1).jpg","hash":"medium_slots_img8_1_aa97bd63c7","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":39.72,"path":null,"url":"/uploads/medium_slots_img8_1_aa97bd63c7.jpg"},"small":{"name":"small_slots-img8 (1).jpg","hash":"small_slots_img8_1_aa97bd63c7","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":22.01,"path":null,"url":"/uploads/small_slots_img8_1_aa97bd63c7.jpg"}}','slots_img8_1_aa97bd63c7','.jpg','image/jpeg',106.3,'/uploads/slots_img8_1_aa97bd63c7.jpg',NULL,'local',NULL,1,1,1633618849222,1633618849230);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (27,'slots-img1 (1).jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img1 (1).jpg","hash":"thumbnail_slots_img1_1_96ac151874","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":7.78,"path":null,"url":"/uploads/thumbnail_slots_img1_1_96ac151874.jpg"},"large":{"name":"large_slots-img1 (1).jpg","hash":"large_slots_img1_1_96ac151874","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":53.9,"path":null,"url":"/uploads/large_slots_img1_1_96ac151874.jpg"},"medium":{"name":"medium_slots-img1 (1).jpg","hash":"medium_slots_img1_1_96ac151874","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":36.83,"path":null,"url":"/uploads/medium_slots_img1_1_96ac151874.jpg"},"small":{"name":"small_slots-img1 (1).jpg","hash":"small_slots_img1_1_96ac151874","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":21.11,"path":null,"url":"/uploads/small_slots_img1_1_96ac151874.jpg"}}','slots_img1_1_96ac151874','.jpg','image/jpeg',88.01,'/uploads/slots_img1_1_96ac151874.jpg',NULL,'local',NULL,1,1,1633618955428,1633618955437);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (28,'slots-img2 (1).jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img2 (1).jpg","hash":"thumbnail_slots_img2_1_5a305683a1","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":6.37,"path":null,"url":"/uploads/thumbnail_slots_img2_1_5a305683a1.jpg"},"large":{"name":"large_slots-img2 (1).jpg","hash":"large_slots_img2_1_5a305683a1","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":48.76,"path":null,"url":"/uploads/large_slots_img2_1_5a305683a1.jpg"},"medium":{"name":"medium_slots-img2 (1).jpg","hash":"medium_slots_img2_1_5a305683a1","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":31.92,"path":null,"url":"/uploads/medium_slots_img2_1_5a305683a1.jpg"},"small":{"name":"small_slots-img2 (1).jpg","hash":"small_slots_img2_1_5a305683a1","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":17.45,"path":null,"url":"/uploads/small_slots_img2_1_5a305683a1.jpg"}}','slots_img2_1_5a305683a1','.jpg','image/jpeg',87.81,'/uploads/slots_img2_1_5a305683a1.jpg',NULL,'local',NULL,1,1,1633618991407,1633618991414);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (29,'tarzan-jungle.jpg','','',3840,2160,'{"thumbnail":{"name":"thumbnail_tarzan-jungle.jpg","hash":"thumbnail_tarzan_jungle_fa0b317b9c","ext":".jpg","mime":"image/jpeg","width":245,"height":138,"size":9.39,"path":null,"url":"/uploads/thumbnail_tarzan_jungle_fa0b317b9c.jpg"},"large":{"name":"large_tarzan-jungle.jpg","hash":"large_tarzan_jungle_fa0b317b9c","ext":".jpg","mime":"image/jpeg","width":1000,"height":563,"size":79.64,"path":null,"url":"/uploads/large_tarzan_jungle_fa0b317b9c.jpg"},"medium":{"name":"medium_tarzan-jungle.jpg","hash":"medium_tarzan_jungle_fa0b317b9c","ext":".jpg","mime":"image/jpeg","width":750,"height":422,"size":51.65,"path":null,"url":"/uploads/medium_tarzan_jungle_fa0b317b9c.jpg"},"small":{"name":"small_tarzan-jungle.jpg","hash":"small_tarzan_jungle_fa0b317b9c","ext":".jpg","mime":"image/jpeg","width":500,"height":281,"size":28.75,"path":null,"url":"/uploads/small_tarzan_jungle_fa0b317b9c.jpg"}}','tarzan_jungle_fa0b317b9c','.jpg','image/jpeg',458.83,'/uploads/tarzan_jungle_fa0b317b9c.jpg',NULL,'local',NULL,1,1,1633619904617,1633619904639);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (30,'Book-of-Deluxe.jpg','','',3728,2480,'{"thumbnail":{"name":"thumbnail_Book-of-Deluxe.jpg","hash":"thumbnail_Book_of_Deluxe_2b0c781175","ext":".jpg","mime":"image/jpeg","width":235,"height":156,"size":10.14,"path":null,"url":"/uploads/thumbnail_Book_of_Deluxe_2b0c781175.jpg"},"large":{"name":"large_Book-of-Deluxe.jpg","hash":"large_Book_of_Deluxe_2b0c781175","ext":".jpg","mime":"image/jpeg","width":1000,"height":665,"size":101.71,"path":null,"url":"/uploads/large_Book_of_Deluxe_2b0c781175.jpg"},"medium":{"name":"medium_Book-of-Deluxe.jpg","hash":"medium_Book_of_Deluxe_2b0c781175","ext":".jpg","mime":"image/jpeg","width":750,"height":499,"size":64.6,"path":null,"url":"/uploads/medium_Book_of_Deluxe_2b0c781175.jpg"},"small":{"name":"small_Book-of-Deluxe.jpg","hash":"small_Book_of_Deluxe_2b0c781175","ext":".jpg","mime":"image/jpeg","width":500,"height":333,"size":34.33,"path":null,"url":"/uploads/small_Book_of_Deluxe_2b0c781175.jpg"}}','Book_of_Deluxe_2b0c781175','.jpg','image/jpeg',509.3,'/uploads/Book_of_Deluxe_2b0c781175.jpg',NULL,'local',NULL,1,1,1633619904653,1633619904668);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (31,'gonzos-quest.jpg','','',4096,2160,'{"thumbnail":{"name":"thumbnail_gonzos-quest.jpg","hash":"thumbnail_gonzos_quest_82464e1898","ext":".jpg","mime":"image/jpeg","width":245,"height":129,"size":9.01,"path":null,"url":"/uploads/thumbnail_gonzos_quest_82464e1898.jpg"},"large":{"name":"large_gonzos-quest.jpg","hash":"large_gonzos_quest_82464e1898","ext":".jpg","mime":"image/jpeg","width":1000,"height":527,"size":83.73,"path":null,"url":"/uploads/large_gonzos_quest_82464e1898.jpg"},"medium":{"name":"medium_gonzos-quest.jpg","hash":"medium_gonzos_quest_82464e1898","ext":".jpg","mime":"image/jpeg","width":750,"height":396,"size":54.11,"path":null,"url":"/uploads/medium_gonzos_quest_82464e1898.jpg"},"small":{"name":"small_gonzos-quest.jpg","hash":"small_gonzos_quest_82464e1898","ext":".jpg","mime":"image/jpeg","width":500,"height":264,"size":27.76,"path":null,"url":"/uploads/small_gonzos_quest_82464e1898.jpg"}}','gonzos_quest_82464e1898','.jpg','image/jpeg',513.69,'/uploads/gonzos_quest_82464e1898.jpg',NULL,'local',NULL,1,1,1633619904684,1633619904695);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (32,'soccer-ball.jpg','','',1200,1000,'{"thumbnail":{"name":"thumbnail_soccer-ball.jpg","hash":"thumbnail_soccer_ball_a456d9d320","ext":".jpg","mime":"image/jpeg","width":187,"height":156,"size":9.17,"path":null,"url":"/uploads/thumbnail_soccer_ball_a456d9d320.jpg"},"large":{"name":"large_soccer-ball.jpg","hash":"large_soccer_ball_a456d9d320","ext":".jpg","mime":"image/jpeg","width":1000,"height":833,"size":141.81,"path":null,"url":"/uploads/large_soccer_ball_a456d9d320.jpg"},"medium":{"name":"medium_soccer-ball.jpg","hash":"medium_soccer_ball_a456d9d320","ext":".jpg","mime":"image/jpeg","width":750,"height":625,"size":90.54,"path":null,"url":"/uploads/medium_soccer_ball_a456d9d320.jpg"},"small":{"name":"small_soccer-ball.jpg","hash":"small_soccer_ball_a456d9d320","ext":".jpg","mime":"image/jpeg","width":500,"height":417,"size":46.03,"path":null,"url":"/uploads/small_soccer_ball_a456d9d320.jpg"}}','soccer_ball_a456d9d320','.jpg','image/jpeg',188.44,'/uploads/soccer_ball_a456d9d320.jpg',NULL,'local',NULL,1,1,1633619905047,1633619905053);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (33,'deadwood.jpg','','',6000,3600,'{"thumbnail":{"name":"thumbnail_deadwood.jpg","hash":"thumbnail_deadwood_a10650cd32","ext":".jpg","mime":"image/jpeg","width":245,"height":147,"size":14.35,"path":null,"url":"/uploads/thumbnail_deadwood_a10650cd32.jpg"},"large":{"name":"large_deadwood.jpg","hash":"large_deadwood_a10650cd32","ext":".jpg","mime":"image/jpeg","width":1000,"height":600,"size":156.23,"path":null,"url":"/uploads/large_deadwood_a10650cd32.jpg"},"medium":{"name":"medium_deadwood.jpg","hash":"medium_deadwood_a10650cd32","ext":".jpg","mime":"image/jpeg","width":750,"height":450,"size":94.97,"path":null,"url":"/uploads/medium_deadwood_a10650cd32.jpg"},"small":{"name":"small_deadwood.jpg","hash":"small_deadwood_a10650cd32","ext":".jpg","mime":"image/jpeg","width":500,"height":300,"size":48,"path":null,"url":"/uploads/small_deadwood_a10650cd32.jpg"}}','deadwood_a10650cd32','.jpg','image/jpeg',1809.82,'/uploads/deadwood_a10650cd32.jpg',NULL,'local',NULL,1,1,1633619905066,1633619905171);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (34,'viking-game.jpg','','',4774,2685,'{"thumbnail":{"name":"thumbnail_viking-game.jpg","hash":"thumbnail_viking_game_f4ecfda95d","ext":".jpg","mime":"image/jpeg","width":245,"height":138,"size":8.57,"path":null,"url":"/uploads/thumbnail_viking_game_f4ecfda95d.jpg"},"large":{"name":"large_viking-game.jpg","hash":"large_viking_game_f4ecfda95d","ext":".jpg","mime":"image/jpeg","width":1000,"height":562,"size":77.67,"path":null,"url":"/uploads/large_viking_game_f4ecfda95d.jpg"},"medium":{"name":"medium_viking-game.jpg","hash":"medium_viking_game_f4ecfda95d","ext":".jpg","mime":"image/jpeg","width":750,"height":422,"size":50.66,"path":null,"url":"/uploads/medium_viking_game_f4ecfda95d.jpg"},"small":{"name":"small_viking-game.jpg","hash":"small_viking_game_f4ecfda95d","ext":".jpg","mime":"image/jpeg","width":500,"height":281,"size":26.65,"path":null,"url":"/uploads/small_viking_game_f4ecfda95d.jpg"}}','viking_game_f4ecfda95d','.jpg','image/jpeg',760.45,'/uploads/viking_game_f4ecfda95d.jpg',NULL,'local',NULL,1,1,1633619905185,1633619905193);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (35,'narcos.jpg','','',5000,2800,'{"thumbnail":{"name":"thumbnail_narcos.jpg","hash":"thumbnail_narcos_b25b105c16","ext":".jpg","mime":"image/jpeg","width":245,"height":137,"size":10.52,"path":null,"url":"/uploads/thumbnail_narcos_b25b105c16.jpg"},"large":{"name":"large_narcos.jpg","hash":"large_narcos_b25b105c16","ext":".jpg","mime":"image/jpeg","width":1000,"height":560,"size":102.54,"path":null,"url":"/uploads/large_narcos_b25b105c16.jpg"},"medium":{"name":"medium_narcos.jpg","hash":"medium_narcos_b25b105c16","ext":".jpg","mime":"image/jpeg","width":750,"height":420,"size":65.21,"path":null,"url":"/uploads/medium_narcos_b25b105c16.jpg"},"small":{"name":"small_narcos.jpg","hash":"small_narcos_b25b105c16","ext":".jpg","mime":"image/jpeg","width":500,"height":280,"size":34.06,"path":null,"url":"/uploads/small_narcos_b25b105c16.jpg"}}','narcos_b25b105c16','.jpg','image/jpeg',948.47,'/uploads/narcos_b25b105c16.jpg',NULL,'local',NULL,1,1,1633619905212,1633619905221);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (36,'minecraft.jpg','','',8320,4680,'{"thumbnail":{"name":"thumbnail_minecraft.jpg","hash":"thumbnail_minecraft_a11f62ba5c","ext":".jpg","mime":"image/jpeg","width":245,"height":138,"size":7.11,"path":null,"url":"/uploads/thumbnail_minecraft_a11f62ba5c.jpg"},"large":{"name":"large_minecraft.jpg","hash":"large_minecraft_a11f62ba5c","ext":".jpg","mime":"image/jpeg","width":1000,"height":563,"size":83.3,"path":null,"url":"/uploads/large_minecraft_a11f62ba5c.jpg"},"medium":{"name":"medium_minecraft.jpg","hash":"medium_minecraft_a11f62ba5c","ext":".jpg","mime":"image/jpeg","width":750,"height":422,"size":49.85,"path":null,"url":"/uploads/medium_minecraft_a11f62ba5c.jpg"},"small":{"name":"small_minecraft.jpg","hash":"small_minecraft_a11f62ba5c","ext":".jpg","mime":"image/jpeg","width":500,"height":281,"size":24.33,"path":null,"url":"/uploads/small_minecraft_a11f62ba5c.jpg"}}','minecraft_a11f62ba5c','.jpg','image/jpeg',1480.58,'/uploads/minecraft_a11f62ba5c.jpg',NULL,'local',NULL,1,1,1633619905606,1633619905613);
INSERT INTO "upload_file" ("id","name","alternativeText","caption","width","height","formats","hash","ext","mime","size","url","previewUrl","provider","provider_metadata","created_by","updated_by","created_at","updated_at") VALUES (37,'slots-img9.jpg','','',1500,500,'{"thumbnail":{"name":"thumbnail_slots-img9.jpg","hash":"thumbnail_slots_img9_2aa5c4438f","ext":".jpg","mime":"image/jpeg","width":245,"height":82,"size":8.25,"path":null,"url":"/uploads/thumbnail_slots_img9_2aa5c4438f.jpg"},"large":{"name":"large_slots-img9.jpg","hash":"large_slots_img9_2aa5c4438f","ext":".jpg","mime":"image/jpeg","width":1000,"height":333,"size":65.77,"path":null,"url":"/uploads/large_slots_img9_2aa5c4438f.jpg"},"medium":{"name":"medium_slots-img9.jpg","hash":"medium_slots_img9_2aa5c4438f","ext":".jpg","mime":"image/jpeg","width":750,"height":250,"size":43.34,"path":null,"url":"/uploads/medium_slots_img9_2aa5c4438f.jpg"},"small":{"name":"small_slots-img9.jpg","hash":"small_slots_img9_2aa5c4438f","ext":".jpg","mime":"image/jpeg","width":500,"height":167,"size":23.54,"path":null,"url":"/uploads/small_slots_img9_2aa5c4438f.jpg"}}','slots_img9_2aa5c4438f','.jpg','image/jpeg',122.6,'/uploads/slots_img9_2aa5c4438f.jpg',NULL,'local',NULL,1,1,1633620580048,1633620580058);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (7,2,1,'home_partners','slider',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (8,6,1,'home_partners','slider',5);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (9,3,1,'home_partners','slider',2);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (10,4,1,'home_partners','slider',3);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (11,5,1,'home_partners','slider',4);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (12,7,1,'components_misc_testimonials','avatar',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (13,9,3,'components_misc_testimonials','avatar',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (14,8,2,'components_misc_testimonials','avatar',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (15,10,1,'home_headers','image',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (18,1,1,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (19,11,1,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (22,1,3,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (23,11,3,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (26,1,4,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (27,11,4,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (28,1,5,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (29,11,5,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (30,1,6,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (31,11,6,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (32,1,7,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (33,11,7,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (36,1,8,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (37,11,8,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (38,1,9,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (39,11,9,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (40,1,10,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (41,11,10,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (42,1,11,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (43,11,11,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (44,1,12,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (45,11,12,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (46,1,13,'casinos','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (47,11,13,'casinos','cover',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (49,13,2,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (51,14,3,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (53,15,4,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (54,16,5,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (55,17,6,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (56,18,7,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (58,19,8,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (59,20,9,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (60,21,10,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (61,22,11,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (62,23,12,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (63,24,13,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (64,25,14,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (65,26,1,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (66,27,15,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (67,28,16,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (68,19,17,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (69,30,33,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (70,29,32,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (71,31,31,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (73,33,30,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (74,34,29,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (75,35,28,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (76,36,27,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (77,32,26,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (78,17,25,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (79,19,24,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (80,17,23,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (82,37,22,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (83,37,21,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (84,19,20,'slots','thumbnail',1);
INSERT INTO "upload_file_morph" ("id","upload_file_id","related_id","related_type","field","order") VALUES (85,28,18,'slots','thumbnail',1);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (1,'application','casino','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (2,'application','casino','count',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (3,'application','casino','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (4,'application','casino','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (5,'application','casino','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (6,'application','casino','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (7,'application','casino','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (8,'application','casino','find',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (9,'application','casino','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (10,'application','casino','findone',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (11,'application','casino','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (12,'application','casino','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (13,'application','tag-types','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (14,'application','tag-types','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (15,'application','tag-types','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (16,'application','tag-types','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (17,'application','tag-types','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (18,'application','tag-types','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (19,'application','tag-types','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (20,'application','tag-types','find',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (21,'application','tag-types','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (22,'application','tag-types','findone',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (23,'application','tag-types','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (24,'application','tag-types','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (25,'application','tags','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (26,'application','tags','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (27,'application','tags','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (28,'application','tags','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (29,'application','tags','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (30,'application','tags','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (31,'application','tags','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (32,'application','tags','find',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (33,'application','tags','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (34,'application','tags','findone',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (35,'application','tags','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (36,'application','tags','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (37,'content-manager','collection-types','bulkdelete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (38,'content-manager','collection-types','bulkdelete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (39,'content-manager','collection-types','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (40,'content-manager','collection-types','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (41,'content-manager','collection-types','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (42,'content-manager','collection-types','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (43,'content-manager','collection-types','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (44,'content-manager','collection-types','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (45,'content-manager','collection-types','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (46,'content-manager','collection-types','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (47,'content-manager','collection-types','previewmanyrelations',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (48,'content-manager','collection-types','previewmanyrelations',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (49,'content-manager','collection-types','publish',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (50,'content-manager','collection-types','publish',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (51,'content-manager','collection-types','unpublish',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (52,'content-manager','collection-types','unpublish',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (53,'content-manager','collection-types','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (54,'content-manager','collection-types','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (55,'content-manager','components','findcomponentconfiguration',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (56,'content-manager','components','findcomponentconfiguration',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (57,'content-manager','components','findcomponents',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (58,'content-manager','components','findcomponents',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (59,'content-manager','components','updatecomponentconfiguration',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (60,'content-manager','components','updatecomponentconfiguration',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (61,'content-manager','content-types','findcontenttypeconfiguration',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (62,'content-manager','content-types','findcontenttypeconfiguration',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (63,'content-manager','content-types','findcontenttypes',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (64,'content-manager','content-types','findcontenttypes',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (65,'content-manager','content-types','findcontenttypessettings',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (66,'content-manager','content-types','findcontenttypessettings',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (67,'content-manager','content-types','updatecontenttypeconfiguration',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (68,'content-manager','content-types','updatecontenttypeconfiguration',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (69,'content-manager','relations','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (70,'content-manager','relations','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (71,'content-manager','single-types','createorupdate',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (72,'content-manager','single-types','createorupdate',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (73,'content-manager','single-types','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (74,'content-manager','single-types','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (75,'content-manager','single-types','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (76,'content-manager','single-types','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (77,'content-manager','single-types','publish',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (78,'content-manager','single-types','publish',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (79,'content-manager','single-types','unpublish',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (80,'content-manager','single-types','unpublish',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (81,'content-manager','uid','checkuidavailability',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (82,'content-manager','uid','checkuidavailability',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (83,'content-manager','uid','generateuid',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (84,'content-manager','uid','generateuid',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (85,'content-type-builder','builder','getreservednames',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (86,'content-type-builder','builder','getreservednames',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (87,'content-type-builder','componentcategories','deletecategory',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (88,'content-type-builder','componentcategories','deletecategory',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (89,'content-type-builder','componentcategories','editcategory',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (90,'content-type-builder','componentcategories','editcategory',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (91,'content-type-builder','components','createcomponent',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (92,'content-type-builder','components','createcomponent',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (93,'content-type-builder','components','deletecomponent',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (94,'content-type-builder','components','deletecomponent',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (95,'content-type-builder','components','getcomponent',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (96,'content-type-builder','components','getcomponent',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (97,'content-type-builder','components','getcomponents',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (98,'content-type-builder','components','getcomponents',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (99,'content-type-builder','components','updatecomponent',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (100,'content-type-builder','components','updatecomponent',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (101,'content-type-builder','connections','getconnections',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (102,'content-type-builder','connections','getconnections',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (103,'content-type-builder','contenttypes','createcontenttype',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (104,'content-type-builder','contenttypes','createcontenttype',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (105,'content-type-builder','contenttypes','deletecontenttype',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (106,'content-type-builder','contenttypes','deletecontenttype',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (107,'content-type-builder','contenttypes','getcontenttype',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (108,'content-type-builder','contenttypes','getcontenttype',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (109,'content-type-builder','contenttypes','getcontenttypes',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (110,'content-type-builder','contenttypes','getcontenttypes',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (111,'content-type-builder','contenttypes','updatecontenttype',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (112,'content-type-builder','contenttypes','updatecontenttype',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (113,'email','email','getsettings',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (114,'email','email','getsettings',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (115,'email','email','send',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (116,'email','email','send',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (117,'email','email','test',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (118,'email','email','test',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (119,'i18n','content-types','getnonlocalizedattributes',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (120,'i18n','content-types','getnonlocalizedattributes',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (121,'i18n','iso-locales','listisolocales',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (122,'i18n','iso-locales','listisolocales',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (123,'i18n','locales','createlocale',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (124,'i18n','locales','createlocale',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (125,'i18n','locales','deletelocale',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (126,'i18n','locales','deletelocale',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (127,'i18n','locales','listlocales',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (128,'i18n','locales','listlocales',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (129,'i18n','locales','updatelocale',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (130,'i18n','locales','updatelocale',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (131,'upload','upload','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (132,'upload','upload','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (133,'upload','upload','destroy',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (134,'upload','upload','destroy',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (135,'upload','upload','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (136,'upload','upload','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (137,'upload','upload','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (138,'upload','upload','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (139,'upload','upload','getsettings',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (140,'upload','upload','getsettings',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (141,'upload','upload','search',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (142,'upload','upload','search',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (143,'upload','upload','updatesettings',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (144,'upload','upload','updatesettings',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (145,'upload','upload','upload',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (146,'upload','upload','upload',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (147,'users-permissions','auth','callback',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (148,'users-permissions','auth','callback',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (149,'users-permissions','auth','connect',1,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (150,'users-permissions','auth','connect',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (151,'users-permissions','auth','emailconfirmation',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (152,'users-permissions','auth','emailconfirmation',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (153,'users-permissions','auth','forgotpassword',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (154,'users-permissions','auth','forgotpassword',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (155,'users-permissions','auth','register',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (156,'users-permissions','auth','register',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (157,'users-permissions','auth','resetpassword',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (158,'users-permissions','auth','resetpassword',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (159,'users-permissions','auth','sendemailconfirmation',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (160,'users-permissions','auth','sendemailconfirmation',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (161,'users-permissions','user','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (162,'users-permissions','user','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (163,'users-permissions','user','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (164,'users-permissions','user','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (165,'users-permissions','user','destroy',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (166,'users-permissions','user','destroy',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (167,'users-permissions','user','destroyall',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (168,'users-permissions','user','destroyall',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (169,'users-permissions','user','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (170,'users-permissions','user','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (171,'users-permissions','user','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (172,'users-permissions','user','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (173,'users-permissions','user','me',1,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (174,'users-permissions','user','me',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (175,'users-permissions','user','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (176,'users-permissions','user','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (177,'users-permissions','userspermissions','createrole',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (178,'users-permissions','userspermissions','createrole',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (179,'users-permissions','userspermissions','deleterole',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (180,'users-permissions','userspermissions','deleterole',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (181,'users-permissions','userspermissions','getadvancedsettings',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (182,'users-permissions','userspermissions','getadvancedsettings',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (183,'users-permissions','userspermissions','getemailtemplate',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (184,'users-permissions','userspermissions','getemailtemplate',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (185,'users-permissions','userspermissions','getpermissions',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (186,'users-permissions','userspermissions','getpermissions',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (187,'users-permissions','userspermissions','getpolicies',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (188,'users-permissions','userspermissions','getpolicies',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (189,'users-permissions','userspermissions','getproviders',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (190,'users-permissions','userspermissions','getproviders',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (191,'users-permissions','userspermissions','getrole',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (192,'users-permissions','userspermissions','getrole',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (193,'users-permissions','userspermissions','getroles',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (194,'users-permissions','userspermissions','getroles',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (195,'users-permissions','userspermissions','getroutes',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (196,'users-permissions','userspermissions','getroutes',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (197,'users-permissions','userspermissions','index',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (198,'users-permissions','userspermissions','index',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (199,'users-permissions','userspermissions','searchusers',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (200,'users-permissions','userspermissions','searchusers',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (201,'users-permissions','userspermissions','updateadvancedsettings',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (202,'users-permissions','userspermissions','updateadvancedsettings',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (203,'users-permissions','userspermissions','updateemailtemplate',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (204,'users-permissions','userspermissions','updateemailtemplate',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (205,'users-permissions','userspermissions','updateproviders',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (206,'users-permissions','userspermissions','updateproviders',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (207,'users-permissions','userspermissions','updaterole',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (208,'users-permissions','userspermissions','updaterole',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (209,'application','nav-links','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (210,'application','nav-links','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (211,'application','nav-links','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (212,'application','nav-links','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (213,'application','nav-links','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (214,'application','nav-links','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (215,'application','nav-links','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (216,'application','nav-links','find',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (217,'application','nav-links','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (218,'application','nav-links','findone',1,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (219,'application','nav-links','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (220,'application','nav-links','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (227,'application','slot','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (228,'application','slot','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (229,'application','slot','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (230,'application','slot','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (231,'application','slot','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (232,'application','slot','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (233,'application','slot','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (234,'application','slot','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (235,'application','slot','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (236,'application','slot','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (237,'application','slot','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (238,'application','slot','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (239,'application','comment','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (240,'application','comment','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (241,'application','comment','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (242,'application','comment','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (243,'application','comment','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (244,'application','comment','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (245,'application','comment','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (246,'application','comment','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (247,'application','comment','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (248,'application','comment','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (249,'application','comment','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (250,'application','comment','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (251,'application','bonus','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (252,'application','bonus','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (253,'application','bonus','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (254,'application','bonus','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (255,'application','bonus','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (256,'application','bonus','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (257,'application','bonus','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (258,'application','bonus','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (259,'application','bonus','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (260,'application','bonus','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (261,'application','bonus','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (262,'application','bonus','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (263,'application','bookmaker','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (264,'application','bookmaker','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (265,'application','bookmaker','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (266,'application','bookmaker','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (267,'application','bookmaker','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (268,'application','bookmaker','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (269,'application','bookmaker','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (270,'application','bookmaker','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (271,'application','bookmaker','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (272,'application','bookmaker','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (273,'application','bookmaker','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (274,'application','bookmaker','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (287,'application','blog-tags','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (288,'application','blog-tags','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (289,'application','blog-tags','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (290,'application','blog-tags','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (291,'application','blog-tags','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (292,'application','blog-tags','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (293,'application','blog-tags','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (294,'application','blog-tags','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (295,'application','blog-tags','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (296,'application','blog-tags','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (297,'application','blog-tags','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (298,'application','blog-tags','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (299,'application','home-header','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (300,'application','home-header','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (301,'application','home-header','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (302,'application','home-header','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (303,'application','home-header','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (304,'application','home-header','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (305,'application','home-characteristics','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (306,'application','home-characteristics','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (307,'application','home-characteristics','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (308,'application','home-characteristics','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (309,'application','home-characteristics','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (310,'application','home-characteristics','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (311,'application','home-faq','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (312,'application','home-faq','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (313,'application','home-faq','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (314,'application','home-faq','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (315,'application','home-faq','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (316,'application','home-faq','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (317,'application','home-testimonials','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (318,'application','home-testimonials','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (319,'application','home-testimonials','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (320,'application','home-testimonials','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (321,'application','home-testimonials','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (322,'application','home-testimonials','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (323,'application','home-get-in-touch','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (324,'application','home-get-in-touch','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (325,'application','home-get-in-touch','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (326,'application','home-get-in-touch','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (327,'application','home-get-in-touch','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (328,'application','home-get-in-touch','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (329,'application','home-partners','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (330,'application','home-partners','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (331,'application','home-partners','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (332,'application','home-partners','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (333,'application','home-partners','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (334,'application','home-partners','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (335,'application','footer','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (336,'application','footer','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (337,'application','footer','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (338,'application','footer','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (339,'application','footer','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (340,'application','footer','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (341,'application','menu','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (342,'application','menu','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (343,'application','menu','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (344,'application','menu','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (345,'application','menu','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (346,'application','menu','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (347,'application','review','count',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (348,'application','review','count',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (349,'application','review','create',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (350,'application','review','create',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (351,'application','review','delete',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (352,'application','review','delete',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (353,'application','review','find',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (354,'application','review','find',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (355,'application','review','findone',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (356,'application','review','findone',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (357,'application','review','update',0,'',1,NULL,NULL);
INSERT INTO "users-permissions_permission" ("id","type","controller","action","enabled","policy","role","created_by","updated_by") VALUES (358,'application','review','update',0,'',2,NULL,NULL);
INSERT INTO "users-permissions_role" ("id","name","description","type","created_by","updated_by") VALUES (1,'Authenticated','Default role given to authenticated user.','authenticated',NULL,NULL);
INSERT INTO "users-permissions_role" ("id","name","description","type","created_by","updated_by") VALUES (2,'Public','Default role given to unauthenticated user.','public',NULL,NULL);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (3,'Customer Support',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (4,'Trust&Fairness',80.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (5,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (6,'Commissions',80.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (7,'Entry fees',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (8,'Customer Support',10.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (9,'Entry fees',45.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (10,'Trust&Fairness',78.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (11,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (12,'Commissions',75.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (13,'Customer Support',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (14,'Entry fees',15.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (15,'Trust&Fairness',78.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (16,'Games',45.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (17,'Commissions',42.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (18,'Customer Support',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (19,'Entry fees',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (20,'Trust&Fairness',80.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (21,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (22,'Commissions',80.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (23,'Customer Support',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (24,'Trust&Fairness',55.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (25,'Games',11.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (26,'Commissions',42.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (27,'Entry fees',34.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (28,'Customer Support',60.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (29,'Entry fees',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (30,'Trust&Fairness',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (31,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (32,'Commissions',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (33,'Customer Support',20.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (34,'Entry fees',10.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (35,'Trust&Fairness',10.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (36,'Games',10.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (37,'Commissions',10.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (38,'Customer Support',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (39,'Entry fees',42.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (40,'Trust&Fairness',30.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (41,'Games',42.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (42,'Commissions',12.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (43,'Customer Support',20.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (44,'Trust&Fairness',72.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (45,'Games',80.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (46,'Commissions',12.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (47,'Entry fees',30.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (48,'Customer Support',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (49,'Entry fees',20.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (50,'Trust&Fairness',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (51,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (52,'Commissions',12.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (53,'Customer Support',60.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (54,'Entry fees',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (55,'Trust&Fairness',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (56,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (57,'Commissions',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (58,'Customer Support',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (59,'Entry fees',42.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (60,'Trust&Fairness',30.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (61,'Games',42.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (62,'Commissions',12.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (63,'Customer Support',20.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (64,'Entry fees',30.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (65,'Trust&Fairness',72.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (66,'Games',80.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (67,'Commissions',12.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (68,'Customer Support',20.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (69,'Entry fees',50.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (70,'Trust&Fairness',72.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (71,'Games',80.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (72,'Commissions',50.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (73,'Customer Support',60.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (74,'Entry fees',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (75,'Trust&Fairness',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (76,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (77,'Commissions',50.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (78,'Customer Support',60.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (79,'Entry fees',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (80,'Trust&Fairness',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (81,'Games',100.0);
INSERT INTO "components_misc_ratings" ("id","title","percentage") VALUES (82,'Commissions',100.0);
INSERT INTO "components_links_mega_menu_links" ("id","title") VALUES (1,'Submenu');
INSERT INTO "components_links_mega_menu_links_components" ("id","field","order","component_type","component_id","components_links_mega_menu_link_id") VALUES (1,'links',1,'components_links_normal_links',1,1);
INSERT INTO "components_links_mega_menu_links_components" ("id","field","order","component_type","component_id","components_links_mega_menu_link_id") VALUES (2,'links',3,'components_links_normal_links',3,1);
INSERT INTO "components_links_mega_menu_links_components" ("id","field","order","component_type","component_id","components_links_mega_menu_link_id") VALUES (3,'links',2,'components_links_normal_links',2,1);
INSERT INTO "components_links_mega_menu_links_components" ("id","field","order","component_type","component_id","components_links_mega_menu_link_id") VALUES (4,'links',4,'components_links_normal_links',4,1);
INSERT INTO "nav_links_components" ("id","field","order","component_type","component_id","nav_link_id") VALUES (1,'submenu',1,'components_links_mega_menu_links',1,1);
INSERT INTO "nav_links_components" ("id","field","order","component_type","component_id","nav_link_id") VALUES (2,'badge',1,'components_misc_badges',1,1);
INSERT INTO "nav_links_components" ("id","field","order","component_type","component_id","nav_link_id") VALUES (3,'link',1,'components_links_normal_links',5,1);
INSERT INTO "components_misc_badges" ("id","text","type") VALUES (1,'New','hot');
INSERT INTO "nav_links" ("id","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,1633423561866,1,1,1633423429752,1633423561886);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (1,'experiences',1,'components_misc_pro_cons',45,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (2,'experiences',4,'components_misc_pro_cons',48,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (3,'experiences',3,'components_misc_pro_cons',47,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (4,'experiences',2,'components_misc_pro_cons',46,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (5,'features',1,'components_misc_custom_enumerations',30,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (6,'features',5,'components_misc_custom_enumerations',31,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (7,'features',4,'components_misc_custom_enumerations',33,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (8,'features',2,'components_misc_custom_enumerations',32,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (9,'features',3,'components_misc_custom_enumerations',34,1);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (10,'experiences',2,'components_misc_pro_cons',50,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (11,'experiences',1,'components_misc_pro_cons',49,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (12,'experiences',3,'components_misc_pro_cons',51,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (13,'experiences',4,'components_misc_pro_cons',52,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (14,'features',1,'components_misc_custom_enumerations',35,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (15,'features',5,'components_misc_custom_enumerations',39,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (16,'features',3,'components_misc_custom_enumerations',37,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (17,'features',2,'components_misc_custom_enumerations',36,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (18,'features',4,'components_misc_custom_enumerations',38,2);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (19,'experiences',1,'components_misc_pro_cons',53,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (20,'experiences',2,'components_misc_pro_cons',54,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (21,'experiences',4,'components_misc_pro_cons',56,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (22,'experiences',3,'components_misc_pro_cons',55,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (23,'features',1,'components_misc_custom_enumerations',40,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (24,'features',5,'components_misc_custom_enumerations',41,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (25,'features',2,'components_misc_custom_enumerations',42,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (26,'features',3,'components_misc_custom_enumerations',43,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (27,'features',4,'components_misc_custom_enumerations',44,3);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (28,'experiences',1,'components_misc_pro_cons',57,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (29,'experiences',4,'components_misc_pro_cons',60,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (30,'experiences',2,'components_misc_pro_cons',58,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (31,'experiences',3,'components_misc_pro_cons',59,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (32,'features',1,'components_misc_custom_enumerations',45,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (33,'features',5,'components_misc_custom_enumerations',46,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (34,'features',3,'components_misc_custom_enumerations',47,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (35,'features',2,'components_misc_custom_enumerations',48,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (36,'features',4,'components_misc_custom_enumerations',49,4);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (37,'experiences',1,'components_misc_pro_cons',61,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (38,'experiences',3,'components_misc_pro_cons',63,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (39,'experiences',2,'components_misc_pro_cons',62,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (40,'experiences',4,'components_misc_pro_cons',64,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (41,'features',1,'components_misc_custom_enumerations',50,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (42,'features',4,'components_misc_custom_enumerations',53,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (43,'features',3,'components_misc_custom_enumerations',54,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (44,'features',5,'components_misc_custom_enumerations',51,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (45,'features',2,'components_misc_custom_enumerations',52,5);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (46,'experiences',1,'components_misc_pro_cons',65,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (47,'experiences',4,'components_misc_pro_cons',68,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (48,'experiences',3,'components_misc_pro_cons',67,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (49,'experiences',2,'components_misc_pro_cons',66,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (50,'features',1,'components_misc_custom_enumerations',55,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (51,'features',5,'components_misc_custom_enumerations',56,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (52,'features',2,'components_misc_custom_enumerations',57,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (53,'features',4,'components_misc_custom_enumerations',59,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (54,'features',3,'components_misc_custom_enumerations',58,6);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (55,'experiences',1,'components_misc_pro_cons',69,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (56,'experiences',2,'components_misc_pro_cons',70,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (57,'experiences',3,'components_misc_pro_cons',71,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (58,'experiences',4,'components_misc_pro_cons',72,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (59,'features',1,'components_misc_custom_enumerations',60,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (60,'features',5,'components_misc_custom_enumerations',61,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (61,'features',2,'components_misc_custom_enumerations',62,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (62,'features',4,'components_misc_custom_enumerations',64,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (63,'features',3,'components_misc_custom_enumerations',63,7);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (64,'experiences',1,'components_misc_pro_cons',73,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (65,'experiences',2,'components_misc_pro_cons',74,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (66,'experiences',4,'components_misc_pro_cons',75,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (67,'experiences',3,'components_misc_pro_cons',76,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (68,'features',1,'components_misc_custom_enumerations',65,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (69,'features',5,'components_misc_custom_enumerations',66,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (70,'features',2,'components_misc_custom_enumerations',67,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (71,'features',3,'components_misc_custom_enumerations',68,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (72,'features',4,'components_misc_custom_enumerations',69,8);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (73,'experiences',3,'components_misc_pro_cons',79,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (74,'experiences',1,'components_misc_pro_cons',77,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (75,'experiences',4,'components_misc_pro_cons',80,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (76,'experiences',2,'components_misc_pro_cons',78,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (77,'features',1,'components_misc_custom_enumerations',70,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (78,'features',5,'components_misc_custom_enumerations',71,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (79,'features',2,'components_misc_custom_enumerations',72,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (80,'features',3,'components_misc_custom_enumerations',73,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (81,'features',4,'components_misc_custom_enumerations',74,9);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (82,'experiences',4,'components_misc_pro_cons',84,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (83,'experiences',1,'components_misc_pro_cons',81,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (84,'experiences',3,'components_misc_pro_cons',83,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (85,'experiences',2,'components_misc_pro_cons',82,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (86,'features',1,'components_misc_custom_enumerations',75,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (87,'features',5,'components_misc_custom_enumerations',76,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (88,'features',3,'components_misc_custom_enumerations',78,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (89,'features',2,'components_misc_custom_enumerations',77,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (90,'features',4,'components_misc_custom_enumerations',79,10);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (91,'experiences',3,'components_misc_pro_cons',87,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (92,'experiences',2,'components_misc_pro_cons',86,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (93,'experiences',1,'components_misc_pro_cons',85,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (94,'experiences',4,'components_misc_pro_cons',88,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (95,'features',1,'components_misc_custom_enumerations',80,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (96,'features',5,'components_misc_custom_enumerations',81,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (97,'features',2,'components_misc_custom_enumerations',82,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (98,'features',3,'components_misc_custom_enumerations',83,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (99,'features',4,'components_misc_custom_enumerations',84,11);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (100,'experiences',3,'components_misc_pro_cons',91,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (101,'experiences',4,'components_misc_pro_cons',92,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (102,'experiences',2,'components_misc_pro_cons',90,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (103,'experiences',1,'components_misc_pro_cons',89,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (104,'features',1,'components_misc_custom_enumerations',85,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (105,'features',5,'components_misc_custom_enumerations',86,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (106,'features',2,'components_misc_custom_enumerations',87,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (107,'features',3,'components_misc_custom_enumerations',88,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (108,'features',4,'components_misc_custom_enumerations',89,12);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (109,'experiences',2,'components_misc_pro_cons',94,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (110,'experiences',4,'components_misc_pro_cons',96,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (111,'experiences',1,'components_misc_pro_cons',93,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (112,'experiences',3,'components_misc_pro_cons',95,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (113,'features',1,'components_misc_custom_enumerations',90,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (114,'features',5,'components_misc_custom_enumerations',91,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (115,'features',2,'components_misc_custom_enumerations',92,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (116,'features',3,'components_misc_custom_enumerations',93,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (117,'features',4,'components_misc_custom_enumerations',94,13);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (118,'experiences',4,'components_misc_pro_cons',100,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (119,'experiences',1,'components_misc_pro_cons',97,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (120,'experiences',3,'components_misc_pro_cons',99,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (121,'experiences',2,'components_misc_pro_cons',98,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (122,'features',5,'components_misc_custom_enumerations',96,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (123,'features',1,'components_misc_custom_enumerations',95,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (124,'features',3,'components_misc_custom_enumerations',98,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (125,'features',2,'components_misc_custom_enumerations',97,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (126,'features',4,'components_misc_custom_enumerations',99,14);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (127,'experiences',1,'components_misc_pro_cons',101,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (128,'experiences',4,'components_misc_pro_cons',104,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (129,'experiences',2,'components_misc_pro_cons',102,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (130,'experiences',3,'components_misc_pro_cons',103,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (131,'features',1,'components_misc_custom_enumerations',100,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (132,'features',3,'components_misc_custom_enumerations',103,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (133,'features',2,'components_misc_custom_enumerations',102,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (134,'features',4,'components_misc_custom_enumerations',104,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (135,'features',5,'components_misc_custom_enumerations',101,15);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (136,'experiences',1,'components_misc_pro_cons',105,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (137,'experiences',4,'components_misc_pro_cons',108,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (138,'experiences',2,'components_misc_pro_cons',106,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (139,'experiences',3,'components_misc_pro_cons',107,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (140,'features',1,'components_misc_custom_enumerations',105,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (141,'features',2,'components_misc_custom_enumerations',107,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (142,'features',5,'components_misc_custom_enumerations',106,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (143,'features',3,'components_misc_custom_enumerations',108,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (144,'features',4,'components_misc_custom_enumerations',109,16);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (145,'experiences',2,'components_misc_pro_cons',110,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (146,'experiences',4,'components_misc_pro_cons',112,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (147,'experiences',3,'components_misc_pro_cons',111,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (148,'experiences',1,'components_misc_pro_cons',109,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (149,'features',1,'components_misc_custom_enumerations',110,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (150,'features',5,'components_misc_custom_enumerations',111,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (151,'features',2,'components_misc_custom_enumerations',112,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (152,'features',4,'components_misc_custom_enumerations',114,17);
INSERT INTO "slots_components" ("id","field","order","component_type","component_id","slot_id") VALUES (153,'features',3,'components_misc_custom_enumerations',113,17);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (2,'Blandit','Blandit',2,1,1,1633536437280,1633536437287);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (3,'Cras','Cras',2,1,1,1633536447975,1633536447986);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (4,'Diam','Diam',2,1,1,1633536455157,1633536455165);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (5,'Egestas','Egestas',2,1,1,1633536465150,1633536465159);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (6,'Elementum','Elementum',2,1,1,1633536474356,1633536474364);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (7,'Eusimod','Eusimod',2,1,1,1633536486000,1633536486009);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (8,'Mauris','Mauris',2,1,1,1633536494991,1633536494999);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (9,'Neque','Neque',2,1,1,1633536508095,1633536508105);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (10,'Rhoncus','Rhoncus',2,1,1,1633536524587,1633536524602);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (11,'Viverra','Viverra',2,1,1,1633536534682,1633536534691);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (12,'Massa','Massa',3,1,1,1633536554204,1633536554212);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (13,'Senectus','Senectus',3,1,1,1633536568471,1633536568481);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (14,'Vitae','Vitae',3,1,1,1633536582843,1633536582851);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (15,'English','English',4,1,1,1633536597833,1633536597845);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (16,'Polish','Polish',4,1,1,1633536719737,1633536719746);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (17,'Russian','Russian',4,1,1,1633536730612,1633536730622);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (18,'AUD','AUD',5,1,1,1633536749103,1633536749112);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (19,'CAD','CAD',5,1,1,1633536765583,1633536765593);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (20,'EUR','EUR',5,1,1,1633536779646,1633536779656);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (21,'GBP','GBP',5,1,1,1633536794297,1633536794307);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (22,'JPY','JPY',5,1,1,1633536802960,1633536802969);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (23,'USD','USD',5,1,1,1633536812244,1633536812253);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (24,'Cash','Cash',6,1,1,1633536836646,1633536836655);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (25,'Credit Cards','Credit-Cards',6,1,1,1633536847444,1633536847454);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (26,'Direct Deposit','Direct-Deposit',6,1,1,1633536863289,1633536863298);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (27,'Brazil','Brazil',7,1,1,1633536875823,1633536875833);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (28,'Nigar','Nigar',7,1,1,1633536885080,1633536885089);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (29,'Peru','Peru',7,1,1,1633536892646,1633536892656);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (30,'Peru','Peru',7,1,1,1633536904292,1633536904300);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (31,'Romania','Romania',7,1,1,1633536912871,1633536912880);
INSERT INTO "tags" ("id","title","slug","tag_type","created_by","updated_by","created_at","updated_at") VALUES (32,'Russia','Russia',7,1,1,1633536920721,1633536920730);
INSERT INTO "tests" ("id","test","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,NULL,1633429592678,1,1,1633429559113,1633429592686);
INSERT INTO "home_headers_components" ("id","field","order","component_type","component_id","home_header_id") VALUES (1,'button',1,'components_links_normal_links',13,1);
INSERT INTO "home_headers" ("id","title","description","email","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,'Joining Casinos','Duis aute irure dolor in reprehenderit in  volup velit esse cillum dolore eu fugiat nulla.','contact@coinflip.com',1633533230283,1,1,1633531347024,1633533230299);
INSERT INTO "components_misc_characteristics" ("id","title","description") VALUES (1,'Advice and guide','After all, as described in Web Design Trends 2015 & 2016, vision dominates a lot of our subconscious interpretation of the world around us that pleasing images create.

');
INSERT INTO "components_misc_characteristics" ("id","title","description") VALUES (2,'Great Solutions','Rounding up a bunch of specific designs & talking about the merits of each is the perfect way to find common ground. Collecting a wide samples is a great way to start.

');
INSERT INTO "components_misc_characteristics" ("id","title","description") VALUES (3,'Support in Person','At its core, every brand has something special to reveal something that inspires people. We are a branding agency, this plan if you create a small portfolio or business showcase');
INSERT INTO "home_characteristics_components" ("id","field","order","component_type","component_id","home_characteristic_id") VALUES (1,'characteristics',1,'components_misc_characteristics',1,1);
INSERT INTO "home_characteristics_components" ("id","field","order","component_type","component_id","home_characteristic_id") VALUES (2,'characteristics',2,'components_misc_characteristics',2,1);
INSERT INTO "home_characteristics_components" ("id","field","order","component_type","component_id","home_characteristic_id") VALUES (3,'characteristics',3,'components_misc_characteristics',3,1);
INSERT INTO "home_characteristics_components" ("id","field","order","component_type","component_id","home_characteristic_id") VALUES (4,'sectionTitle',1,'components_misc_section_titles',1,1);
INSERT INTO "components_misc_section_titles" ("id","title","subtitle","description") VALUES (1,'Welcome To Coinflip','Just flip a Coin','At Modeltheme, we show only the best websites and portfolios built completely
with passion, simplicity & creativity !');
INSERT INTO "components_misc_faqs" ("id","question","answer") VALUES (1,'Which Plan is good for me ?','Trends, vision dominates a lot of our subconscious interpretation of the world around us. On top it, pleasing images create better user experience.');
INSERT INTO "components_misc_faqs" ("id","question","answer") VALUES (2,'Do I have to commit to a program','Trends, vision dominates a lot of our subconscious interpretation of the world around us. On top it, pleasing images create better user experience.');
INSERT INTO "components_misc_faqs" ("id","question","answer") VALUES (3,'What Payment Methods are Available?','Trends, vision dominates a lot of our subconscious interpretation of the world around us. On top it, pleasing images create better user experience.');
INSERT INTO "home_faqs_components" ("id","field","order","component_type","component_id","home_faq_id") VALUES (1,'faqList',1,'components_misc_faqs',1,1);
INSERT INTO "home_faqs_components" ("id","field","order","component_type","component_id","home_faq_id") VALUES (2,'faqList',2,'components_misc_faqs',2,1);
INSERT INTO "home_faqs_components" ("id","field","order","component_type","component_id","home_faq_id") VALUES (3,'faqList',3,'components_misc_faqs',3,1);
INSERT INTO "components_misc_testimonials" ("id","author","position","message") VALUES (1,'Thomas Moriz','CEO MODELTHEME','“My project was a simple & small task, but the persistence and determination of the team turned it into an awesome and great project which make me very happy”');
INSERT INTO "components_misc_testimonials" ("id","author","position","message") VALUES (2,'Roberta Cozza','General Director','Sed perspiciatis unde omnis natus error sit voluptatem totam rem aperiam, eaque quae architecto beatae explicabo.');
INSERT INTO "components_misc_testimonials" ("id","author","position","message") VALUES (3,'Emma Roberts','Assistant Manager','Sed perspiciatis unde omnis natus error sit voluptatem totam rem aperiam, eaque quae architecto beatae explicabo.');
INSERT INTO "home_testimonials_components" ("id","field","order","component_type","component_id","home_testimonial_id") VALUES (1,'testimonials',1,'components_misc_testimonials',1,1);
INSERT INTO "home_testimonials_components" ("id","field","order","component_type","component_id","home_testimonial_id") VALUES (2,'testimonials',3,'components_misc_testimonials',3,1);
INSERT INTO "home_testimonials_components" ("id","field","order","component_type","component_id","home_testimonial_id") VALUES (3,'testimonials',2,'components_misc_testimonials',2,1);
INSERT INTO "home_faqs" ("id","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,NULL,1,1,1633529997630,1633529997654);
INSERT INTO "home_testimonials" ("id","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,1633533238004,1,1,1633531148797,1633533238023);
INSERT INTO "home_characteristics" ("id","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,NULL,1,1,1633530081893,1633530081917);
INSERT INTO "components_misc_get_in_touches_components" ("id","field","order","component_type","component_id","components_misc_get_in_touch_id") VALUES (1,'link',1,'components_links_normal_links',10,1);
INSERT INTO "components_misc_get_in_touches_components" ("id","field","order","component_type","component_id","components_misc_get_in_touch_id") VALUES (2,'link',1,'components_links_normal_links',11,3);
INSERT INTO "components_misc_get_in_touches_components" ("id","field","order","component_type","component_id","components_misc_get_in_touch_id") VALUES (3,'link',1,'components_links_normal_links',12,2);
INSERT INTO "components_misc_get_in_touches_components" ("id","field","order","component_type","component_id","components_misc_get_in_touch_id") VALUES (4,'icon',1,'components_misc_icons',5,1);
INSERT INTO "components_misc_get_in_touches_components" ("id","field","order","component_type","component_id","components_misc_get_in_touch_id") VALUES (5,'icon',1,'components_misc_icons',7,2);
INSERT INTO "components_misc_get_in_touches_components" ("id","field","order","component_type","component_id","components_misc_get_in_touch_id") VALUES (6,'icon',1,'components_misc_icons',6,3);
INSERT INTO "home_get_in_touches_components" ("id","field","order","component_type","component_id","home_get_in_touch_id") VALUES (1,'content',1,'components_misc_get_in_touches',1,1);
INSERT INTO "home_get_in_touches_components" ("id","field","order","component_type","component_id","home_get_in_touch_id") VALUES (2,'content',2,'components_misc_get_in_touches',3,1);
INSERT INTO "home_get_in_touches_components" ("id","field","order","component_type","component_id","home_get_in_touch_id") VALUES (3,'content',3,'components_misc_get_in_touches',2,1);
INSERT INTO "home_get_in_touches" ("id","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,1633530658317,1,1,1633530656367,1633530658340);
INSERT INTO "home_partners" ("id","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,1633533234824,1,1,1633530892515,1633533234835);
INSERT INTO "footers" ("id","aboutUs","email","phoneNo","subscribeText","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,'All in One Casino Affiliate WordPress theme with slots, bonuses and more.','office@example.com',' (+40) 74 0920 2288','Sed perspiciatis unde omnis natus error quae natus error quae.',1633530164662,1,1,1633530161943,1633530233238);
INSERT INTO "footers_components" ("id","field","order","component_type","component_id","footer_id") VALUES (1,'socials',1,'components_links_social_links',1,1);
INSERT INTO "footers_components" ("id","field","order","component_type","component_id","footer_id") VALUES (2,'socials',2,'components_links_social_links',2,1);
INSERT INTO "footers_components" ("id","field","order","component_type","component_id","footer_id") VALUES (3,'socials',4,'components_links_social_links',4,1);
INSERT INTO "footers_components" ("id","field","order","component_type","component_id","footer_id") VALUES (4,'socials',3,'components_links_social_links',3,1);
INSERT INTO "components_misc_get_in_touches" ("id","title","description") VALUES (1,'Our Address','New York 11673 Collins Street West United State.');
INSERT INTO "components_misc_get_in_touches" ("id","title","description") VALUES (2,'Our Support','Support Email : support@coinflip.com Phone: 0756 192 121');
INSERT INTO "components_misc_get_in_touches" ("id","title","description") VALUES (3,'Contact Info','Main Email : contact@coinflip.com Phone: 0756 192 121');
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,'JackPot City','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','affiliate',54,'https://modeltheme.com/go/coinflip/',1,'JackPot-City',2,1633343386894,1,1,1633096543380,1633542756671);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (3,'The Vegas','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','affiliate',54,'https://modeltheme.com/go/coinflip/',1,'The-Vegas',NULL,1633537146200,1,1,1633537139746,1633537183447);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (4,'Golden Casino','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','affiliate',12,'https://modeltheme.com/go/coinflip/',1,'Golden-Casino',2,1633537240007,1,1,1633537217585,1633542756671);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (5,'Get Lucky','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','affiliate',9,'https://modeltheme.com/go/coinflip/',1,'Get-Lucky',8,1633537319094,1,1,1633537288991,1633542483497);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (6,'Vera Vegas','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',12,'https://modeltheme.com/go/coinflip/',1,'Vera-Vegas',8,1633537405409,1,1,1633537385548,1633542483497);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (7,'Ruby Fortune','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',5,'https://modeltheme.com/go/coinflip/',1,'Ruby-Fortune',11,1633545566286,1,1,1633545561419,1633545566347);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (8,'Royal Vegas','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',11,'https://modeltheme.com/go/coinflip/',1,'Royal-Vegas',11,1633545612033,1,1,1633545593610,1633545612098);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (9,'Spin Casino','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',0,'https://modeltheme.com/go/coinflip/',1,'Spin-Casino',2,1633545626556,1,1,1633545624901,1633545626617);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (10,'Spin Casino','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',54,'https://modeltheme.com/go/coinflip/',1,'Spin-Casino',3,1633545784886,1,1,1633545783192,1633545784942);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (11,'Casino X','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',0,'https://modeltheme.com/go/coinflip/',1,'Casino-X',11,1633545871389,1,1,1633545867866,1633545871449);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (12,'Planet 9','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',9,'https://modeltheme.com/go/coinflip/',1,'Planet-9',10,1633545901412,1,1,1633545891497,1633545901474);
INSERT INTO "casinos" ("id","title","description","about","type","likes","referralLink","active","slug","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (13,'Gaming Club','Coinflip Casino is an Online Casino. It features 5 games and you can get a $5 reward just for registering right.','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.

 

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

> Love the selection of games, friendly customer service and the bright interface of the casino.

Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','certified',0,'https://modeltheme.com/go/coinflip/',1,'Gaming-Club',9,1633545928157,1,1,1633545918856,1633545928218);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (1,'rating',1,'components_misc_ratings',28,1);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (2,'rating',5,'components_misc_ratings',29,1);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (3,'rating',2,'components_misc_ratings',30,1);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (4,'rating',3,'components_misc_ratings',31,1);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (5,'rating',4,'components_misc_ratings',32,1);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (6,'rating',1,'components_misc_ratings',33,2);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (7,'rating',5,'components_misc_ratings',34,2);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (8,'rating',3,'components_misc_ratings',36,2);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (9,'rating',2,'components_misc_ratings',35,2);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (10,'rating',4,'components_misc_ratings',37,2);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (11,'rating',1,'components_misc_ratings',38,3);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (12,'rating',5,'components_misc_ratings',39,3);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (13,'rating',2,'components_misc_ratings',40,3);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (14,'rating',3,'components_misc_ratings',41,3);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (15,'rating',4,'components_misc_ratings',42,3);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (16,'rating',1,'components_misc_ratings',43,4);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (17,'rating',4,'components_misc_ratings',46,4);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (18,'rating',5,'components_misc_ratings',47,4);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (19,'rating',3,'components_misc_ratings',45,4);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (20,'rating',2,'components_misc_ratings',44,4);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (21,'rating',1,'components_misc_ratings',48,5);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (22,'rating',4,'components_misc_ratings',52,5);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (23,'rating',3,'components_misc_ratings',51,5);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (24,'rating',2,'components_misc_ratings',50,5);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (25,'rating',5,'components_misc_ratings',49,5);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (26,'rating',1,'components_misc_ratings',53,6);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (27,'rating',4,'components_misc_ratings',57,6);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (28,'rating',5,'components_misc_ratings',54,6);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (29,'rating',2,'components_misc_ratings',55,6);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (30,'rating',3,'components_misc_ratings',56,6);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (31,'rating',1,'components_misc_ratings',58,7);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (32,'rating',5,'components_misc_ratings',59,7);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (33,'rating',3,'components_misc_ratings',61,7);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (34,'rating',2,'components_misc_ratings',60,7);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (35,'rating',4,'components_misc_ratings',62,7);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (36,'rating',1,'components_misc_ratings',63,8);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (37,'rating',5,'components_misc_ratings',64,8);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (38,'rating',3,'components_misc_ratings',66,8);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (39,'rating',2,'components_misc_ratings',65,8);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (40,'rating',4,'components_misc_ratings',67,8);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (41,'rating',1,'components_misc_ratings',68,9);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (42,'rating',5,'components_misc_ratings',69,9);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (43,'rating',3,'components_misc_ratings',71,9);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (44,'rating',2,'components_misc_ratings',70,9);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (45,'rating',4,'components_misc_ratings',72,9);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (46,'rating',1,'components_misc_ratings',73,10);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (47,'rating',4,'components_misc_ratings',77,10);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (48,'rating',2,'components_misc_ratings',75,10);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (49,'rating',5,'components_misc_ratings',74,10);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (50,'rating',3,'components_misc_ratings',76,10);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (51,'rating',1,'components_misc_ratings',78,11);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (52,'rating',5,'components_misc_ratings',79,11);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (53,'rating',2,'components_misc_ratings',80,11);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (54,'rating',3,'components_misc_ratings',81,11);
INSERT INTO "reviews_components" ("id","field","order","component_type","component_id","review_id") VALUES (55,'rating',4,'components_misc_ratings',82,11);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Consectetur Adipis',NULL,1633537602787,1,1,1633537593741,1633539971102);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Lorem ipsum dolor',NULL,1633537698840,1,1,1633537629054,1633542756697);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Bookmaker Review',NULL,1633537735371,1,1,1633537731340,1633539989486);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Bookmaker Review',NULL,1633542415397,1,1,1633540043505,1633542415445);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','La pergola',NULL,1633542443468,1,1,1633542441254,1633542443482);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Casino X Review',NULL,1633542454024,1,1,1633542452921,1633542790196);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Royal Prism',NULL,1633542470323,1,1,1633542462393,1633542470337);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Slot Review','',1633542561881,1,1,1633542483465,1633542561941);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Planet Nine',NULL,1633542702336,1,1,1633542700772,1633542702351);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Review Coinflip',NULL,1633542737716,1,1,1633542736543,1633542737729);
INSERT INTO "reviews" ("id","content","title","slug","published_at","created_by","updated_by","created_at","updated_at") VALUES (11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nisl nibh, porta sed tristique vel, mattis eu lectus. Praesent accumsan ullamcorper sapien nec varius. In id elementum mauris. Praesent eu faucibus mi. Mauris eget varius arcu, feugiat gravida leo. Maecenas mattis viverra nulla at interdum. Phasellus porttitor, mi non ornare commodo, erat tellus cursus ipsum, ac congue orci tortor in leo.

Important: A review can be written for one post type or for multiple post types. By post type we understand one of these: (Casino, Slot, Bonus, or Bookmaker). So, a review can be assigned to 1 post from each of the mentioned four post types.
- Vivamus a nisi vitae leo varius bibendum.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Vestibulum faucibus efficitur massa, et sodales lorem pretium id.
- Vivamus nec leo ac nisi scelerisque finibus.
- Nullam bibendum cursus sodales. Sed ac molestie elit.
- Vivamus in urna sapien. In hac habitasse platea dictumst.

Duis et enim tellus. Suspendisse fringilla interdum metus at vulputate. Aenean finibus finibus ultricies. Aenean convallis semper gravida. Nunc in nulla eu nulla feugiat interdum vitae et leo. Morbi luctus ex eu aliquam rhoncus. Sed hendrerit enim dapibus libero condimentum, ac tempor libero molestie.

Ut arcu orci, malesuada hendrerit mi quis, sagittis eleifend quam. Proin ac tristique libero, et ultricies augue. Sed tempus tristique dolor sed pretium. Donec fringilla, nisl non mollis molestie, nisl dui consequat nulla, in vestibulum nibh erat vel nisl. Curabitur magna orci, bibendum et congue molestie, interdum nec ipsum. Praesent in gravida odio. Etiam euismod nulla eu nisi varius varius. Pellentesque eleifend et libero nec suscipit. Aenean quis urna luctus, sodales dui eget, eleifend mauris. Nulla non congue sapien, sed tincidunt urna.

> Sed blandit, lectus quis gravida tristique, elit mauris blandit felis, vel lacinia est purus vitae nunc. Pellentesque at arcu at enim auctor tincidunt aliquet sit amet tortor. Mauris non ex vitae ante elementum convallis id ac nulla.

Integer efficitur nisl magna, nec suscipit nulla maximus eget. Sed lacinia ante nec magna dictum consectetur. Ut fermentum leo nec nulla sollicitudin, vitae accumsan libero elementum. Integer consequat quam vel libero pulvinar convallis. Fusce non purus imperdiet, scelerisque arcu nec, fringilla ante. Morbi vulputate fermentum massa et tincidunt. Pellentesque luctus posuere fringilla. Fusce lacus ante, interdum vitae odio at, sollicitudin sodales dolor. Suspendisse dictum sed sem eget dictum. Phasellus convallis justo risus, nec hendrerit ante consectetur at.','Ruby Review',NULL,1633542772132,1,1,1633542770896,1633542772145);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (1,'Go For Broke',1,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633614541217,1633618851056);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (2,'Generations',4,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633615583692,1633615583739);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (3,'Masterspace',1,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633615643150,1633615661605);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (4,'Dreamreign',6,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633615700436,1633615746610);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (5,'Cloudmind',13,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633615787434,1633615787487);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (6,'Ace And Kings',11,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633615837838,1633615837888);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (7,'Clover Grove',9,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633615885456,1633615885504);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (8,'Fruit Ninja',8,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','classic','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633615921913,1633615928312);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (9,'Trone Kingdom',12,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','esport','Chilling PvP Strategy Mayhem',NULL,1633614544880,1,1,1633618166859,1633618166909);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (10,'Shadow Legend',3,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','esport','RTS Adventure on High Seas Game',NULL,1633614544880,1,1,1633618267199,1633618267254);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (11,'War of Empires',13,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','esport','Vikings Jarl in MMORTS game',NULL,1633614544880,1,1,1633618363380,1633618363432);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (12,'Spartan League',9,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','esport','Pro Action games in United Adventure.',NULL,1633614544880,1,1,1633618434503,1633618434552);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (13,'Tide of Fortune',9,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','esport','Jackpot Night is an Online Casino.',NULL,1633614544880,1,1,1633618468769,1633618468813);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (14,'Rope In',1,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','fruit_games','Jackpot Night is an Online Casino.',2,1633618829937,1,1,1633618819501,1633618830011);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (15,'Ace And Kings',9,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','fruit_games','Jackpot Night is an Online Casino.',9,1633618965490,1,1,1633618963948,1633618965560);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (16,'Clover Grove',9,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','fruit_games','Jackpot Night is an Online Casino.',5,1633618998191,1,1,1633618993412,1633618998256);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (17,'Fruit Ninja',9,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','fruit_games','Jackpot Night is an Online Casino.',7,1633619018239,1,1,1633619016905,1633619018310);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (18,'Ace And Kings',4,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','progressive','Jackpot Night is an Online Casino.',3,1633619018239,1,1,1633619016905,1633620696188);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (19,'Clover Grove',12,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','progressive','Jackpot Night is an Online Casino.',5,1633619018239,1,1,1633619016905,1633619016905);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (20,'Fruit Ninja',13,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','progressive','Jackpot Night is an Online Casino.',9,1633619018239,1,1,1633619016905,1633620655516);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (21,'Bankroll',11,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','progressive','Jackpot Night is an Online Casino.',1,1633619018239,1,1,1633619016905,1633620602932);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (22,'Bankroll',3,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','video','Jackpot Night is an Online Casino.',5,1633619018239,1,1,1633619016905,1633620591301);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (23,'Ace And Kings',5,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','video','Jackpot Night is an Online Casino.',7,1633619018239,1,1,1633619016905,1633620480716);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (24,'Clover Grove',7,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','video','Jackpot Night is an Online Casino.',3,1633619018239,1,1,1633619016905,1633620095449);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (25,'Fruit Ninja',10,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','video','Jackpot Night is an Online Casino.',2,1633619018239,1,1,1633619016905,1633620046536);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (26,'Soccer Games',5,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',1,1633619018239,1,1,1633619016905,1633620029130);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (27,'Minecraft',5,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',6,1633619018239,1,1,1633619016905,1633619991867);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (28,'Narcos',8,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',4,1633619018239,1,1,1633619016905,1633619979201);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (29,'Viking Game',7,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',9,1633619018239,1,1,1633619016905,1633619973560);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (30,'Deadwood',8,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',4,1633619018239,1,1,1633619016905,1633619964832);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (31,'Gonzo’s Quest',7,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',6,1633619018239,1,1,1633619016905,1633619940985);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (32,'Tarzan Jungle',10,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',7,1633619018239,1,1,1633619016905,1633619934911);
INSERT INTO "slots" ("id","title","casino","description","about","type","shortDescription","review","published_at","created_by","updated_by","created_at","updated_at") VALUES (33,'Book of Deluxe',10,'**100% match bonus** based on first deposit of £/$/€20+. **Additional bonuses**.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .

','First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.


> Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.

Love the selection of games, friendly customer service and the bright interface of the casino.
Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.','games','Jackpot Night is an Online Casino.',8,1633619018239,1,1,1633619016905,1633619922675);
CREATE UNIQUE INDEX IF NOT EXISTS "strapi_role_name_unique" ON "strapi_role" (
	"name"
);
CREATE UNIQUE INDEX IF NOT EXISTS "strapi_role_code_unique" ON "strapi_role" (
	"code"
);
CREATE UNIQUE INDEX IF NOT EXISTS "strapi_administrator_email_unique" ON "strapi_administrator" (
	"email"
);
CREATE UNIQUE INDEX IF NOT EXISTS "i18n_locales_code_unique" ON "i18n_locales" (
	"code"
);
CREATE UNIQUE INDEX IF NOT EXISTS "users-permissions_role_type_unique" ON "users-permissions_role" (
	"type"
);
CREATE UNIQUE INDEX IF NOT EXISTS "users-permissions_user_username_unique" ON "users-permissions_user" (
	"username"
);
COMMIT;
