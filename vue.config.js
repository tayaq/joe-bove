module.exports = {
    pages: {
      index: {
        entry: 'src/main.js'
      }
    },
    publicPath: process.env.VUE_APP_PATH
  }
