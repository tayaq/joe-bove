/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

// vuex.d.ts
import { ComponentCustomProperties } from 'vue'

declare module '@vue/runtime-core' {
  // declare your own store states
  // provide typings for `this.$store`
  interface ComponentCustomProperties {
    $getFileSource: (object: string) => string;
    $formatDate: (object: string|Date) => string;
    $formatPrice: (price: number, currency?: string) => string;
    $formatTextToMarkdown: (text: string) => string;
  }
}