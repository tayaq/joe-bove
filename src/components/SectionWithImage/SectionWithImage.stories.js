import SectionWithImage from "./SectionWithImage.vue";
import Jumbotron from "../Jumbotron/Jumbotron.vue";

export default {
  component: SectionWithImage,
  subcomponents: {Jumbotron},
  title: "Components/SectionWithImage",
  parameters: {
    backgrounds: { default: "light" },
    layout: "fullscreen"
  },
};

export const Default = (args) => ({
  components: { SectionWithImage, Jumbotron },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<section-with-image v-bind="args" ><Jumbotron v-bind="args.hero" /></section-with-image>',
});

Default.args = {
 thumbnail: {url:'../images/misc/featured-game1.jpg'},
  rtl: false,
  hero: {
    title: "Looking to get into the exciting world of online gambling?",
    subtitle: "Just flip a Coin",
    description: "Dress with crossover V-neckline with ruffled trims. Features long sleeves with ruffled trims, elastic cuffs, a cinched waist and a ruffled hem lorem ipsum dolor sid amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.  ",
    buttonName : "Read more"
  }
};