import PopularCategoriesCards from "./PopularCategoriesCards.vue";

export default {
  component: PopularCategoriesCards,
  title: "Components/PopularCategoriesCards",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { PopularCategoriesCards },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<PopularCategoriesCards style="width:250px;" v-bind="args" />',
});

Default.args = {
  title: "Casino Affiliate",
  thumbnail: {url:  "images/demosimages/casino-popular-category-1.png"}
};
