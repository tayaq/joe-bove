import CasinoDetails from "./CasinoDetails.vue";

export default {
  component: CasinoDetails,
  title: "Components/CasinoDetails",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CasinoDetails },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CasinoDetails style="max-width:800px;" v-bind="args" />',
});

Default.args = {
  thumbnail: { url :"images/casino-logos/cat_casino4.jpg"},
  title: "Casino X",
  accepts: false,
  description: "<strong>Coinflip Casino</strong> is an Online Casino. It features 5 games and you can get a <strong>$5 reward</strong> just for registering right now.",
  likes: 5,
  rating: 2,
  socials: [
    {icon: {name: 'facebook', type: 'fab'}, name:'facebook', href: '/'},
    {icon: {name: 'twitter', type: 'fab'}, name:'twitter', href: '/'}
  ]
}