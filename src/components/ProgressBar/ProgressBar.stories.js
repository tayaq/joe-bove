import ProgressBar from "./ProgressBar.vue";

export default {
  component: ProgressBar,
  title: "Components/ProgressBar",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { ProgressBar },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<ProgressBar style="width:500px;" v-bind="args" />',
});

Default.args = {
  title: "Customer Support",
  percentage: 50
};
