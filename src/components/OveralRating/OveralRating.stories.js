import OveralRating from "./OveralRating.vue";

export default {
  component: OveralRating,
  title: "Components/OveralRating",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { OveralRating },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<div style="width:500px;" ><OveralRating v-bind="args" /></div>',
});

Default.args = {
  ratings: [
    {
      title: "Customer Support",
      percentage: 10,
    },
    {
      title: "Trust&Fairness",
      percentage: 10,
    },
    {
      title: "Games20",
      percentage: 10,
    },
    {
      title: "Commissions",
      percentage: 10,
    },
    {
      title: "Entry fees",
      percentage: 10,
    },
  ],
};
