import CasinoAbout from "./CasinoAbout.vue";

export default {
  component: CasinoAbout,
  title: "Components/CasinoAbout",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CasinoAbout },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CasinoAbout style="max-width:800px;" v-bind="args" />',
});

Default.args = {
  description: "<p></p><p>First time to do a review for this casino since I am just a new member, been playing here constantly for three days now. All seems to be running smooth especially the slots.</p><p>&nbsp;</p><p>Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.</p><blockquote><p>Love the selection of games, friendly customer service and the bright interface of the casino.</p></blockquote><p>Coinflip Casino is pretty decent, but do expect some delays for withdrawal requests if connected to your MasterCard. Not really a bother to me as long as the payout is just a small amount. In terms of layout I am not much a fan, it is cute but does not really entice male players like me, in my opinion. Design does not get in the way of my game, so it’s all good.</p><p></p>",
}