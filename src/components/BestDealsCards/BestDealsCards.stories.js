import BestDealsCards from "./BestDealsCards.vue";

export default {
  component: BestDealsCards,
  title: "Components/BestDealsCards",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { BestDealsCards },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<BestDealsCards style="width:600px;" v-bind="args" />',
});

Default.args = {
  title: "Claim Special Reward From Wagering.",
  thumbnail: {url:  "images/demosimages/best-deals.png"},
  href: '/'
};
