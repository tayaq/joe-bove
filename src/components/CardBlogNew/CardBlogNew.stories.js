import CardBlogNew from "./CardBlogNew.vue";

export default {
  component: CardBlogNew,
  title: "Components/CardBlogNew",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardBlogNew },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardBlogNew style="width:400px;height:auto;" v-bind="args" />',
});

Default.args = {
  title: "Four ways to cheer yourself up on the Next Blue Monday!",
  description:"Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum complectitur et, eam ut veri",
  thumbnail: {url: "images/demosimages/card-blog-1.jpeg"},
  date:"7 jul 2021",
  link:"https://coinflip.modeltheme.com/four-ways-to-cheer-yourself-up-on-the-next-blue-monday/",
  author:"Trader Marcus"
};
