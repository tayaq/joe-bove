import CardBlogPost from "./CardBlogPost.vue";

export default {
  component: CardBlogPost,
  title: "Components/CardBlogPost",
  parameters: {
    backgrounds: { default: "light" },
  },
  argTypes: {
    type: {
      options: ['normal','mini','compact'],
      control: { type: 'select' }
    }
  }
}

export const Default = (args) => ({
  components: { CardBlogPost },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardBlogPost style="width:700px;" v-bind="args" />',
});

Default.args = {
  title: "How to Properly Use Casino Fibonacci System for now",
  subtitle: "Just flip a Coin",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
  thumbnail : { url :"images/blogs/article-blog-3.jpg" },
  author: {
    name: "Trader Marcus",
    href: "/"
  },
  details: {
    date: new Date(),
    commentNo: 1,
  },
  type: 'normal'
};

export const Mini = (args) => ({
  components: { CardBlogPost },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardBlogPost style="width:500px;" v-bind="args" />',
});

Mini.args = {
  title: "How to Properly Use Casino Fibonacci System for now",
  type: 'mini',
  subtitle: "Just flip a Coin",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
  thumbnail : { url :"images/blogs/article-blog-3.jpg" },
  author: {
    name: "Trader Marcus",
    href: "/"
  },
  details: {
    date: new Date(),
    commentNo: 1,
    category: "BETTING-NEWS"
  }
};

export const Compact = (args) => ({
  components: { CardBlogPost },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardBlogPost style="width:500px;" v-bind="args" />',
});

Compact.args = {
  title: "How to Properly Use Casino Fibonacci System for now",
  type: 'compact',
  subtitle: "Just flip a Coin",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
  thumbnail : { url :"images/blogs/article-blog-3.jpg" },
  author: {
    name: "Trader Marcus",
    href: "/"
  },
  details: {
    date: new Date(),
    commentNo: 1,
    category: "BETTING-NEWS"
  }
};
