import Badge from "./Badge.vue";

export default {
  component: Badge,
  title: "Components/Badge",
  parameters: {
    backgrounds: { default: "light" },
  },
  argTypes: {
    position: {
      options: ['none', 'top-right', 'top-left', 'bottom-right', 'bottom-left'],
      control: { type: 'select' }
    },
    type: {
      options: ['none','new', 'newCount', 'hot'],
      control: { type: 'select' }
    }
  }
};

export const Default = (args) => ({
  components: { Badge },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Badge v-bind="args" />',
});

Default.args = {
  text: 'New',
  bgColor: '#22a6b3',
  textColor: 'white',
  position: 'none',
  type: 'new'
}