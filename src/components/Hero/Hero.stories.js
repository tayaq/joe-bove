import Hero from "./Hero.vue";

export default {
  component: Hero,
  title: "Components/Hero",
  parameters: {
    backgrounds: { default: "light" },
    layout: "fullscreen",
  },
};

export const Default = (args) => ({
  components: { Hero },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Hero v-bind="args" />',
});

Default.args = {
  title: "Categories: Certified Casinos",
  breadcrumbs: [
    { href: "/", name: "Home" },
    { href: "/", name: "Certified Casinos" },
  ],
  backgroundImage: "images/background/bg_certified_casions.png",
};

export const Mini = (args) => ({
  components: { Hero },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Hero v-bind="args" />',
});

Mini.args = {
  breadcrumbs: [
    { href: "/", name: "Home" },
    { href: "/", name: "Certified Casinos" },
  ],
};

export const WithActions = (args) => ({
  components: { Hero },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: `
  <hero v-bind="args" > 
    <template #title>
      <h1 class="list_title" style="    font-weight: 700;
      margin-top: 10px;    color: #fff;font-size:36px;">
        Super Bonus<br>
        Up to <strong style="    color: #fab702;">$500 bonus</strong> + <strong style="    color: #fab702;">$50 spins</strong> at Royal                          
      </h1>
    </template>
    <template #actions>
      <div class="center" style="margin: 25px 40px 10px;">
      <p style="display: inline-block;
      font-weight: 600;
      font-size: 15px;
      margin-right: 10px;
      color: #fff;
      margin-bottom:0;">Get Bonus :</p>
        <span class="bonus center" href="/">NOW50</span>
        <a-button style="padding:15px 45px !important;" class="button general-btn" target="_blank" href="https://modeltheme.com/go/coinflip/">Get Bonus</a-button>
      </div>
    </template> 
  </hero>
  `,
});

WithActions.args = {
  breadcrumbs: [
    { href: "/", name: "Home" },
    { href: "/", name: "Bonuses" },
    { href: "/", name: "Super Bonus" },
  ],
  backgroundImage: "images/background/bg_certified_casions.png",
  description: "100% match bonus based on first deposit of £/$/€20+. Additional bonuses.  "
};
