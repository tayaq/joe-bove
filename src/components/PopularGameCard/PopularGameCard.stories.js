import PopularGameCard from "./PopularGameCard.vue";

export default {
  component: PopularGameCard,
  title: "Components/PopularGameCard",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { PopularGameCard },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<PopularGameCard  v-bind="args" />',
});

Default.args = {
  title: "Narcos",
  thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
};
