import { Thumbnail } from "@/types";

export type PopularGameCard = {
    title: string,
    thumbnail: Thumbnail,
    author: string
    position: string
    description: string
}