import WizardBar from "./WizardBar.vue";

export default {
  component: WizardBar,
  title: "Components/WizardBar",
  parameters: {
    backgrounds: { default: "light" },
    layout: 'padded'
  },
}

export const Default = (args) => ({
  components: { WizardBar },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<WizardBar style="width:100%;" v-bind="args" />',
});

Default.args = {
  payoutTimeout: 5
};