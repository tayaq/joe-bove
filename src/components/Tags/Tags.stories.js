import Tags from "./Tags.vue";

export default {
  component: Tags,
  title: "Components/Tags",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { Tags },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Tags v-bind="args" :items="args.items" />',
});

Default.args = {
  routerPage: 'List',
  params: { category: "licences", component: "casino" },
  items: [
    {
      slug: "/",
      name: "Blandit",
    },
    { slug: "/", name: "Diam" },
    { slug: "/", name: "Egestas" },
    { slug: "/", name: "Mauris" },
    { slug: "/", name: "Elementum" },
    { slug: "/", name: "Euismod" },
  ],
};

export const SquareStyle = (args) => ({
  components: { Tags },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Tags v-bind="args" :items="args.items" />',
});

SquareStyle.args = {
  title: "Tags",
  type: "square",
  routerPage: 'List',
  params: { category: "licences", component: "casino" },
  items: [
    {
      slug: "/",
      name: "Blandit",
    },
    { slug: "/", name: "Diam" },
    { slug: "/", name: "Egestas" },
    { slug: "/", name: "Mauris" },
    { slug: "/", name: "Elementum" },
    { slug: "/", name: "Euismod" },
  ],
};
