import ContactInfo from "./ContactInfo.vue";

export default {
  component: ContactInfo,
  title: "Components/ContactInfo",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { ContactInfo },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<ContactInfo style="width:350px;" v-bind="args" />',
});

Default.args = {
  phoneNo: "+50 900 701 584",
  address: "211 Ullamcorper St Roseville",
  mail: "example@example.com"
};
