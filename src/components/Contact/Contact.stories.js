import Contact from "./Contact.vue";
import ContactInfo from "./ContactInfo.vue";
import ContactForm from "./ContactForm.vue";

export default {
  component: Contact,
  subcomponents: { ContactInfo, ContactForm },
  title: "Components/Contact",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { Contact, ContactInfo },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Contact  v-bind="args" />',
});

Default.args = {
  title: "01.",
  subtitle: "Advice and guide",
  description:
    "After all, as described in Web Design Trends 2015 & 2016, vision dominates a lot of our subconscious interpretation of the world around us that pleasing images create.",
  contactData: {
    phoneNo: "+50 900 701 584",
    address: "211 Ullamcorper St Roseville",
    mail: "example@example.com",
  },
};
