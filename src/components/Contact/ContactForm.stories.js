import ContactForm from "./ContactForm.vue";

export default {
  component: ContactForm,
  title: "Components/ContactForm",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { ContactForm },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<ContactForm style="" v-bind="args" />',
});

Default.args = {
  title: "01.",
  subtitle: "Advice and guide",
  description: "After all, as described in Web Design Trends 2015 & 2016, vision dominates a lot of our subconscious interpretation of the world around us that pleasing images create."
};
