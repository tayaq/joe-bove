import CommentBox from "./CommentBox.vue";

export default {
  component: CommentBox,
  title: "Components/CommentBox",
  parameters: {
    backgrounds: { default: "light" }
  },
  
};

export const Default = (args) => ({
  components: { CommentBox },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CommentBox  v-bind="args" />',
});

Default.args = {
  isReply: true,
  replyAuthorName: "Trader Marcus"
};
