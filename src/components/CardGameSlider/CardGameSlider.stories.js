import CardGamesSlider from "./CardGamesSlider.vue";

export default {
  component: CardGamesSlider,
  title: "Components/CardGamesSlider",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardGamesSlider },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardGamesSlider style="width:1200px" v-bind="args" />',
});

Default.args = {
  items: [
    {
      title: "Over 1000 eSports",
  description: "The Chilling PvP Strategy Mayhem",
  thumbnail: {url:  "images/demosimages/card-game-1.jpeg"}
    },
    {
      title: "Over 1000 eSports",
  description: "The Chilling PvP Strategy Mayhem",
  thumbnail: {url:  "images/demosimages/card-game-2.jpeg"}
    },
    {
      title: "Over 1000 eSports",
  description: "The Chilling PvP Strategy Mayhem",
  thumbnail: {url:  "images/demosimages/card-game-3.jpeg"}
    },
    {
      title: "Over 1000 eSports",
  description: "The Chilling PvP Strategy Mayhem",
  thumbnail: {url:  "images/demosimages/card-game-4.jpeg"}
    },
    {
      title: "Over 1000 eSports",
  description: "The Chilling PvP Strategy Mayhem",
  thumbnail: {url:  "images/demosimages/card-game-5.jpeg"}
    }
  ],
};
