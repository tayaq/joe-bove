import WidgetLayout from "./WidgetLayout.vue";
import CardBlogPost from "../CardBlogPost/CardBlogPost.vue";
export default {
  component: WidgetLayout,
  subcomponents: { CardBlogPost },
  title: "Components/WidgetLayout",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { WidgetLayout },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<WidgetLayout style="width:550px;" v-bind="args" />',
});

Default.args = {
  title: "Popular Casinos",
  items: [
    {
      title: "Jackpot Night",
      description: "Get a $5 reward !",
      thumbnail: { url: "images/casino-logos/cat_casino4.jpg" },
      excerpt: "New customer offer. Exchange bets excluded.",
      mini: true,
      href: "/",
    },
    {
      title: "Jackpot Night",
      description: "Get a $5 reward !",
      thumbnail: { url: "images/casino-logos/cat_casino4.jpg" },
      excerpt: "New customer offer. Exchange bets excluded.",
      mini: true,
      href: "/",
    },
    {
      title: "Jackpot Night",
      description: "Get a $5 reward !",
      thumbnail: { url: "images/casino-logos/cat_casino4.jpg" },
      excerpt: "New customer offer. Exchange bets excluded.",
      mini: true,
      href: "/",
    },
  ],
};

export const RecentNews = (args) => ({
  components: { WidgetLayout, CardBlogPost },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: `
  <WidgetLayout style="width:350px;" v-bind="args">
  <template v-slot:default="slotProps">
  <CardBlogPost v-bind="slotProps.item" :class="{'mt-4' : slotProps.i !== 0}"></CardBlogPost>

  </template>
  </WidgetLayout>`,
});

RecentNews.args = {
  title: "Recent News",
  items: [
    {
      title: "How to Properly Use Casino Fibonacci System for now",
      type: "compact",
      subtitle: "Just flip a Coin",
      description:
        "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
      thumbnail: { url: "images/blogs/article-blog-3.jpg" },
      author: {
        name: "Trader Marcus",
        href: "/",
      },
      details: {
        date: new Date(),
        commentNo: 1,
        category: "BETTING-NEWS",
      },
    },
    {
      title: "Which are the Most Popular Roulette Systems?",
      type: "compact",
      subtitle: "Just flip a Coin",
      description:
        "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
      thumbnail: { url: "images/blogs/article-blog-3.jpg" },
      author: {
        name: "Trader Marcus",
        href: "/",
      },
      details: {
        date: new Date(),
        commentNo: 50,
        category: "E-SPORTS",
      },
    },
  ],
};
