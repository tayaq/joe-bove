import AboutUs from "./AboutUs.vue";

export default {
  component: AboutUs,
  title: "Components/AboutUs",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { AboutUs },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<AboutUs style="max-width:1200px;" v-bind="args" />',
});

Default.args = {
  rtl: false,
  href: "/",
  smalltitle:"Just A FlipCoin",
  subtitle:"At Modeltheme, we show only the best websites and portfolios builtcompletely with passion, simplicity & creativity!",
  title: "What We Offer",
  thumbnail:{url:  "../images/misc/about-us.png"},
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, pharetra a tincidunt quis, semper ac lorem. Sed quis vulputate erat. Nunc viverra felis sed sollicitudin imperdiet. Morbi risus ipsum, hendrerit vel diam non, dictum hendrerit metus. Duis interdum semper posuere. Aliquam in tempus elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;"
};
