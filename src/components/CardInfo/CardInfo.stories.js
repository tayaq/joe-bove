import CardInfo from "./CardInfo.vue";

export default {
  component: CardInfo,
  title: "Components/CardInfo",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardInfo },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardInfo style="width:700px;" v-bind="args" />',
});

Default.args = {
  title: 'Our Address',
  description: 'New York 11673 Collins Street West <br/>  Phone: 0756 192 121',
  icon: {
    type: 'fas',
    name: 'globe'
  },
  link: {
    name: 'Discover',
    href: '/'
  }
};
