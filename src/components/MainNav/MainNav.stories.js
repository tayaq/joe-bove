import MainNav from "./MainNav.vue";

export default {
  component: MainNav,
  title: "Components/MainNav",
  parameters: {
    backgrounds: { default: "light" },
    layout: "fullscreen",
  },
};

export const Default = (args) => ({
  components: { MainNav },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<MainNav v-bind="args" />',
});

Default.args = {
  links: [
    { title: "Homie", path: "/" },
    {
      title: "Features",
      path: "/",
      items: [
        {
          title: "Casino Categories",
          items: [
            { title: "Certified Casinos", path: "/" },
            { title: "Download Casinos", path: "/" },
            { title: "Live Roulette", path: "/" },
            { title: "Mobile Casinos", path: "/" },
            { title: "Online Casinos", path: "/" },
          ],
        },
        {
          title: "Casino Softwares",
          items: [
            { title: "Blandit", path: "/" },
            { title: "Cras", path: "/" },
            { title: "Elementum", path: "/" },
            { title: "Mauris", path: "/" },
            { title: "Viverra", path: "/" },
          ],
        },
        {
          title: "Payment Methods",
          items: [
            { title: "Cash", path: "/" },
            { title: "Credit Cards", path: "/" },
            { title: "Direct Deposit", path: "/" },
            { title: "Ewallet", path: "/" },
            { title: "Mobile Pay", path: "/" },
          ],
        },
        {
          title: "Casino Languages",
          items: [
            { title: "English", path: "/" },
            { title: "French", path: "/" },
            { title: "German", path: "/" },
            { title: "Polish", path: "/" },
            { title: "Russian", path: "/" },
          ],
        },
      ],
    },
    { title: "Shortcodes", path: "/" },
    { title: "Casinos", path: "/" },
    { title: "Slots", path: "/" },
    { title: "Bonuses", path: "/" },
    { title: "Bookmakers", path: "/" },
    { title: "Reviews", path: "/" },
    { title: "News", path: "/" },
  ],
};
