import Submenu from "./Submenu.vue";

export default {
  component: Submenu,
  title: "Components/Submenu",
  parameters: {
    backgrounds: { default: "light" },
    layout: "padded",
  },
};

export const Default = (args) => ({
  components: { Submenu },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: `          
  <div class="position-relative" style="width:5px;">
    <Submenu :class="{active : active}" v-bind="args"/>
  </div>`,
});

Default.args = {
  items: [
    {
      title: 'Casino Categories',
      items: [
        {title: 'Certified Casinos',path: '/'},
        {title: 'Download Casinos',path: '/'},
        {title: 'Live Roulette',path: '/'},
        {title: 'Mobile Casinos',path: '/'},
        {title: 'Online Casinos',path: '/'},
      ]
    },
    {
      title: 'Casino Softwares',
      items: [
        {title: 'Blandit',path: '/'},
        {title: 'Cras',path: '/'},
        {title: 'Elementum',path: '/'},
        {title: 'Mauris',path: '/'},
        {title: 'Viverra',path: '/'},
      ]
    },
    {
      title: 'Payment Methods',
      items: [
        {title: 'Cash',path: '/'},
        {title: 'Credit Cards',path: '/'},
        {title: 'Direct Deposit',path: '/'},
        {title: 'Ewallet',path: '/'},
        {title: 'Mobile Pay',path: '/'},
      ]
    },
    {
      title: 'Casino Languages',
      items: [
        {title: 'English',path: '/'},
        {title: 'French',path: '/'},
        {title: 'German',path: '/'},
        {title: 'Polish',path: '/'},
        {title: 'Russian',path: '/'},
      ]
    },
  ],
  itemsPerRow: 4
};
