import ProConBox from "./ProConBox.vue";

export default {
  component: ProConBox,
  title: "Components/ProConBox",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { ProConBox },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<ProConBox v-bind="args" />',
});

Default.args = {
  items: {
    pros: [
      "Unlimited withdraw limits",
      "Multiple withdraw methods",
      "Live chat open 24/7",
      "Mobile-friendly design"
    ],
    cons: [
      "A number of restricted countries",
      "Slow connection sometimes",
      "Long withdraw time",
      "Some bugs"
    ]
  }
}