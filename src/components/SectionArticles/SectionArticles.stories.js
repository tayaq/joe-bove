import SectionArticles from "./SectionArticles.vue";

export default {
  component: SectionArticles,
  title: "Components/SectionArticles",
  parameters: {
    backgrounds: { default: "light" }
  },
};

export const Default = (args) => ({
  components: { SectionArticles },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<SectionArticles  v-bind="args" />',
});

Default.args = {
items:[
  {
  smalltitle:"betting-news",
  title: "Four ways to cheer yourself up on the Next Blue Monday!",
  thumbnail: {url:  "images/demosimages/article-blog-1.png"},
  date:"7 jul",
  link:"https://coinflip.modeltheme.com/four-ways-to-cheer-yourself-up-on-the-next-blue-monday/"
  },
  {
  smalltitle:"betting-news",
  title: "Four ways to cheer yourself up on the Next Blue Monday!",
  thumbnail: {url:  "images/demosimages/article-blog-1.png"},
  date:"7 jul",
  link:"https://coinflip.modeltheme.com/four-ways-to-cheer-yourself-up-on-the-next-blue-monday/"
  },
  {
  smalltitle:"betting-news",
  title: "Four ways to cheer yourself up on the Next Blue Monday!",
  thumbnail: {url:  "images/demosimages/article-blog-1.png"},
  date:"7 jul",
  link:"https://coinflip.modeltheme.com/four-ways-to-cheer-yourself-up-on-the-next-blue-monday/"
  }

]
};
