import CasinoFeatures from "./CasinoFeatures.vue";

export default {
  component: CasinoFeatures,
  title: "Components/CasinoFeatures",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CasinoFeatures },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CasinoFeatures style="width:700px;" v-bind="args" />',
});

Default.args = {
  description:"<p><strong>Coinflip Casino</strong> is an Online Casino. It features 5 games and you can get a <strong>$5 reward</strong> just for registering right now.</p>",
  items: [
    "Excepteur sint occaecat cupidatat",
    "Duis aute irure dolor",
    "Lorem ipsum dolor sit amet, consectetur",
    "Fames ac turpis egestas maecenas",
    "Dignissim cras tincidunt lobortis"
  ]
};
