import VideoPopup from "./VideoPopup.vue";

export default {
  component: VideoPopup,
  title: "Components/VideoPopup",
  parameters: {
    backgrounds: { default: "light" },
    layout: 'padded'
  },
}

export const Default = (args) => ({
  components: { VideoPopup },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<VideoPopup style="width:100%;" v-bind="args" />',
});

Default.args = {
  youtubeLink: '//www.youtube.com/embed/Ue5ght7izjc?autoplay=1'
};