import PopularGamesSlider from "./PopularGamesSlider.vue";

export default {
  component: PopularGamesSlider,
  title: "Components/PopularGamesSlider",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { PopularGamesSlider },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<PopularGamesSlider style="width:1200px" v-bind="args" />',
});

Default.args = {
  items: [
    {
      title: "Soccer Games",
      thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
    {
      title: "Narcos",
      thumbnail: { url: "images/demosimages/PopularGameCard-2.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
    {
      title: "Narcos",
      thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
    {
      title: "Narcos",
      thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
    {
      title: "Narcos",
      thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
    {
      title: "Narcos",
      thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
    {
      title: "Narcos",
      thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
    {
      title: "Narcos",
      thumbnail: { url: "images/demosimages/PopularGameCard-1.jpeg" },
      link: "https://coinflip.modeltheme.com/slot/soccer-games/",
    },
  ],
};
