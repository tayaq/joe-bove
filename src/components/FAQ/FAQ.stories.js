import FAQ from "./FAQ.vue";

export default {
  component: FAQ,
  title: "Components/FAQ",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { FAQ },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<FAQ style="width:700px;" v-bind="args" />',
});

Default.args = {
  items: [
    {
      question: "Which Plan is good for me ?",
      answer:
        "Trends, vision dominates a lot of our subconscious interpretation of the world around us. On top it, pleasing images create better user experience.    ",
    },
    {
      question: "Do I have to commit to a program",
      answer:
        "Trends, vision dominates a lot of our subconscious interpretation of the world around us. On top it, pleasing images create better user experience.    ",
    },
    {
      question: "What Payment Methods are Available?",
      answer:
        "Trends, vision dominates a lot of our subconscious interpretation of the world around us. On top it, pleasing images create better user experience.    ",
    },
  ],
};
