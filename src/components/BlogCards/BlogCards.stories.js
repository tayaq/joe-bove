import BlogCards from "./BlogCards.vue";

export default {
  component: BlogCards,
  title: "Components/BlogCards",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { BlogCards },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<BlogCards style="width:380px;height:380px;" v-bind="args" />',
});

Default.args = {
  smalltitle:"betting-news",
  title: "Four ways to cheer yourself up on the Next Blue Monday!",
  thumbnail: {url:  "images/demosimages/article-blog-1.png"},
  date: new Date(),
  link:"https://coinflip.modeltheme.com/four-ways-to-cheer-yourself-up-on-the-next-blue-monday/"
};
