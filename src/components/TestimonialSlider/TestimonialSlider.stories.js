import TestimonialSlider from "./TestimonialSlider.vue";

export default {
  component: TestimonialSlider,
  title: "Components/TestimonialSlider",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { TestimonialSlider },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<TestimonialSlider style="width:700px;" v-bind="args" />',
});

Default.args = {
  items: [
    {
      author: "Thomas Moriz",
      avatar: { url: "images/avatars/testimonial1-v1.jpg" },
      position: "CEO MODELTHEME",
      message:
        " “My project was a simple & small task, but the persistence and determination of the team turned it into an awesome and great project which make me very happy” ",
    },
    {
      author: "Roberta Cozza",
      avatar: { url: "images/avatars/testimonial-member2.jpg" },
      position: "General Director",
      message:
        " Sed perspiciatis unde omnis natus error sit voluptatem totam rem aperiam, eaque quae architecto beatae explicabo. ",
    },
  ],
};
