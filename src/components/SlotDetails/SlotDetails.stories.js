import SlotDetails from "./SlotDetails.vue";

export default {
  component: SlotDetails,
  title: "Components/SlotDetails",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { SlotDetails },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<SlotDetails  v-bind="args" />',
});

Default.args = {
  title: "Generations",
  developer: "by The Plaza",
  description:
    "100% match bonus based on first deposit of £/$/€20+. Additional bonuses.   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .   Sed perspiciatis unde omnis natus error sit voluptatem totam aperiam, eaque omnis quae architecto beatae .",
  developerLink: "/",
};
