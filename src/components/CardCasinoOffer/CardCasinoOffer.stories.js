import CardCasinoOffer from "./CardCasinoOffer.vue";

export default {
  component: CardCasinoOffer,
  title: "Components/CardCasinoOffer",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { CardCasinoOffer },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardCasinoOffer style="width:300px;" v-bind="args" />',
});

Default.args = {
  title: "Royal Vegas",
  reward: "Get a $5 reward !",
  description: "Jackpot Night is an Online Casino. It features 5 games",
  rating: 3,
  offer: "New customer offer. Exchange bets excluded.",
  thumbnail: {url: "images/casino-logos/cat_casino4.jpg"},
  href: '/',
  badge: {
    positionNo: 2,
    type: 'default'
  }
};
