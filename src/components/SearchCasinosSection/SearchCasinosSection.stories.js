import SearchCasinosSection from "./SearchCasinosSection.vue";

export default {
  component: SearchCasinosSection,
  title: "Components/SearchCasinosSection",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { SearchCasinosSection },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<SearchCasinosSection style="width:1300px" v-bind="args" />',
});

Default.args = {
  title: "Find Verified Offers From Trusted Casinos",
  description:
    "We have reviewed 2000+ deals to make your casino gaming experience better",
};
