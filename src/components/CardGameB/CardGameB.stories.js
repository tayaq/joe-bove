import CardGameB from "./CardGameB.vue";

export default {
  component: CardGameB,
  title: "Components/CardGameB",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardGameB },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardGameB style="width:300px;" v-bind="args" />',
});

Default.args = {
  title: "Over 1000 eSports",
  description: "The Chilling PvP Strategy Mayhem",
  thumbnail: {url:  "images/demosimages/card-game-1.jpeg"},
  badgeText: 'Get It Now',
  href: '/'
};
