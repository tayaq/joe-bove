import { Thumbnail } from "@/types"
export type CardGameB = {
    thumbnail: Thumbnail,
    title:string,
    description: string,
    badgeText: string
}