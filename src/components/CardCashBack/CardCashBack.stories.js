import CardCashBack from "./CardCashBack.vue";

export default {
  component: CardCashBack,
  title: "Components/CardCashBack",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardCashBack },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardCashBack style="width:290px;" v-bind="args" />',
});

Default.args = {
  bonus: "100RF",
  rewards: "100% match bonus based on first deposit of £/$/€20+. Additional bonuses.",
  category: "affiliate cashback"
};
