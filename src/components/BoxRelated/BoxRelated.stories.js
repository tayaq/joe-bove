import BoxRelated from "./BoxRelated.vue";

export default {
  component: BoxRelated,
  title: "Components/BoxRelated",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { BoxRelated },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<BoxRelated style="width:300px;" v-bind="args" />',
});

Default.args = {
  tags: "Edge, Games, Roullete",
  title: "Early Back Friday deals at Buy - Xbox One",
  thumbnail: {url: "../images/misc/related-post-thumbnail.jpeg"},
  description:
    "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum",
  author: "Trader Marcus",
  href: "/"
};
