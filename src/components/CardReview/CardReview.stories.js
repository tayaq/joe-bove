import CardReview from "./CardReview.vue";

export default {
  component: CardReview,
  title: "Components/CardReview",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { CardReview },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardReview style="width:300px;" v-bind="args" />',
});

Default.args = {
  title: "Royal Vegas",
  thumbnail: "images/casino-logos/cat_casino4.jpg",
  forItems: [
    {href: '/', title: 'Get Lucky'},
    {href: '/', title: 'Narcos'},
    {href: '/', title: 'Good Bonus'},
  ]
};
