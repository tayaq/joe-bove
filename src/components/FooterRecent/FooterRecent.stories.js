import FooterRecent from "./FooterRecent.vue";

export default {
  component: FooterRecent,
  title: "Components/FooterRecent",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { FooterRecent },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<FooterRecent style="width:300px;" v-bind="args" />',
});

Default.args = {
  title: "About Us",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
  posts: [
    {thumbnail: {url :'images/blogs/article-blog-1.jpg'}, title: 'Four ways to cheer..', date: new Date(), href: '/'},
    {thumbnail: {url :'images/blogs/article-blog-3.jpg'}, title: 'Which are the Most..', date: new Date(), href: '/'}
  ]
};
