import CardCasinoOnline from "./CardCasinoOnline.vue";

export default {
  component: CardCasinoOnline,
  title: "Components/CardCasinoOnline",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardCasinoOnline },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardCasinoOnline style="width:550px;" v-bind="args" />',
});

Default.args = {
  title: "Jackpot Night is an Online Casino. It",
  description: "Get a $5 reward !",
  thumbnail: {url:"images/casino-logos/cat_casino4.jpg"},
  offer: "New customer offer. Exchange bets excluded.",
  mini: false
};

export const Mini = (args) => ({
  components: { CardCasinoOnline },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardCasinoOnline style="width:350px;" v-bind="args" />',
});

Mini.args = {
  title: "Jackpot Night",
  description: "Get a $5 reward !",
  thumbnail: {url:"images/casino-logos/cat_casino4.jpg"},
  offer: "New customer offer. Exchange bets excluded.",
  mini: true
};
