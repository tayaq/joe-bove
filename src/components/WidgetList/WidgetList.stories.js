import WidgetList from "./WidgetList.vue";

export default {
  component: WidgetList,
  title: "Components/WidgetList",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { WidgetList },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<WidgetList  v-bind="args" />',
});

Default.args = {
  title: "Widgets",
  items:[
    {
      name:"Betting News",
      count:"(4)",
      href:"https://coinflip.modeltheme.com/bonus-categories/cashback-bonuses/"
    },
    {
      name:"Bookmasters",
      count:"(5)",
      href:"/"
    },
    {
      name:"Casino Affiliate",
      count:"(3)",
      href:"/"
    },
    {
      name:"Esport Betting",
      count:"(1)",
      href:"/"
    },
    {
      name:"Esport Betting2",
      count:"(3)",
      href:"/"
    },
    {
      name:"Gambling News",
      count:"(5)",
      href:"/"
    }
]
};
