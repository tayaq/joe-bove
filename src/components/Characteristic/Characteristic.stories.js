import Characteristic from "./Characteristic.vue";

export default {
  component: Characteristic,
  title: "Components/Characteristic",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { Characteristic },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Characteristic style="width:350px;" v-bind="args" />',
});

Default.args = {
  title: "01.",
  subtitle: "Advice and guide",
  description: "After all, as described in Web Design Trends 2015 & 2016, vision dominates a lot of our subconscious interpretation of the world around us that pleasing images create."
};
