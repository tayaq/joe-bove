import SubtitleSections from "./SubtitleSections.vue";

export default {
  component: SubtitleSections,
  title: "Components/SubtitleSections",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { SubtitleSections },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<SubtitleSections style="width:800px" v-bind="args" />',
});

Default.args = {
  title: "Gambling News",
  subtitle:"At Coinflip, we show only the best casinos."
}