import CardCashbackEsports from "./CardCashbackEsports.vue";

export default {
  component: CardCashbackEsports,
  title: "Components/CardCashbackEsports",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardCashbackEsports },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardCashbackEsports style="width:290px;"  v-bind="args" />',
});

Default.args = {
  bonus: "Coinflip",
  type: "Cashback Bonuses",
  features: [
    "€122 WELCOME BONUS",
    "24/7 SUPPORT",
    "WIDE ARRAY OF ESPORT"
  ],
};
