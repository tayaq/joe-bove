import Pagination from "./Pagination.vue";

export default {
  component: Pagination,
  title: "Components/Pagination",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { Pagination },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Pagination v-bind="args" :items="args.items" />',
});

Default.args = {
  routerPage: 'List',
  params: { category: "licences", component: "casino" },
  items: [
    {
      slug: "/",
      name: "Blandit",
    },
    { slug: "/", name: "Diam" },
    { slug: "/", name: "Egestas" },
    { slug: "/", name: "Mauris" },
    { slug: "/", name: "Elementum" },
    { slug: "/", name: "Euismod" },
  ],
};

export const SquareStyle = (args) => ({
  components: { Pagination },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Pagination v-bind="args" :items="args.items" />',
});


  items: [
    {
      slug: "/",
      name: "Blandit",
    },
    { slug: "/", name: "Diam" },
    { slug: "/", name: "Egestas" },
    { slug: "/", name: "Mauris" },
    { slug: "/", name: "Elementum" },
    { slug: "/", name: "Euismod" },
  ]

