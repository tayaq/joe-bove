import Comment from "./Comment.vue";

export default {
  component: Comment,
  title: "Components/Comment",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { Comment },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Comment style="width:700px;"  v-bind="args" />',
});

Default.args = {
  authorName: "Trader Marcus",
  content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it.",
  thumbnail: "images/avatars/comment.png",
  createdAt:"March 4,2020 at 10:12 am"
};
