import CharacteristicsEsports from "./CharacteristicsEsports.vue";

export default {
  component: CharacteristicsEsports,
  title: "Components/CharacteristicsEsports",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { CharacteristicsEsports },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CharacteristicsEsports style="width:350px;" v-bind="args" />',
});

Default.args = {
  title: "Over 1000 eSports",
  subtitle: "Opt in & bet on any markets",
  thumbnail: {url:  "images/demosimages/icon-esport-betting1.png"}
};
