import Rating from "./Rating.vue";

export default {
  component: Rating,
  title: "Components/Rating",
  parameters: {
    backgrounds: { default: "light" },
  },
  argTypes: {
    position: {
      options: ["none", "top-right", "top-left", "bottom-right", "bottom-left"],
      control: { type: "select" },
    },
    type: {
      options: ["one", "two", "three", "four"],
      control: { type: "select" },
    },
  },
};

export const Default = (args) => ({
  components: { Rating },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Rating v-bind="args" />',
});

Default.args = {
  maxStars: 5,
  ratings: [
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
  ],
};
export const OneStar= (args) => ({
  components: { Rating },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Rating  v-bind="args" />',
});

OneStar.args = {
  limitToOneStar: true,
  maxStars: 5,
  ratings: [
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
  ],
};

export const OneStarWithRating = (args) => ({
  components: { Rating },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Rating  v-bind="args" />',
});

OneStarWithRating.args = {
  limitToOneStar: true,
  hasRating: true,
  maxStars: 5,
  ratings: [
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
  ],
};

export const OneStarWithRatingOutOfN = (args) => ({
  components: { Rating },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Rating v-bind="args" />',
});

OneStarWithRatingOutOfN.args = {
  maxStars: 5,
  hasRatingOutOf: true,
  limitToOneStar: true,
  ratings: [
    {
      percentage: 10,
    },
    {
      percentage: 0,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
  ],
};

export const MultipleStars = (args) => ({
  components: { Rating },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Rating  v-bind="args" />',
});

MultipleStars.args = {
  maxStars: 10,
  ratings: [
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
    {
      percentage: 10,
    },
  ],
};

export const MultipleStarsWithRating = (args) => ({
  components: { Rating },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Rating  v-bind="args" />',
});

MultipleStarsWithRating.args = {
  type: "four",
  maxStars: 10,
  hasRatingOutOf: true,
  ratings: [
    {
      percentage: 10,
    },
    {
      percentage: 40,
    },
    {
      percentage: 40,
    },
    {
      percentage: 30,
    },
    {
      percentage: 10,
    },
  ],
};
