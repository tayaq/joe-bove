import CardNews from "./CardNews.vue";

export default {
  component: CardNews,
  title: "Components/CardNews",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardNews },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardNews style="width:390px;" v-bind="args" />',
});

Default.args = {
  title: "Slot Machine that has no paylines",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum complectitur et, eam ut veri",
  href: "/",
  author: {
    href: '/',
    name: 'Trader Marcus'
  }
};

export const WithImage = (args) => ({
  components: { CardNews },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardNews style="width:390px;" v-bind="args" />',
});

WithImage.args = {
  title: "Slot Machine that has no paylines",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum complectitur et, eam ut veri",
  href: "/",
  author: {
    href: '/',
    name: 'Trader Marcus'
  },
  thumbnail: {
    url: "images/demosimages/card-blog-1.jpeg"
  }
};