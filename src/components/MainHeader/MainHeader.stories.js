import MainHeader from "./MainHeader.vue";

export default {
  component: MainHeader,
  title: "Components/MainHeader",
  parameters: {
    backgrounds: { default: "light" },
    layout: "fullscreen"
  },
};

export const Default = (args) => ({
  components: { MainHeader },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<MainHeader style="height:700px" v-bind="args" />',
});

Default.args = {
  title: "Joining Casinos",
  subtitle: "Hi there, we are Coinflip !",
  description: "Duis aute irure dolor in reprehenderit in <br/> volup velit esse cillum dolore eu fugiat nulla. ",
  buttonName : "CONTACT US",
  href : "/",
  secondaryText : "contact@coinflip.com"
};
