import CardGame from "./CardGame.vue";

export default {
  component: CardGame,
  title: "Components/CardGame",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { CardGame },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CardGame style="width:390px;" v-bind="args" />',
});

Default.args = {
  title: "War of Empires",
  description: "First time to do a review for this casino since",
  thumbnail: {url:  "images/games/esport-betting-slot3.jpg"}
};
