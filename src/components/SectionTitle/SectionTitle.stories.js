import SectionTitle from "./SectionTitle.vue";

export default {
  component: SectionTitle,
  title: "Components/SectionTitle",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { SectionTitle },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<SectionTitle style="max-width:800px;" v-bind="args" />',
});

Default.args = {
  title: "About Us",
  removeBorderBottom: false,
  slim: false
}