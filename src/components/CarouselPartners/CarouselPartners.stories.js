import CarouselPartners from "./CarouselPartners.vue";

export default {
  // component: CarouselPartners,
  // title: "Components/CarouselPartners",
  // parameters: {
  //   backgrounds: { default: "light" },
  //   layout: 'fullscreen'
  // },
};

export const Default = (args) => ({
  components: { CarouselPartners },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CarouselPartners v-bind="args" />',
});

Default.args = {
  items: [
    "images/partners/client_1.png",
    "images/partners/client_02.png",
    "images/partners/client_3.png",
    "images/partners/client_4.png",
    "images/partners/client_5.png",
  ],

};
