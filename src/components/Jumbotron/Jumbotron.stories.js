import Jumbotron from "./Jumbotron.vue";

export default {
  component: Jumbotron,
  title: "Components/Jumbotron",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { Jumbotron },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Jumbotron style="width:400px;" v-bind="args" />',
});

Default.args = {
  title: "Looking to get into the exciting world of online gambling?",
  subtitle: "Just flip a Coin",
  description: "Dress with crossover V-neckline with ruffled trims. Features long sleeves with ruffled trims, elastic cuffs, a cinched waist and a ruffled hem lorem ipsum dolor sid amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.  ",
  buttonName : "Read more"
};
