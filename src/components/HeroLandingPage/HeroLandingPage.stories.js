import HeroLandingPage from "./HeroLandingPage.vue";

export default {
  component: HeroLandingPage,
  title: "Components/HeroLandingPage",
  parameters: {
    backgrounds: { default: "light" },
    layout: "fullscreen"
  },
};

export const Default = (args) => ({
  components: { HeroLandingPage },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<HeroLandingPage v-bind="args" />',
});

Default.args = {
    title: "Casino Landing Page",
    description:"This is a sample external page for after you click the button links on the website custom post types (casino, games, etc.)",
    purchaseLink: "https://modeltheme.com/go/purchase-coinflip-theme/",
    youtubeLink: "//www.youtube.com/embed/_GhNZrBbWfQ?autoplay=1"
}