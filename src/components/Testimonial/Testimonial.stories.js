import Testimonial from "./Testimonial.vue";

export default {
  component: Testimonial,
  title: "Components/Testimonial",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { Testimonial },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Testimonial style="width:700px;" v-bind="args" />',
});

Default.args = {
  author:"Thomas Moriz",
  avatar : {url: "images/avatars/testimonial1-v1.jpg"},
  position: "CEO MODELTHEME",
  message: " “My project was a simple & small task, but the persistence and determination of the team turned it into an awesome and great project which make me very happy” "
};
