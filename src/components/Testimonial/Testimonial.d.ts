export type Testimonial = {
    thumbnail: string,
    author: string
    position: string
    description: string
}