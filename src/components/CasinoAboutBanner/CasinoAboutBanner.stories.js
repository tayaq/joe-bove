import CasinoAboutBanner from "./CasinoAboutBanner.vue";

export default {
  component: CasinoAboutBanner,
  title: "Components/CasinoAboutBanner",
  parameters: {
    backgrounds: { default: "light" },
    layout: "fullscreen"
  },
};

export const Default = (args) => ({
  components: { CasinoAboutBanner },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<div class="position-relative"><CasinoAboutBanner style="width:700px;" v-bind="args" /></div>',
});

Default.args = {
  title: "Royal Vegas",
  href: "https://modeltheme.com/go/coinflip/",
  thumbnail : {url: "images/casino-logos/cat_casino4.jpg"},
};
