import BoxNews from "./BoxNews.vue";

export default {
  component: BoxNews,
  title: "Components/BoxNews",
  parameters: {
    backgrounds: { default: "light" },
  },
};

export const Default = (args) => ({
  components: { BoxNews },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<BoxNews v-bind="args" />',
});

Default.args = {
  categoryName: "Betting News",
  buttonText: "Continue Reading",
  title: "Which are the Most Popular Roulette Systems?",
  thumbnail: {url: "../images/news/box-news.jpeg"} ,
  description:
    "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum complectitur et, eam ut veri omnium fabulas. Mel et duis dicat prodesset ei, ius […]",
  author: { name: "Trader Marcus", href: "/" },
  href: "/"
};
