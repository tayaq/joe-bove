import SearchBox from "./SearchBox.vue";

export default {
  component: SearchBox,
  title: "Components/SearchBox",
  parameters: {
    backgrounds: { default: "light" }
  },
};

export const Default = (args) => ({
  components: { SearchBox },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<SearchBox  v-bind="args" />',
});

Default.args = {
  SearchBox: 3
};
