import CallToActionSection from "./CallToActionSection.vue";

export default {
  component: CallToActionSection,
  title: "Components/CallToActionSection",
  parameters: {
    backgrounds: { default: "light" },
    layout: "fullscreen"
  },
};

export const Default = (args) => ({
  components: { CallToActionSection },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<CallToActionSection v-bind="args" />',
});

Default.args = {
  title: "A Platform for eSports",
  description: "Dress with crossover V-neckline with ruffled trims.opns Features & longslee sleeves with ruffled trims, elastic cuffs, a cinched waist ruffled hem loreming ipsum dolor amet. Lorem ipsum dolor amet.",
  thumbnail: {url:  "images/demosimages/logo-coinflip-esports.png"}
};
