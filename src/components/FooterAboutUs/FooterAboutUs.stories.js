import FooterAboutUs from "./FooterAboutUs.vue";

export default {
  component: FooterAboutUs,
  title: "Components/FooterAboutUs",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { FooterAboutUs },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<FooterAboutUs style="width:300px;" v-bind="args" />',
});

Default.args = {
  title: "About Us",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
  contacts: [
    { name: 'Email Us: ' ,actionURL: 'mailto:office@example.com', actionMask: 'office@example.com' }
  ],
  socials: [
    {icon: {name: 'facebook', type: 'fab'}, name:'facebook', href: '/'},
    {icon: {name: 'twitter', type: 'fab'}, name:'twitter', href: '/'}
  ]
};
