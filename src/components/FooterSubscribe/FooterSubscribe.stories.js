import FooterSubscribe from "./FooterSubscribe.vue";

export default {
  component: FooterSubscribe,
  title: "Components/FooterSubscribe",
  parameters: {
    backgrounds: { default: "dark" },
  },
};

export const Default = (args) => ({
  components: { FooterSubscribe },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<FooterSubscribe style="width:300px;" v-bind="args" />',
});

Default.args = {
  title: "SUBSCRIBE",
  description: "Lorem ipsum dolor sit amet, cibo mundi ea duo, vim exerci phaedrum (..)  ",
};
