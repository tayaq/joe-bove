export interface Author {
  name: string;
  href: string;
}
export type API_Contact = {
  mail: string;
  phoneNo: string;
  address: string;
};
export type API_Review = {
  bookmakers: API_Bookmaker[]; //todo
  casinos: Casino[];
  content: string;
  created_at: string;
  id?: number;
  published_at: string;
  slot: null; // todo
  slug: string;
  thumbnail: Thumbnail;
  rating: Rating[];
  title: string;
  updated_at: string;
};
export type API_Characteristics = {
  characteristics: {
    title: String;
    subtitle: String;
  }[];
  sectionTitle: {
    title: String;
    subtitle: String;
    description: String;
  };
};
export interface SlotDetails {
  title: string;
  developer: string;
  description: string;
}

export type AboutUs = {
  thumbnail: string;
  title: string;
  subtitle: string;
  content: string;
  smalltitle: string;
};
export type Bookmakers = {
  title: string;
  features: {
    icon: string;
    tooltip: string;
  }[];
  comments: string;
  offerValue: string;
  offerInfo: string;
  link: string;
};
export interface Widgets {
  name: string;
  count: number;
  links: Link[] | object[];
}
export interface Comments {
  name: string;
  thumbnail: Thumbnail;
  date: Date;
  content: string;
}
export interface Filters {
  name: string;
  href: string;
}

export interface Tag {
  name: string;
  slug: string;
  code?: string;
  id: number;
}

export interface Blog_Tag extends Tag {
  blogs: API_Blog[];
}

export type API_Comment = {
  authorId?: number;
  authorName: string;
  authorEmail: string;
  content: string;
  authorWebsite?: string;
  threadOf?: number; // id of comment we would like to start / continue the thread (Optional)
  related: {
    refId: number; // ??????
    ref: string; // entity maybe ??
    field: string; // relation field ??
  }[];
  blog?: API_Blog[]
};
export interface HeroCertfCasino {
  href: string;
}
export interface BlogDetails {
  commentNo: number;
  date: Date;
  category?: string;
}
// icon must be imported in /utils/icons/{respective_icon_type}
export interface Icon {
  type: "fas" | "far" | "fab";
  name: string;
}
export interface Link {
  href: string | object;
  name: string;
  target?: string;
}
/** FOOTER ABOUT US  */
export interface SocialLink extends Link {
  icon: Icon;
}
export interface ContactLink extends Link {
  actionMask: string;
  actionURL: string;
}

/** FOOTER (RECENT POSTS) */
export type MiniPost = {
  thumbnail: Thumbnail;
  title: string;
  date: Date;
  href: string;
};

/** NAVBAR */
export interface MegaMenuLink {
  title?: string;
  links: Link[];
}
export interface Badge {
  text: string;
  type: string;
}
export interface API_Menu_Link {
  id: number;
  title: string;
  menuAttached: boolean;
  path: string;
  type: "INTERNAL" | "EXTERNAL";
  uiRouterKey: string;
  slug: string;
  external: boolean;
  related: string;
  items: API_Menu_Link[];
}
export interface API_Menu {
  id: number;
  siteName: string;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  socials: {
    id: number;
    icon: Icon;
    link: SocialLink;
  }[];
  logo: Thumbnail;
}
export interface BadgeColors {
  bgColor: string;
  textColor: string;
}
export interface NavLink extends Link {
  submenu?: MegaMenuLink[];
  badge?: Badge;
}

export interface FAQitem {
  header: string;
  desc: string;
}

export interface ProCons {
  pros: string[];
  cons: string[];
}

export interface Rating {
  id: number;
  title: string;
  percentage: number;
  rating: number[];
}

export type Position =
  | "top-right"
  | "top-left"
  | "bottom-right"
  | "bottom-left"
  | "none";

export interface Image {
  id: number;
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string | null;
  provider: string;
  providerMetadata: string | null;
  createdAt: string;
  updatedAt: string;
}

export interface FileFormat {
  name: string;
  hash: string;
  ext: string;
  mime: string;
  width: number;
  height: number;
  size: number;
  path: string | null;
  url: string;
}
export interface Thumbnail extends Image {
  formats: {
    thumbnail: FileFormat;
  };
}
export interface AcceptedCountries {
  id: number;
  text: string;
}

export interface API_CustomEnumeration {
  text: string;
}
export interface Casino {
  id: number;
  title: string;
  excerpt: string;
  description: string;
  about: string;
  type: "affiliate" | "certified" | "download" | "live" | "mobile" | "online";
  likes: number;
  referralLink: string | null;
  active: boolean;
  slug: string;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  experiences: [];
  review: API_Review;
  socials: SocialLink[];
  acceptedCountries: AcceptedCountries[];
  thumbnail: Thumbnail;
  cover: Thumbnail;
  tags: {
    categories?: Tag[];
    currencies?: Tag[];
    languages?: Tag[];
    licences?: Tag[];
    paymentMethods?: Tag[];
    restrictedCountries?: Tag[];
  };
  features: API_CustomEnumeration[];
}
export interface Bonus {
  id: number;
  title: string;
  bonus: string;
  excerpt: string;
  description: string;
  about: string;
  type: "affiliate" | "certified" | "download" | "live" | "mobile" | "online";
  likes: number;
  referralLink: string | null;
  active: boolean;
  slug: string;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  experiences: [];
  review: {
    rating: Rating[];
  };
  socials: SocialLink[];
  acceptedCountries: AcceptedCountries[];
  thumbnail: Thumbnail;
  tags: {
    categories?: Tag[];
    currencies?: Tag[];
    languages?: Tag[];
    licences?: Tag[];
    paymentMethods?: Tag[];
    restrictedCountries?: Tag[];
  };
  features: API_CustomEnumeration[];
}

export interface Slot {
  id: number;
  title: string;
  excerpt: string;
  description: string;
  about: string;
  type: "affiliate" | "certified" | "download" | "live" | "mobile" | "online";
  likes: number;
  referralLink: string | null;
  active: boolean;
  slug: string;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  experiences: [];
  review: {
    rating: Rating[];
  };
  socials: SocialLink[];
  acceptedCountries: AcceptedCountries[];
  thumbnail: Thumbnail;
  tags: {
    categories?: Tag[];
    currencies?: Tag[];
    languages?: Tag[];
    licences?: Tag[];
    paymentMethods?: Tag[];
    restrictedCountries?: Tag[];
  };
  features: API_CustomEnumeration[];
}

export type CommentCTX = {
  id?: number;
  contentID: number;
  contentType: string;
};
export interface API_Footer {
  aboutUs: string;
  email: string;
  phoneNo: string;
  subscribeText: string;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  socials: SocialLink[];
}
export interface API_Blog {
  id?: number;
  title: string;
  description: string;
  slug: string;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  thumbnail: Thumbnail;
  blogTags: Tag[];
  author: {
    id: number;
    firstname: string;
    lastname: string;
    username: string;
    preferedLanguage: string;
  };
}
export interface API_Bookmaker {
  id: number;
  title: string;
  excerpt: string;
  about: string;
  type: "affiliate" | "certified" | "download" | "live" | "mobile" | "online";
  likes: number;
  referralLink: string;
  active: boolean;
  slug: string;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  termsAndConditions: string;
  experiences: [];
  review: API_Review;
  socials: API_CustomEnumeration[];
  // acceptedCountries: [{ id: 95; text: "RO" }];
  tags: {
    categories?: Tag[];
    currencies?: Tag[];
    languages?: Tag[];
    licences?: Tag[];
    paymentMethods?: Tag[];
    restrictedCountries?: Tag[];
  };
  features: API_CustomEnumeration[];
  thumbnail: Thumbnail;
  cover: Thumbnail;
}

export interface API_Slot {
  id: number;
  title: string;
  excerpt: string;
  about: string;
  type: "affiliate" | "certified" | "download" | "live" | "mobile" | "online";
  likes: number;
  referralLink: string;
  active: boolean;
  slug: string;
  review: API_Review;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  termsAndConditions: string;
  experiences: [];
  socials: API_CustomEnumeration[];
  tags: {
    categories?: Tag[];
    currencies?: Tag[];
    languages?: Tag[];
    licences?: Tag[];
    paymentMethods?: Tag[];
    restrictedCountries?: Tag[];
  };
  features: API_CustomEnumeration[];
  thumbnail: Thumbnail;
  cover: Thumbnail | null;
  comments: [];
  casino: Casino;
}

export interface API_Bonus {
  id: number;
  bonus: string;
  title: string;
  bonusLink: string;
  rewards: string;
  excerpt: string;
  about: string;
  slug: string;
  termsAndConditions: string;
  review: API_Review;
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  experiences: [];
  background: Thumbnail;
  categories: Tag[];
}
