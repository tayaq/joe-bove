import { get } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { API_Slot, AcceptedCountries, Rating, ProCons, Tag } from "../../types";
import { ref, computed, ComputedRef } from "vue";


export default function (criteria: object, customPath?: string) {
  const slots = ref<API_Slot[]>([]);
  const loading = ref<boolean>(true);
  
  const fetchSlots = () : Promise<void | API_Slot> => {
    return get<API_Slot[]>(customPath ?? '', criteria).then((result) => {
      loading.value = false;
      slots.value = camelizeKeys(result)[0].slots;
    });
  };

  fetchSlots();

  return {
    slots,
    fetchSlots,
    loading,
  };
}
