import { getSingleBySlug } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { API_Bookmaker } from "../../types";
import { ref } from "vue";
import bookmakerHelpers from "./bookmakerHelpers";
export default function fetchBookmaker(slug: string | string[]) {
  const bookmaker = ref<API_Bookmaker>({} as API_Bookmaker);
  const loading = ref<boolean>(true);

  const fetchBookmaker = (): Promise<void | API_Bookmaker> => {
    return getSingleBySlug<API_Bookmaker>(GET.BOOKMAKERS, slug).then((result) => {
      loading.value = false;
      bookmaker.value = camelizeKeys(result);
    });
  };

  const { canUserPlay, calcRating, getProCons, getFeatures } = bookmakerHelpers(
    bookmaker
  );

  fetchBookmaker();

  return {
    bookmaker,
    fetchBookmaker,
    loading,
    canUserPlay,
    calcRating,
    getProCons,
    getFeatures,
  };
}
