import { getSingleBySlug } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { Casino, AcceptedCountries, Rating, ProCons, Tag, API_Review } from "../../types";
import { ref, computed, ComputedRef } from "vue";
import {meanBy} from "lodash"

export default function (slug: string|string[]) {
  const review = ref<API_Review>({} as API_Review);
  const loading = ref<boolean>(true);
  
  const fetchReview = () : Promise<void | Casino> => {
    return getSingleBySlug<API_Review[]>(GET.REVIEW, slug).then((result) => {
      loading.value = false;
      review.value = camelizeKeys(result);
    });
  };

  fetchReview();

  return {
    review,
    fetchReview,
    loading,
  };
}
