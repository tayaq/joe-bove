import { get } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { API_Bookmaker, AcceptedCountries, Rating, ProCons, Tag } from "../../types";
import { ref, computed, ComputedRef } from "vue";


export default function (criteria: object, customPath?: string) {
  const bookmakers = ref<API_Bookmaker[]>([]);
  const loading = ref<boolean>(true);
  
  const fetchBookmakers = () : Promise<void | API_Bookmaker> => {
    return get<API_Bookmaker[]>(GET.BOOKMAKERS + customPath ?? '', criteria).then((result) => {
      loading.value = false;
      bookmakers.value = camelizeKeys(result);
    });
  };

  fetchBookmakers();

  return {
    bookmakers,
    fetchBookmakers,
    loading,
  };
}
