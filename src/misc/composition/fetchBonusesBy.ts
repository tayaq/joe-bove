import { get } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { API_Bonus, AcceptedCountries, Rating, ProCons, Tag } from "../../types";
import { ref, computed, ComputedRef } from "vue";


export default function (criteria: object, customPath?: string) {
  const bonuses = ref<API_Bonus[]>([]);
  const loading = ref<boolean>(true);
  
  const fetchBonuses = () : Promise<void | API_Bonus> => {
    return get<API_Bonus[]>(customPath ?? '', criteria).then((result) => {
      loading.value = false;
      bonuses.value = camelizeKeys(result)[0].bonuses;
    });
  };

  fetchBonuses();

  return {
    bonuses,
    fetchBonuses,
    loading,
  };
}
