import { getSingleBySlug } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { Casino } from "../../types";
import { ref } from "vue";
import casinoHelpers from "./casinoHelpers";
export default function fetchCasino(slug: string | string[]) {
  const casino = ref<Casino>({} as Casino);
  const loading = ref<boolean>(true);

  const fetchCasino = (): Promise<void | Casino> => {
    return getSingleBySlug<Casino>(GET.CASINOS, slug).then((result) => {
      loading.value = false;
      casino.value = camelizeKeys(result);
    });
  };

  const { canUserPlay, calcRating, getProCons, getFeatures } = casinoHelpers(
    casino
  );

  fetchCasino();

  return {
    casino,
    fetchCasino,
    loading,
    canUserPlay,
    calcRating,
    getProCons,
    getFeatures,
  };
}
