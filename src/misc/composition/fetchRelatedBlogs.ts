import { get } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import {
  Casino,
  AcceptedCountries,
  Rating,
  ProCons,
  Tag,
  API_Blog,
} from "../../types";
import { ref, computed, ComputedRef } from "vue";
import qs from "qs";

export default function (tags: Tag[], no: number = 2) {
  const blog = ref<API_Blog>({} as API_Blog);
  const loading = ref<boolean>(true);
  
  const fetchBlog = (): Promise<void | Casino> => {
    return get<API_Blog[]>(GET.BLOG, {
      "blogTags.id_in": tags.map(function (obj) {
        return obj.id;
      }),
      "_limit": no,
    }).then((result) => {
      loading.value = false;
      blog.value = camelizeKeys(result);
    });
  };

  fetchBlog();

  return {
    blog,
    fetchBlog,
    loading,
  };
}
