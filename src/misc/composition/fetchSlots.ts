import { get } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import {
  Casino,
  AcceptedCountries,
  Rating,
  ProCons,
  Tag,
  API_Slot,
} from "../../types";
import { ref, computed, ComputedRef } from "vue";
import qs from "qs";

export default function (limit: number = 4) {
  const slots = ref<API_Slot[]>([]);
  const loading = ref<boolean>(true);

  const fetchSlots = (): Promise<void | Casino[]> => {
    return get<API_Slot[]>(GET.SLOTS, {
      _limit: limit,
      _sort: "published_at:DESC",
    }).then((result) => {
      loading.value = false;
      slots.value = camelizeKeys(result);
    });
  };

  fetchSlots();

  return {
    slots,
    fetchSlots,
    loading,
  };
}
