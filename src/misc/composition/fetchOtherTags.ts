import { get } from "../utilis/api/wrappers";
import { kebabize } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import {
  API_Slot,
  API_Comment,
  CommentCTX,
  Rating,
  ProCons,
  Tag,
} from "../../types";
import { ref, computed, ComputedRef, watch } from "vue";
import { message } from "ant-design-vue";
import { find } from "lodash";
import fetchCasinosBy from "./fetchCasinosBy";
import fetchBookmakersBy from "./fetchBookmakersBy";
import { camelizeKeys } from "../utilis/api/helpers";
import fetchSlotsBy from "./fetchSlotsBy";
import fetchBonusesBy from "./fetchBonusesBy";

export default function fetchOtherTags(
  tag: string,
  current: string,
  component: string
) {
  const loading = ref<boolean>(true);
  const tags = ref<Tag[]>([]);
  const fetch = ref();
  const slots = ref();

  const endpoints = [
    GET.TAG_LANGUAGES,
    GET.TAG_LICENCES,
    GET.TAG_CURRENCIES,
    GET.TAG_CURRENCIES,
    GET.TAG_PAYMENT_METHODS,
    GET.TAG_SOFTWARES,
    GET.TAG_RESTRICTED_COUNTRIES,
    GET.TAG_CATEGORIES,
    GET.SLOT_TAGS,
    GET.BONUS_TAGS
  ] as string[];

  const fetchTags = () => {
    get<Tag[]>(
      tag,
      {},
      {
        headers: { "Content-Type": "application/json" },
      }
    ).then((result) => {
      loading.value = false;
      tags.value = camelizeKeys(result);
    });
  };

  const getCurrentTag: ComputedRef<Tag | undefined> = computed(():
    | Tag
    | undefined => {
    return find<Tag>(tags.value, { slug: current });
  });

  if (!endpoints.includes(tag)) {
    // handle error
    message.error(
      "Something has gone wrong! Please refresh this page or contact the administrator."
    );
  } else {
    fetchTags();
  }

  watch(loading, () => {
    if (!loading.value) {
      if (component === GET.CASINOS) {
        const { casinos: data, loading } = fetchCasinosBy(
          {},
          `/getByTags/${tag}/${current}`
        );
        fetch.value = { data, loading };
      } else if (component === GET.BOOKMAKERS) {
        const { bookmakers: data, loading } = fetchBookmakersBy(
          {},
          `/getByTags/${tag}/${current}`
        );
        fetch.value = { data, loading };
      } else if (component === GET.SLOTS) {
        const { slots: data, loading } = fetchSlotsBy({ slug: current }, tag);
        fetch.value = { data, loading };
      } else if (component === GET.BONUSES) {
        const { bonuses: data, loading } = fetchBonusesBy(
          { slug: current },
          tag
        );
        fetch.value = { data, loading };
      }
    }
  });

  return {
    fetch,
    getCurrentTag,
    tags,
    fetchTags,
    loading,
  };
}
