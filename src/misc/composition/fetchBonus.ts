import { getSingleBySlug } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { API_Bonus } from "../../types";
import { ref } from "vue";
// import slotHelpers from "./slotHelpers";

export default function (slug: string | string[]) {
  const bonus = ref<API_Bonus>({} as API_Bonus);
  const loading = ref<boolean>(true);

  const fetchSlot = (): Promise<void | API_Bonus> => {
    return getSingleBySlug<API_Bonus>(GET.BONUSES, slug).then((result) => {
      loading.value = false;
      bonus.value = camelizeKeys(result);
    });
  };

  // const { canUserPlay, calcRating, getProCons, getFeatures } = slotHelpers(
  //   bonuses
  // );

  fetchSlot();

  return {
    bonus,
    fetchSlot,
    loading,
    // canUserPlay,
    // calcRating,
    // getProCons,
    // getFeatures,
  };
}
