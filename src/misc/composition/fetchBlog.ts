import { getSingleBySlug } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import { Casino, AcceptedCountries, Rating, ProCons, Tag, API_Blog } from "../../types";
import { ref, computed, ComputedRef } from "vue";


export default function (slug: string|string[]) {
  const blog = ref<API_Blog>({} as API_Blog);
  const loading = ref<boolean>(true);
  
  const fetchBlog = () : Promise<void | Casino> => {
    return getSingleBySlug<API_Blog[]>(GET.BLOG, slug).then((result) => {
      loading.value = false;
      blog.value = camelizeKeys(result);
    });
  };

  fetchBlog();

  return {
    blog,
    fetchBlog,
    loading,
  };
}
