import { getSingle } from "../utilis/api/wrappers";
import { camelizeKeys } from "../utilis/api/helpers";
import { GET } from "../utilis/api/endpoints";
import {ref} from "vue";
import { API_Blog } from "../../types";
import { Link } from "../../types"; 



export default function fetchHomeGridNews() {
  const gnews = ref<API_Blog>({} as API_Blog);
  const loading = ref<boolean>(true);
  const fetchGridNews= () => {
    getSingle<API_Blog>(GET.BLOG).then((result) => {
      loading.value = false;
      gnews.value = camelizeKeys(result);
    });
  };
  fetchGridNews();
  return{gnews,loading,fetchGridNews}
}
