import marked from 'marked';

export default {
  install: (app) => {
    app.config.globalProperties.$formatTextToMarkdown = (text) => {
      return marked(text);
    };
  },
};
